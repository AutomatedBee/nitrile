# Nitrile

This is the repository for Nitrile, the [Clean][] package manager.

## Documentation

The documentation is a MkDocs project, and is hosted at
https://clean-and-itasks.gitlab.io/nitrile.

To work on the documentation, use the following:

```bash
cd doc
sudo pip3 install -r requirements.txt
mkdocs serve
```

You can now open http://localhost:8000/nitrile in your browser.

Alternatively, use `mkdocs build` to generate a static version.

## Maintainer & license

This project is maintained by [Camil Staps][].

The following people have contributed:

- Gijs Alberts
- Elroy Jumpertz
- Mart Lubbers

This project is licensed under AGPL v3 with additional terms under section 7;
see the [LICENSE](/LICENSE) file for details.

The files in the `doc` directory are licensed under CC BY-NC-SA 4.0 instead;
see https://creativecommons.org/licenses/by-nc-sa/4.0/ for details.

The files `bin/libz3.dll` (on Windows) and `bin/libz3.nitrile.so` (on Linux)
are licensed under the license in [LICENSE-z3](/LICENSE-z3).

[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org
