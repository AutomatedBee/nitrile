#!/bin/sh

# Copy system libz3.so into bin/ and patch bin/nitrile 
# to use local library instead of system

if ! command -v patchelf > /dev/null 2>&1; then
    echo "WARN: patchelf is required for 'copy-libz3'"
    exit 1
fi

if [ ! -x bin/nitrile ]; then
    echo 'WARN: Run "nitile build"'
    exit 1
fi

LIB_Z3_PATH=$( ldd bin/nitrile | awk '/libz3/{print $3}' )
LIB_Z3_NAME=$( ldd bin/nitrile | awk '/libz3/{print $1}' )
LIB_Z3_NITRILE=libz3.nitrile.so

if [ -f "bin/$LIB_Z3_NITRILE" ]; then
    echo "WARN: bin/$LIB_Z3_NITRILE already copied"
elif [ ! -f "$LIB_Z3_PATH" ]; then
    echo "WARN: could't find \"$LIB_Z3_PATH\"."
    echo 'WARN: Run "nitrile build"'
    exit 1
else
    cp -H "$LIB_Z3_PATH" "bin/$LIB_Z3_NITRILE"
    chmod a-x "bin/$LIB_Z3_NITRILE"
fi

if [ "$LIB_Z3_NAME" != "$LIB_Z3_NITRILE" ]; then
    patchelf --replace-needed "$LIB_Z3_NAME" "$LIB_Z3_NITRILE" bin/nitrile
    patchelf --add-rpath '$ORIGIN' bin/nitrile
    echo "copy-libz3: Copied $LIB_Z3_PATH -> bin/$LIB_Z3_NITRILE"
else
    echo "copy-libz3: WARN: bin/nitrile already use $LIB_Z3_NITRILE"
fi
