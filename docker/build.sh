#!/bin/sh

set -e

DEBIAN_RELEASES="bullseye bookworm"
LATEST_DEBIAN_RELEASE=bookworm
VERSIONS="$(curl -s https://clean-lang.org/api/packages/nitrile | jq -cr '.versions | keys[]' | sort -V)"

for VERSION in $VERSIONS; do
	for DEBIAN_RELEASE in $DEBIAN_RELEASES; do
		TAG="$VERSION-$DEBIAN_RELEASE"

		echo $DEBIAN_RELEASE $VERSION

		docker build --pull --build-arg DEBIAN_RELEASE=$DEBIAN_RELEASE --build-arg VERSION=$VERSION -t "cleanlang/nitrile:$TAG" .

		VERSION_WITHOUT_PATCH="$(echo "$VERSION" | sed 's/\.[[:digit:]]\+$//')"
		docker tag "cleanlang/nitrile:$TAG" "cleanlang/nitrile:$VERSION_WITHOUT_PATCH-$DEBIAN_RELEASE"

		docker tag "cleanlang/nitrile:$TAG" "cleanlang/nitrile:latest-$DEBIAN_RELEASE"

		if [ "$DEBIAN_RELEASE" = "$LATEST_DEBIAN_RELEASE" ]; then
			docker tag "cleanlang/nitrile:$TAG" "cleanlang/nitrile:$VERSION"
			docker tag "cleanlang/nitrile:$TAG" "cleanlang/nitrile:$VERSION_WITHOUT_PATCH"
			docker tag "cleanlang/nitrile:$TAG" "cleanlang/nitrile:latest"
		fi

		VERSION_WITHOUT_MINOR="$(echo "$VERSION_WITHOUT_PATCH" | sed 's/\.[[:digit:]]\+$//')"
		if [ "$VERSION_WITHOUT_MINOR" != "0" ]; then
			docker tag "cleanlang/nitrile:$TAG" "cleanlang/nitrile:$VERSION_WITHOUT_MINOR-$DEBIAN_RELEASE"

			if [ "$DEBIAN_RELEASE" = "$LATEST_DEBIAN_RELEASE" ]; then
				docker tag "cleanlang/nitrile:$TAG" "cleanlang/nitrile:$VERSION_WITHOUT_MINOR"
			fi
		fi
	done
done

docker push --all-tags cleanlang/nitrile
