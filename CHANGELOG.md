# Changelog

#### v0.4.20

- Fix: added libz3.so to linux package to make it compatible with non
  debian/ubuntu distributions.

#### v0.4.19

- Enhancement: `nitrile fetch` now outputs a list of dependencies that are not
  using the latest version.
- Enhancement: ignore `*.tmp*` files with `nitrile watch`; these are sometimes
  generated as temporary files by system linkers.
- Fix: exit with non-zero exit code if build specified with `--only` does not
  exist.

#### v0.4.18

- Fix: fix error that base-clm could not be found when running compilation
  tests for some packages.

#### v0.4.17

- Fix: include `scope: Test` dependencies when running `script` tests.

#### v0.4.16

- Fix: only test directories with `scope: Use` in compilation tests.

#### v0.4.15

- Change: only test directories with `scope: Use` in compilation tests.
- Enhancement: parallelize downloads when running `nitrile fetch`.
- Fix: only use dependencies with `scope: Use` in compilation tests.

#### v0.4.14

- Fix: fixes an issue where `PATH` and other environment variables were not set
  correctly for `script` tests, so that e.g. `clm` jobs would not execute
  correctly.

#### v0.4.13

- Feature: add `except_modules` and `only_modules` to compilation test
  configuration.
- Feature: add shorthand syntax for dependency lists in nitrile.yml.
- Enhancement: add error when `required_dependencies` mentions a dependency
  that is not marked as optional.
- Fix: fix incorrect error when using `compilation` tests and clm is not
  installed.

#### v0.4.12

- Chore: on Windows, don't check for experimental feature
  `PSNativeCommandErrorActionPreference` on PowerShell >= 7.4, in which this
  feature has become stable.

#### v0.4.11

- Feature: add `required_dependencies` field to `build` and `tests` in
  nitrile.yml to require optional dependencies for certain jobs.
- Change: `nitrile fetch` now downloads optional dependencies by default. This
  can be prevented with `--no-optional`.
- Enhancement: write `typedef` extension field to ctags with a full
  representation of type definitions.
- Enhancement: write the lowest compatible `format_version` in `nitrile init`,
  instead of the current version.

#### v0.4.10

- Feature: add `undecidable_instances` to `clm_options` in `nitrile.yml`.

#### v0.4.9

- Feature: add `actions` setting and `nitrile do` subcommand.
- Feature: add `--list` option to `nitrile build` and `nitrile test`.
- Feature: add `--no-header` global option.
- Enhancement: improve error when the dependencies of a local dependency cannot
  be found.
- Fix: only accept `--clm-linker=lld` on x64; this linker throws errors on x86.

#### v0.4.8

- Fix: on Windows, correctly strip the BOM from files used in
  `expected_result` tests.

#### v0.4.7

- Feature: add global configuration file `~/.nitrile/nitrile-settings.yml` for
  default values of `--parallel-jobs` and `--clm-linker`.
- Feature: add global option `--clm-linker`.
- Feature: add global option `--parallel-jobs`.
- Enhancement: write a `format_version` when using `nitrile init` (the current
  Nitrile version) to avoid warnings when the new package is used.
- Fix: on Windows, catch non-zero exit codes of native commands in `build` (and
  test) scripts. This requires the user to have run
  `pwsh.exe -Command 'Enable-ExperimentalFeature PSNativeCommandErrorActionPreference'`.
- Fix: `nitrile init` now writes a trailing newline.

#### v0.4.6

- Fix: do not print legal header when using `nitrile run` so that only output
  of the child process is printed to stdout.
- Fix: replace Nitrile process with child process in `nitrile run` so that
  stdin is passed correctly.

#### v0.4.5

- Feature: add `nitrile global install PACKAGE [VERSION]`,
  `nitrile global list` and `nitrile global remove PACKAGE VERSION` commands.
- Feature: add `nitrile run PACKAGE BINARY [ARGS]` command.
- Feature: add `nitrile help` command.
- Fix: also watch paths of dependencies and local dependencies in `nitrile
  watch`.
- Fix: lockfiles are now written with a trailing newline.

#### v0.4.4

- Feature: add `optional` configuration option for dependencies.
- Feature: add `nr-of-tests` option to `property-tester-options` to specify a
  maximum number of tests to execute per property.
- Enhancement: delete old dependencies that are not needed any more on `nitrile
  fetch`.
- Enhancement: add `format_version` field to `nitrile.yml`.
- Enhancement: ignore `*.dcl~` and `*.icl~` in `nitrile watch`, which are
  automatically generated by Vim if `writebackup` is set.
- Fix: `nitrile fetch` now looks at the actually installed versions of packages
  to see what has to be updated, instead of at the lockfile.
- Fix: take actually installed versions of packages into account when resolving
  dependencies, instead of looking at the lockfile.

#### v0.4.3

- Feature: add `--except` option to `nitrile test`.

#### v0.4.2

- Enhancement: make `contact_email`, `description`, `license`, `maintainer`,
  `name`, `url`, `version` optional (except when they are needed in
  `nitrile package` and/or `nitrile publish`).
- Enhancement: add `version` field to `.nitrile.json` (created by
  `nitrile package`).

#### v0.4.1

- Feature: add `--property-tester-options` option to `nitrile test` with
  possible options `compile-only`, `no-regenerate`, and `only`.
- Feature: add `--test-runner-options` option to `nitrile test` with option
  `parallel`.
- Enhancement: added documentation on versioning.

## v0.4

- Changed: the default `scope` for `dependencies` is now `Build` for packages
  with `type: Application`.
- Removed: removed `build_dependencies` and `test_dependencies`, which can now
  be specified using `scope: Build` and `scope: Test` in `dependencies`.
- Removed: removed `test_src`, which can now be specified using `scope: Test`
  in `src`.
- Removed: removed the `publish-0.2` alias for `nitrile publish`.

#### v0.3.3

- Change: `build_dependencies` and `test_dependencies` are deprecated. You can
  now specify these in regular dependencies, e.g.:

  ```yml
  dependencies:
    base: ^1.0
    property-tester:
      version: ^3.0
      scope: Test
  ```

  Possible values for `scope` are `Build`, `Test`, and `Use`. The default scope
  is `Build` for applications and `Use` for libraries and miscellaneous
  packages.
- Change: `test_src` is deprecated. You can now specify these directories in
  `src` with a `Test` scope, e.g.:

  ```yml
  src:
    - src
    - path: test
      scope: Test
  ```

  For possible values of `scope`, see above. `Use` is not allowed in
  applications.
- Change: the constraint sets stored in `~/.nitrile` now include a boolean
  indicating whether dependencies are optional or not. Run `nitrile update`
  once to avoid the error "Failed to parse constraint set.".
- Fix: do not include test dependencies when building.
- Fix: disallow `--local-dependency` with `nitrile fetch`, as it is not clear
  what the behaviour should be.

#### v0.3.2

- Fix: fix adding of `git_ref` to generated package files in GitLab CI if `git`
  is not installed; use environment variables `CI_COMMIT_TAG` and
  `CI_COMMIT_SHA` instead.

#### v0.3.1

- Feature: add `git_ref` key to (i) `.nitrile.json` in package tarballs and
  (ii) `nitrile-metadata.json` in GitLab packages. It contains a reference to
  the git commit from which the package was built.
- Enhancement: improve errors reported by `nitrile fetch` in case of
  unsatisfiable dependencies.
- Fix: fix importing modules from the dependencies of a `--local-dependency`.

## v0.3

- Chore: move to https://clean-lang.org.
- Feature: add `list_types` field to `clm_options`.
- Enhancement: be more informative when resolving dependency fails.
- Fix: give a correct 'unsatisfiable core' of dependencies if constraint
  resolving fails; previously the first few dependencies would be listed.
- Fix: allow constraint solving to succeed when a package has broken versions,
  if there is at least one version that is not broken.

#### v0.2.23

- Feature: add `nitrile watch` subcommand to rebuild/retest when source files
  change. Requires [Watchman](https://facebook.github.io/watchman/). Also adds
  a global `watch` key to `nitrile.yml`.
- Enhancement: enable parallel jobs in `clm` when available.
- Enhancement: print short program name instead of full path in legal header on
  Windows.
- Fix: don't print legal header in `nitrile env` as newlines will be discarded
  by bash ``eval `...` ``.

#### v0.2.22

- Robustness: retry download/upload in `nitrile fetch` and `nitrile publish`.
- Fix: print legal header as comments in `nitrile env`.
- Fix: don't show warnings for `_system` and `_startup*` modules in `base-rts`
  and `base-stdenv` on `nitrile fetch`.

#### v0.2.21

- Fix: allow `nitrile build` without preceding `nitrile update` if there are no
  dependencies.

#### v0.2.20

- Feature: add `nitrile generate-tags` subcommand to generate `.nitrile-tags`,
  for use by editors.
- Feature: on `nitrile fetch`, an extended ctags file is now written to each
  package's `lib/.nitrile-tags` file, for use by editors.
- Feature: add option `--local-dependency` to develop multiple packages at the
  same time.
- Feature: add option `--verbose`.
- Feature: add `nitrile terms` subcommand.
- Enhancement: more detailed error information in `nitrile package`.
- Robustness: let `curl` follow redirects (needed when the GitLab handle has
  changed, or uses different capitalization).
- Fix: set modification time of dependencies to time of `nitrile fetch`, so
  that modules are recompiled when needed.

#### v0.2.19

- Fix: issue where destination paths in packages lost the first character of
  some path parts, depending on whether a trailing slash was used or not.
- Fix: use `nitrile-tmp` instead of `nitrile-tmp-0`.

#### v0.2.18

- Enhancement: add pattern support to `package.include`. Add asterisk patterns
  to both `package.exclude` and `package.include`.
- Fix: when `nitrile-tmp` already exists, use `nitrile-tmp-1` (etc.) instead.
  Fixes a "Failed to create `../nitrile-tmp`" error when nitrile is used in a
  nitrile build step.

#### v0.2.17

- Feature: add `env` subcommand.

#### v0.2.16

- Fix: write build scripts to a file in the temporary directory instead of
  passing them as an argument to `powershell`. This fixes long build scripts
  (>= 32768 characters) and scripts with funny characters.

#### v0.2.15

- Enhancement: add output on `nitrile update`.
- Fix: on Windows, append `.exe` to the `compiler` `clm` option if needed
  (again, fix in v0.2.14 was faulty in some cases).
- Fix: on Windows, append `.exe` to the `post_link` `clm` option if needed.

#### v0.2.14

- Fix: include applications (icl modules without dcl) in `compilation` tests.
- Fix: on Windows, append `.exe` to the `compiler` `clm` option if needed.

#### v0.2.13

- Feature: add `extra_tests` field to rule settings.
- Feature: add global `clm_options` setting.

#### v0.2.12

- Change: `expected_result` tests now convert Windows line endings to `\n`
  before checking the result.

#### v0.2.11

- Enhancement: when `nitrile fetch` cannot satisfy version constraints, it now
  outputs a set of packages of which the constraints are irreconcilable.
- Enhancement: flush stdout after logging, to improve relative positioning of
  messages on stdout and stderr in CI.
- Fix: when a dependency is mentioned in at least two of `dependencies`,
  `build_dependencies`, and `test_dependencies`, the constraints are combined
  when running `nitrile fetch` (instead of picking one of the constraints
  semi-at-random).
- Fix: avoid powershell exception when a build step has an empty script.
- Fix: translate paths in `package.exclude` on Windows platforms.
- Fix: error as soon as a script step fails on Windows (as on UNIX).

#### v0.2.10

- Fix: close pipes after we're done with them.

#### v0.2.9

- Fix: fix `compiler_options` clm option on Windows (commas have to be escaped
  in PowerShell).

#### v0.2.8

- Feature: add `clm` option `post_link` for the iTasks web resource collector.
- Change: package names must now match the regular expression
  `[a-z]([a-z0-9-]*[a-z0-9])?`.

#### v0.2.7

- Change: do not enable fusion by default, as this seems to be buggy.
- Fix: fix cycle in spine in `fetch` by disabling fusion.

#### v0.2.6

- Feature: add `clm` options `compiler`, `partial_functions`, `profiling`, and
  `fusion`.
- Change: fusion of normal functions is enabled by default in `clm` builds.
- Change: the `stack_tracing` option for `clm` is now incorporated in the new
  `profiling` option.
- Change: `.nitrile.json` in package tarballs now always uses `/` as a
  directory separator for consistency.

#### v0.2.5

- Feature: add `tests:compilation` for testing that all modules in `src` and
  `test_src` compile, as a very crude test for libraries.
- Change: `.nitrile.json` now contains a `fileinfo` key instead of
  `file_sources`. This key is a record with an entry for each path in the
  package. Entries can contain more information than the source file, in
  particular whether a module is a 'core' module or not (to be used by
  Cloogle).
- Fix: "No such file or directory" error when running a `clm` build without
  `target`.

#### v0.2.4

- Feature: add `.nitrile.json` to generated package tarballs, with a source map
  that Cloogle can use to generate URLs to the GitLab blame view.

#### v0.2.3

- Feature: add support for property tests with
  [property-tester](https://clean-lang.org/pkg/property-tester).
- Change: use pseudoterminals for running builds and tests on POSIX. This means
  nitrile won't pass the output to `stdout` and `stderr` (because
  pseudoterminals don't distinguish these), but the advantage is that `stdout`
  is now line-buffered so that the order is as expected.
- Change: more readable parse stack in case nitrile.yml cannot be parsed.

#### v0.2.2

- Fix: on Windows force UTF-8 encoding of `expected_result` tests (by default,
  PowerShell would use UTF-16).

#### v0.2.1

- Feature: allow multiple packages in one nitrile.yml; add `--name` option to
  select a different one than the first.
- Fix: output of `init` subcommand used the `gitlab` key instead of `url`.

## v0.2

- Feature: add support for packages for `any` platform and/or architecture.
- Feature: add `extra_dependencies` option in `rules`.
- Change: the `publish` subcommand now has the semantics of `publish-0.2`.
- Removed: the `install` alias for the `fetch` subcommand.
- Removed: old-style notation of builds and tests in nitrile.yml.

#### v0.1.16

- Feature: new `publish-0.2` subcommand which publishes packages for all
  targets at once.

#### v0.1.15

- Feature: add `clm` option `wait_for_key_press`.
- Feature: add Windows support.

#### v0.1.14

- Feature: add `package:exclude` option.
- Feature: add `nitrile init` subcommand.

#### v0.1.13

- Feature: add `test_dependencies`.
- Feature: add support for
  [test-runner](https://clean-lang.org/pkg/test-runner) tests.

#### v0.1.12

- Feature: add `clm` options `generate_descriptors`, `export_local_labels`,
  `strip`.
- Fix: `clm` bytecode options where not used correctly.
- Fix: last command of `expected_result` tests now runs in the same shell so
  that it has the same directory, access to environment variables, etc.

#### v0.1.11

- Feature: add `clm` option `compiler_options`.

#### v0.1.10

- Feature: add `clm` option `src`.
- Feature: `tests` that just run a series of shell commands.
- Feature: give builds and tests names; add `depends` option.
- Feature: add `--only` CLI option for `build` and `test` subcommands.
- Change: rename `nitrile install` to `nitrile fetch`; `install` is an alias.
- Enhancement: build `script`s are now optional (useful for meta-goals).
- Enhancement: always run `clm` with support for dynamics.
- Enhancement: allow directories in `package:extra_files`

#### v0.1.9

- Feature: add `test` subcommand and `tests` options.
- Feature: add `clm` options `stack_tracing` and `print_constructors`.

#### v0.1.8

- Fix: fix include paths when dependencies of dependencies are used.

#### v0.1.7

- Feature: add `license` field to nitrile.yml.

#### v0.1.6

- Feature: add `clm` options `warnings`, `garbage_collector`, `check_indices`,
  and `link`.

#### v0.1.5

- Fix: packages mentioned in the lockfile but not present in
  `nitrile-packages` are now installed when running `fetch`.

#### v0.1.4

- Feature: add central directory with binaries of dependencies; use this
  directory when running `clm` builds.
- Removed: library sources (e.g. `lib: Platform`); these should now be
  dependencies.
- Robustness: ignore unparseable packages in the registry when running
  `update`.

#### v0.1.3

- Feature: add options to build/package/test for a target different than the
  current one.
- Feature: add `Miscellaneous` package type, e.g. for `base`.
- Enhancement: add lockfile.
- Robustness: allow broken packages in the index when running `fetch`, as long
  as they are not relevant to us.

#### v0.1.2

First tagged version.
