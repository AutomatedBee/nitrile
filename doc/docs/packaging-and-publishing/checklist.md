---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Checklist

You can use the following checklist to prepare for publishing a package to the
registry for the first time, as well as for subsequent updates.

## Basics

1. [`contact_email`](/nitrile-yml/reference.md#contact_email) should be a valid
	 email address which you regularly check. It is not normally used, but the
	 registry maintainer may use it to contact you if necessary.
2. Check that [`description`](/nitrile-yml/reference.md#description) gives a
	 brief, up to date, accurate presentation of what your package does.
3. Also check `name`, `maintainer`, and `version`.
4. Check that [`url`](/nitrile-yml/reference.md#url) is up to date.
5. Check that your GitLab repository is publicly available.
6. It is recommended to add a `README.md` and `CHANGELOG.md` (they are used by
	 the user interface of [the registry](https://clean-lang.org)), but they are
	 not required.

## Versioning

Nitrile relies on [semantic versioning](https://semver.org). This means that
packages have a version `MAJOR.MINOR.PATCH`, where:

- `MAJOR` increases with incompatible API changes.
- `MINOR` increases when functionality is added in a compatible way (including
  adding exported symbols, which may cause name clashes).
- `PATCH` increases with backwards compatible bug fixes.

Make sure that the [version](../nitrile-yml/reference.md#version) in
`nitrile.yml` is updated accordingly prior to publishing.

## Licensing

A number of projects in the registry are licensed under a copyleft license
(e.g. GPL or AGPL). If your project links with such a dependency, it will need
to be licensed under the same or a compatible license.

## Including the right files

1. Before publishing your package, run `nitrile package` locally. Inspect the
	 resulting tarball to ensure that it contains all the necessary files (and
	 not too many).
2. In most cases, you will want to include files like `LICENSE`, `README.md`,
	 and `CHANGELOG.md` in your package. Do this with
	 [`package:extra_files`](../nitrile-yml/reference.md#packageextra_files):

```yml
package:
  extra_files:
    - CHANGELOG.md
    - LICENSE
    - README.md
```

## Special files and directories

In principle you are free to structure your packages how you see fit. However,
a number of files and directories have a special function:

- `bin` should contain executables that can be run by the user. These are
	exposed in `$PATH` when the package is
	[installed globally](../cli/global.md#nitrile-global-install-package-version),
	and are available with [`nitrile run`](../cli/run.md).
- `exe` should contain executables that should not be run by the user directly.
	Examples are the code generator or the iTasks post-linker.
- `lib` should contain public modules of libraries; these are added to the
	include path of build tools.
- `misc/dll` can be used to distribute DLLs that are needed to build
	applications with the library. (DLLs that are needed to run executables in
	`bin` or `exe` should be added to those directories, however.)
- `misc/src` can be used to distribute source files (most commonly C headers)
	that can be used by users of the library.
- There should not be a file `.nitrile.json`, as this is used internally.

You can now proceed to set up [GitLab CI](gitlab-ci-examples.md).
