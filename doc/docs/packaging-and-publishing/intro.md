---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Packaging and publishing with Nitrile

Nitrile packages are not stored centrally. Instead, distributions are stored in
[GitLab.com][] projects for each package. The [Clean Registry][] only keeps
track of the packages in existence and links to the files on GitLab.com.

To publish a package, you need to create a GitLab CI configuration that runs
[`nitrile publish`](/cli/publish.md) after
[`nitrile package`](/cli/package.md). This command:

1. Uploads the package and some metadata to GitLab as a [generic package][].
2. Informs the registry of the change.

The registry will then check GitLab.com and update the versions it holds. You
do not need to create an account on the registry for this; the registry has no
accounts.

`nitrile publish` **should only be run when a new version is created**. We have
collected some [example configurations](gitlab-ci-examples.md) which make sure
that `nitrile publish` is only run for git tags.

Before publishing a package, make sure that it is [ready](checklist.md).

[generic package]: https://docs.gitlab.com/ee/user/packages/generic_packages/index.html
[GitLab.com]: https://gitlab.com/
[Clean Registry]: https://clean-lang.org/
