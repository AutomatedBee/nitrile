---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Nitrile

Nitrile is a package manager for [Clean][].

These pages document the use of the package manager. Click on the "Next" link
below to continue.

[![CC BY-NC-SA 4.0 license](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)  
This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).

The source for these pages can be found on
[GitLab](https://gitlab.com/clean-and-itasks/nitrile).
Contribution is much appreciated.

[Clean]: https://clean-lang.org
