---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Global settings

For some [global options](global-options.md), a default value can be set in a
global settings file. This file is `nitrile-settings.yml` and should be located
in the global Nitrile directory.

## `clm`

This field can be used to control the behaviour of
[`clm`](../nitrile-yml/reference.md#buildscriptclm) jobs. The following fields
are accepted:

- `linker`: specify the linker to use (see
  [`--clm-linker`](global-options.md#-clm-linkerlinker))
- `parallel_jobs`: specify how many subprocesses can be used in parallel (see
  [`--parallel-jobs`](global-options.md#-parallel-jobsn))

For example:

```yml
clm:
  linker: lld
  parallel_jobs: 4
```
