---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# `nitrile global`

This command is used to manage packages installed in a global, cross-project
scope. Globally installed packages can be used with [`nitrile run`](run.md).

This command can also be used to
[manage Nitrile versions](../intro/installation.md#updating-nitrile).

## Commands

The following commands can be performed:

- [`install`](#nitrile-global-install-package-version): installs a new (version
	of a) package in the global scope.
- [`list`](#nitrile-global-list): lists all currently installed packages.
- [`remove`](#nitrile-global-remove-package-version): removes a package from
	the global scope.

### `nitrile global install PACKAGE [VERSION]`

Installs a package globally. If `VERSION` is not specified, the latest version
known to the registry will be installed.

You should run [`nitrile update`](update.md) beforehand to have an up to date
copy of the registry.

!!! warning
	Installing a package globally does not install any dependencies. The reason
	for this is that [`nitrile run`](run.md) allows dependencies to be installed
	both locally and globally. Therefore, at the moment a global package is
	installed, nitrile does not know whether the dependencies should be installed
	locally or globally.

### `nitrile global list`

Lists globally installed packages and their versions.

### `nitrile global remove PACKAGE VERSION`

Removes the globally installed version `VERSION` of `PACKAGE`.
