---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# `nitrile package`

This command packages the package. This builds a tarball
`PACKAGE-VERSION-PLATFORM-ARCHITECTURE.tar.gz`, e.g.
`nitrile-0.1.0-linux-x64.tar.gz`.

The package typically contains at least one of three directories:

- `lib`: these directories are included in the include path when building
	applications that depend on your package. They should contain the source code
	of libraries.
- `bin`: files in this directory will be available in the `PATH` when running
	[`nitrile build`](build.md) and [`nitrile test`](test.md).
- `exe`: files in this directory are in the `CLEANLIB` path. This is used by
	make tools like [`clm`](../nitrile-yml/reference.md#buildscriptclm) to access
	the compiler and code generator. You normally don't need this directory.

In addition, a package may contain any other files and directories. Usually,
you want to include at least `LICENSE` and `README.md`, plus any files that
`README.md` may depend on (like images).

It is not needed to include `nitrile.yml` in the package.

## Package metadata

A tarball created with `nitrile package` also contains a special file
`.nitrile.json` with some metadata. This JSON file contains the following keys:

### `fileinfo`

This is a mapping from paths in the package to records containing information
about the file:

- `src`: the source file in the repository, used by [Cloogle][] to generate
	links to blame views on GitLab.
- `core_module`: true if this is a Clean module that should be considered
	library-internal, not to be used outside of the library. This is also used by
	[Cloogle][], but may for example also be used by IDEs.

	Which modules are considered core modules is determined by the
	[`package:core_modules`](../nitrile-yml/reference.md#packagecore_modules)
	setting in nitrile.yml.

Paths always use `/` as directory separator, also on Windows.

### `git_ref`

If the package was built in a git repository, this key contains a reference to
the commit on which it was built. A tag is preferred, if it can be parsed as a
semantic version (optionally prefixed by `v`) and this version matches the one
in nitrile.yml. Otherwise, a hash is used.

### `version`

The [version](../nitrile-yml/reference.md#version) of the package.

## Command behaviour

By default, all executables created in
[`build`](../nitrile-yml/reference.md#build) steps are copied. Normally you
will want to generate these executables in `bin` (or `exe`).

Additionally, all source files in [`src`](../nitrile-yml/reference.md#src)
directories with the `Use` [`scope`](../nitrile-yml/reference.md#srcscope) are
copied to `lib`. This includes `src` directories without explicit `scope` in
libraries and miscellaneous packages (see
[`type`](../nitrile-yml/reference.md#type)).

To include non-Clean source files from `src`, use
[`package:include`](../nitrile-yml/reference.md#packageinclude). To exclude
some source files from `src`, use
[`package:exclude`](../nitrile-yml/reference.md#packageexclude).

To include extra files (like a `LICENSE` or `README.md`), use
[`package:extra_files`](../nitrile-yml/reference.md#packageextra_files).

[Cloogle]: https://cloogle.org
