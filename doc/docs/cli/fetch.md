---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# `nitrile fetch`

This command installs dependencies of the current package in
`nitrile-packages`. Dependencies that were previously installed but are not
needed any more are deleted.

You need to run [`nitrile update`](update.md) beforehand to have an up to date
copy of the registry.

## Options

### `--no-optional`

Do not fetch
[optional dependencies](../nitrile-yml/reference.md#dependenciesoptional).
