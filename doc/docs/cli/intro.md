---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# The Nitrile CLI

With Nitrile [installed](../intro/getting-started.md) you can start using the
CLI to build, test, package, and publish applications and libraries. These
pages document the command line interface.

## Usage

In general, `nitrile` is invoked with a subcommand, for example:

```txt
$ nitrile build
```

There are [global options](global-options.md), which may appear both before and
after the subcommand. For some global options a default can be set in a
[global settings file](global-settings.md).

Some subcommands have additional options. These may only appear after the
subcommand.

For example:

```txt
$ nitrile --global-option-1 build --global-option-2 --build-option
```

## Subcommands

The CLI has the following subcommands:

- [`build`](build.md): runs the build steps for the current package.
- [`env`](env.md): outputs a shell or PowerShell script to set some environment
	variables with which you can run `clm` from the command line.
- [`fetch`](fetch.md): fetches dependencies of the current package.
- [`generate-tags`](generate-tags.md): generates a ctags file for use in an
	editor.
- [`global`](global.md): allows to manage global (cross-project) packages.
- `help`: shows help on a specific subcommand.
- [`init`](init.md): creates a new package in the current directory.
- [`package`](package.md): packages the current package.
- [`publish`](publish.md): publishes the current package to the registry.
- [`run`](run.md): runs executables from local or global packages.
- [`terms`](terms.md): prints the full legal terms of the program.
- [`test`](test.md): runs the tests for the current package.
- [`update`](update.md): updates the local copy of the registry.
