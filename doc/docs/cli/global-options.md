---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Global options

## `--arch=ARCHITECTURE`

By default, Nitrile will detect the current platform and architecture and build
for this target. `--arch` allows you to pretend to be a different target.

This is for example useful to build a 32-bit application on a 64-bit platform.

It can also be used to package libraries for entirely different targets (e.g.,
create an ARM distribution on an Intel machine) when there are no build steps,
i.e. when packaging just involves collecting files.

Possible values of `ARCHITECTURE` are:

- `arm64`
- `x64`
- `x86`
- `any` (when the architecture and bitwidth do not matter)

The following values can be used when the architecture matters but the bitwidth
does not matter:

- `arm`
- `intel`

The following values can be used when the bitwidth matters but the architecture
does not matter:

- `32bit`
- `64bit`

## `--clm-linker=LINKER`

Use `LINKER` to link the application in `clm` jobs. For the possible values of
`LINKER`, see the
[`gcc` documentation on `-fuse-ld`](https://gcc.gnu.org/onlinedocs/gcc/Link-Options.html).
Use `--clm-linker=ld` to use the default linker.

A default can be set for this option in
[`nitrile-settings.yml`](global-settings.md).

## `--local-dependency DEP PATH`

This option is useful when developing multiple packages at once. For example,
you are fixing a bug in package `current` for which some changes in a
dependency `dep` are needed. You can then clone the repository for `dep`
elsewhere, and use `--local-dependency dep path/to/dep` when running
`nitrile build` or `nitrile test` in the current package.

`PATH/nitrile.yml` must exist. The dependencies of the current package and those
of `DEP` are combined. The constraint solver is invoked to find a set of
packages from both the current package's `nitrile-packages` and `DEP`'s
`nitrile-packages`. Thus, for each dependency, one of these sources is used (in
descending order of preference):

- The current package
- A package specified with `--local-dependency`
- A package installed in either the current package's `nitrile-packages`, or in
	the `nitrile-packages` dir of a local dependency

Because `--local-dependency` uses special logic to resolve dependencies, it
cannot be used with `nitrile fetch`.

!!! info
	Use `--verbose` to see the sources used for all the dependencies.

!!! warning
	For the time being `--local-dependency` has an impact on include paths only.
	It is not possible to use this option to point to a package from which an
	executable from `bin` or `exe` is needed.

!!! warning
	Build steps of local dependencies are not taken into account. If your changes
	in the local dependencies require a `nitrile build` cycle (for example, if
	the dependency contains C sources to link with), you have to manually run
	`nitrile build` in the directory of the dependency.

## `--name=NAME`

When `nitrile.yml` contains
[multiple packages](../nitrile-yml/intro.md#multiple-packages), `NAME` specifies
the package to use.

## `--no-header`

Do not print copyright and license information. This can be useful in scripts.
The license terms still apply.

## `--parallel-jobs=N`

Use at most `N` parallel jobs in `clm` jobs in [build](build.md) and
[test](test.md) scripts. `N` must be a positive integer and should not be
higher than the number of available processors. By default, `clm` takes as many
subprocesses as there are processors available.

A default can be set for this option in
[`nitrile-settings.yml`](global-settings.md).

## `--platform=PLATFORM`

This is like [`--arch`](#-archarchitecture), but allows you to specify a
platform rather than an architecture. Possible values of `PLATFORM` are:

- `linux`
- `mac`
- `windows`
- `any` (when the platform does not matter)

## `--verbose`

Enables verbose output.
