---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# `nitrile watch`

This command watches the source and test directories and runs user-configurable
[commands](#subcommands) when
[changes are detected](#which-changes-are-detected).

This command requires [Watchman](https://facebook.github.io/watchman/) to be
installed.

!!! note
	[Optional dependencies](../nitrile-yml/reference.md#dependenciesoptional)
	are treated as required. This means that all dependencies must be
	installed, and that `nitrile watch` will trigger commands when a file
	changes in any of the dependencies, even if that dependency is optional and
	not required for the triggered command.

!!! warning
	There is no guarantee that all commands in a sequence will be executed. For
	efficiency reasons, Nitrile will check if changes have been detected after
	every command in the sequence. If changes have been detected, the rest of the
	commands will not be executed and the sequence will be restarted.

!!! warning
	This command is not heavily tested on Windows, and Watchman is currently in
	beta on Windows.

## Which changes are detected?

Nitrile will detect changes to files in the following directories:

- [`src`](../nitrile-yml/reference.md#src);
- any [`extra_src`](../nitrile-yml/reference.md#rulesextra_src) field in rules;
- any [`src`](../nitrile-yml/reference.md#clm_options) field in
	[`clm`](../nitrile-yml/reference.md#buildscriptclm) options;

*except*:

- compiler-generated files: `*.abc`; `*.bc`; `*.o`; `*.obj`; `*.pbc`; `*.tcl`;
- any target of [`clm`](../nitrile-yml/reference.md#buildscriptclm) builds;
- temporary files used by Vim's `writebackup`: `*.dcl~`; `*.icl~`.

Also, the [`watch:exclude`](../nitrile-yml/reference.md#watchexclude) setting
in `nitrile.yml` may be used to exclude more files.

For [local dependencies](global-options.md#-local-dependency-dep-path), only
the `src` and `extra_src` fields are included, and `src` fields of
`clm_options` are excluded.

## Subcommands

The commands to be run when changes are detected can be specified with `--do`.
Any Nitrile subcommand (except `watch` itself) can be used, although it is most
sensible to use commands like [`build`](build.md), [`test`](test.md), and
[`generate-tags`](generate-tags.md). For example:

```bash
nitrile watch --do build
```

This option may be given multiple times:

```bash
nitrile watch --do generate-tags --do build
```

If any `--do` action fails, the remaining actions will not be executed.
However, Nitrile will continue to watch the source directory and restart the
sequence on changes.

### Subcommand arguments

Subcommands may have arguments:

```bash
nitrile watch --do build --only=main
```

The behaviour with [global options](global-options.md) requires some
explanation. When global options are used *before* the first `--do`, they apply
to all subcommands. The following command builds and runs tests for X86:

```bash
nitrile --arch=x86 watch --do build --do test
```

It is also possible to use global options for one particular subcommand. The
following first builds for X86-64, then for X86:

```bash
nitrile watch --do build --arch=x64 --do build --arch=x86
```

### Shell subcommands

You can also specify shell commands to be run. On Windows, these commands run
in PowerShell; on other systems, in `/bin/sh`.

```bash
nitrile watch --do build --do shell ./bin/main
```

Shell commands can be made 'cancellable' with `-c`. This is meant for programs
that do not terminate (or may run for a long time), such as servers. When a
command is cancellable, Nitrile will continue to watch for changes in the
background. When changes are detected, the shell command is terminated and all
commands in the chain are restarted:

```bash
nitrile watch --do build --do shell -c ./bin/main
```
