---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# `nitrile test`

This command tests the package according to the
[`tests`](../nitrile-yml/reference.md#tests) in `nitrile.yml`.

## Options

### `--except=CASE1[,CASE2,..]`

All tests except `CASE1`, `CASE2`, etc.

`--except` cannot be combined with `--only`.

### `--list`

Only print test names. The tests are not run.

### `--only=CASE1[,CASE2,..]`

Only test `CASE1`, `CASE2`, etc.

`--only` cannot be combined with `--except`.

### `--property-tester-options=OPTION[,OPTION,..]`

Adds options for property tests defined using
[`properties`](../nitrile-yml/reference.md#testsproperties). The following
options are supported:

- `compile-only`: only generate and compile the property tests to generate new
	test executables. The tests are not executed. By default, the property tests
	are executed.
- `no-regenerate`: only execute the test executables. The tests are not
	regenerated.
- `nr-of-tests:N`: perform a maximum of N tests for each property that is being
	tested. The default maximum number of tests to generate for each property is
	1000.
- `only:MODULE1[+MODULE2+..]`: only generate and run tests defined in the given
	modules. Multiple modules can be given using `+` as a separator.

`compile-only` and `no-regenerate` can be used to build an
[efficient CI pipeline](../packaging-and-publishing/gitlab-ci-examples.md#parallelizing-tests).

### `--test-runner-options`

Adds options for running
[property tests](../nitrile-yml/reference.md#testsproperties) and
[`test-runner` tests](../nitrile-yml/reference.md#buildscripttest-runner). The
following options are supported:

- `parallel:PROCESSINDEX:PROCESSTOTAL`: only perform the tests for which
	`TEST INDEX % PROCESSTOTAL = PROCESSINDEX`.
	[This is useful in CI](../packaging-and-publishing/gitlab-ci-examples.md#parallelizing-tests).
