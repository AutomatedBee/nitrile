---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# `nitrile init`

This command can be used to create a simple `nitrile.yml` file.

This is an interactive command: it asks the user a series of questions on the
command line.
