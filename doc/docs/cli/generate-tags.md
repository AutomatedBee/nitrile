---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# `nitrile generate-tags`

This command writes [ctags][] for the `src` paths in the [extended ctags][]
format to `.nitrile-tags`.

This file can be used for autocompletion in your editor.

[ctags]: https://en.wikipedia.org/wiki/Ctags
[extended ctags]: https://en.wikipedia.org/wiki/Ctags#Extended_Ctags
