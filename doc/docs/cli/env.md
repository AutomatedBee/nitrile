---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# `nitrile env`

This command outputs some useful environment variable settings.

!!! note
	[Optional dependencies](../nitrile-yml/reference.md#dependenciesoptional)
	are treated as required. This means that all dependencies must be installed,
	and that their library paths are included in the `CLMFLAGS` even when they
	are optional.

## Usage

The output can be fed to `eval` in bash to set the settings in the current
process:

```bash
eval `nitrile env`
```

In PowerShell, you can use `Invoke-Expression`:

```powershell
nitrile env | Invoke-Expression
```

## Use case

A typical use case is to do some simple tests using `clm`. For example, you can
create a temporary `temp.icl` and without adding a
[`build`](../nitrile-yml/reference.md#build) step compile and run it with:

```bash
eval `nitrile env`
clm $CLMFLAGS temp -o temp
./temp
```

## Defined variables

The following variables are defined:

- `CLEAN_HOME`: points to a central directory in `nitrile-packages` with a
	`bin` and `exe` subdirectory containing links or copies to the binaries of
	all dependencies.
- `CLEANLIB`: identical to `$CLEAN_HOME/exe`, used by `clm`.
- `CLMFLAGS`: the flags nitrile would use to invoke `clm`.
- `PATH`: identical to the previous `PATH`, but with `$CLEAN_HOME/bin`
	prepended (if it was not already at the start of `PATH`).
