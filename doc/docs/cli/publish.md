---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# `nitrile publish`

This command publishes a [packaged](package.md) project to the [registry][].

You need to specify the targets you want to publish, for example:

```txt
$ nitrile publish --targets=linux-x64,linux-x86,windows-x64
```

Concretely, it performs the following steps:

- Create a `nitrile-metadata.json` with information used by the registry.

- Upload `nitrile-metadata.json` and the package tarballs from
	[`nitrile package`](package.md) to a
	[GitLab generic package](https://docs.gitlab.com/ee/user/packages/generic_packages/index.html).

- Ping the [registry][] to fetch new information about the package.

This command should only be used in GitLab CI, because it depends on variables
that are set here.

Typically, you should run `nitrile publish` in a last, separate stage in CI,
which gets the package files as CI artifacts from previous stages. See the
[CI configuration examples](/packaging-and-publishing/gitlab-ci-examples.md).
However, if there is only one build step you could also run `nitrile publish`
in that job.

[registry]: https://clean-lang.org
