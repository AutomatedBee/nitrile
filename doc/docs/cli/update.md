---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# `nitrile update`

This command updates your local copy of the registry in `~/.nitrile`.

The registry contains information about all packages and versions, including
dependencies. This information is needed for [`nitrile fetch`](fetch.md).

It is recommended to run `nitrile update` before `nitrile fetch`, unless you
have recently updated already.
