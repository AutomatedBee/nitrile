---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Introduction to `nitrile.yml`

`nitrile.yml` defines your package. It is used by Nitrile to decide what to do
to build and package your application or library. It can be compared to
`package.json` for npm or `project.cabal` for Cabal, for example.

At the bare minimum it should contain the fields described in the
[getting started guide](../intro/getting-started.md#creating-and-building-projects).

Fore more fields, see the [reference](reference.md).

## Multiple packages

To specify multiple packages in one `nitrile.yml`, you can use multiple YAML
documents separated by `---`:

```yml
name: my-project
# ...

---

name: my-other-project
# ...
```

This is for example useful to have a separate package for an application and a
library (as is done for the
[iTasks compiler](https://gitlab.com/clean-and-itasks/base/compiler/-/tree/itasks)).

By default, Nitrile will use the first package, but you can control this with
the global option [`--name`](../cli/global-options.md#-namename).
