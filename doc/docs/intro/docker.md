---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Docker images

Nitrile is bundled as a Docker image and published on the Docker Hub as
[`cleanlang/nitrile`](https://hub.docker.com/r/cleanlang/nitrile).

The images are based on slim versions of Debian images (e.g.
`debian:bullseye-slim`). The following tags are available:

- `X.Y.Z-RELEASE`, where `X.Y.Z` is a Nitrile version and `RELEASE` a Debian
  release (e.g. `bullseye`). There are no images based on releases before
  bullseye, and the stable release is the latest supported release.
- `X.Y-RELEASE` links to the latest patch version in `X.Y`.
- `X-RELEASE` links to the latest minor-patch version in `X` (only for `X`
  greater than 0).
- `X[.Y[.Z]]` links to `X[.Y[.Z]]-RELEASE` where `RELEASE` is the current
  stable release.
- `RELEASE-latest` links to the latest `X.Y.Z-RELEASE` with the same `RELEASE`.
- `latest` links to `RELEASE-latest` where `RELEASE` is the current release.

In CI and scripts you should fix the major version and the Debian version for
backwards compatibility, by using a `X-RELEASE` tag, or one that is more
restrictive.
