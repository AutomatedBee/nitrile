---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Getting started

[Having installed Nitrile](installation.md), you can use this short tutorial to
learn how to use Nitrile to manage a Clean project.

Nitrile works with projects, which are specified in a
[`nitrile.yml` file](../nitrile-yml/reference.md). Base packages (like the
compiler, code generator, and run time system) are not installed globally but
locally, in a subdirectory of a project. You do not have a global Clean
installation. This enables you to work with different versions of libraries and
applications (including the compiler) at the same time.

To get started, create a simple `nitrile.yml` file (or use
[`nitrile init`](/cli/init.md)):

```yml
name: my-project
type: Application
description: An example project.
version: 0.1.0
license: AGPL-3.0-only
url: https://gitlab.com/example/my-project
maintainer: My Name
contact_email: name@example.org
```

Also create the directory `src` and a main file `src/app.icl`:

```clean
module app

Start = "Hello, world!"
```

We need to tell Nitrile where to look for the source files. Add the following
to your `nitrile.yml`:

```yml
src:
  - src
build:
  app:
    script:
      - clm:
          main: app
          target: bin/app
```

If we now run `nitrile build`, we get:

```txt
$ nitrile build
/bin/sh: 1: clm: not found
```

We have forgotten to add `base` as a dependency! This is needed to have a
working compiler and build tools. Add the dependency to `nitrile.yml`:

```yml
dependencies:
  base: ^1.0.0
```

Now run:

```txt
$ nitrile update
$ nitrile fetch
```

The first gets the latest package information from the registry. The second
satisfies the dependencies and downloads them into `nitrile-packages`.

Now we can run `nitrile build` again:

```txt
$ nitrile build
Generating code for _system
Compiling app
Generating code for app
Linking app
```

And run our application:

```txt
$ ./bin/app
"Hello world!"
Execution: 0.00  Garbage collection: 0.00  Total: 0.00
```
