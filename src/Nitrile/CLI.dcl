definition module Nitrile.CLI

/**
 * Copyright 2021-2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError
from Data.Map import :: Map
from System.FilePath import :: FilePath

from Data.SemVer import :: Version
from Nitrile.CLI.Build import :: CLIBuildOptions
from Nitrile.CLI.Do import :: CLIDoOptions
from Nitrile.CLI.Fetch import :: CLIFetchOptions
from Nitrile.CLI.Global import :: CLIGlobalCommand
from Nitrile.CLI.Publish import :: CLIPublishOptions
from Nitrile.CLI.Run import :: CLIRunOptions
from Nitrile.CLI.Settings import :: GlobalSettings
from Nitrile.CLI.Test import :: CLITestOptions
from Nitrile.CLI.Util.DependencySources import :: DependencySource, :: DependencySources
from Nitrile.CLI.Watch import :: CLIWatchOptions
from Nitrile.Package import :: UsageScope
from Nitrile.Target import :: Target

:: GlobalOptions =
	{ name               :: !?String
	, target             :: !Target
	, verbose            :: !Bool
	, no_header          :: !Bool
	, clm_linker         :: !?String
	, parallel_jobs      :: !?Int //* The maximum number of jobs to run in parallel
	, local_dependencies :: !Map String FilePath
	, scope              :: !UsageScope
	}

:: CLICommand
	= Build !CLIBuildOptions
	| Do !CLIDoOptions
	| Env
	| Fetch !CLIFetchOptions
	| GenerateTags
	| Global !CLIGlobalCommand
	| Help !String
	| Init
	| Package
	| Publish !CLIPublishOptions
	| Run !CLIRunOptions
	| Terms
	| Test !CLITestOptions
	| Update
	| Watch !CLIWatchOptions

parseCommandLine :: !GlobalSettings ![String] -> MaybeError String (CLICommand, GlobalOptions)

performCommand :: !CLICommand !GlobalOptions !*World -> (!Bool, !*World)
