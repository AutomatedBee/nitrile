definition module Nitrile.Package

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError
from SPDX.License import :: License
from System.FilePath import :: FilePath
from Text.YAML import :: YAMLErrorWithLocations, :: YAMLNode, :: YAMLSchema, generic gConstructFromYAML

from Data.NamedList import :: NamedList
from Data.SemVer import :: Version
from Data.SemVer.Constraint import :: VersionConstraint
from Nitrile.Build import :: Build, :: BuildCommand, :: ClmBuildOptions, :: TestRunnerOutputFormat
from Nitrile.Target import :: ArchitectureFamily, :: Bitwidth, :: Platform, :: Target

:: Package =
	{ name               :: !?String
	, maintainer         :: !?String
	, contact_email      :: !?String
	, url                :: !?String
	, description        :: !?String
	, version            :: !?Version
	, license            :: !?License
	, type               :: !PackageType
	, dependencies       :: !DependencyList
	, src                :: !?[SourceDirectory]
	, clm_options        :: !?(ClmBuildOptions (?())) // ?() makes sure you cannot specify 'main'
	, build              :: !NamedList Build
	, package            :: !?PackageOptions
	, rules              :: !Rules
	, tests              :: !NamedList TestSpec
	, actions            :: !NamedList Action
	, watch              :: !?WatchOptions
	}

:: PackageType
	= Application
	| Library
	| Miscellaneous

:: DependencyList =: DependencyList [(String, Dependency)]

fromDependencyList :: !DependencyList -> [(String, Dependency)]

:: Dependency =
	{ version  :: !VersionConstraint
	, scope    :: !?UsageScope
	, optional :: !?Bool
	}

:: UsageScope = UseScope | BuildScope | TestScope

:: SourceDirectory =
	{ path  :: !FilePath
	, scope :: !?UsageScope
	}

:: PackageOptions =
	{ include :: !?[String]
		//* By default, only .dcl and .icl files from the src directories will
		//* be included when packaging a library. This option allows you to
		//* specify additional file names, which is useful for hand-written ABC
		//* or object files in Clean System Files that need to be included.
	, exclude :: !?[String]
		//* Files to exclude.
	, extra_files :: !?[CopyFileSettings]
		//* Extra file paths outside the src directories that need to be
		//* included.
	, core_modules :: !?[String]
		//* Patterns of modules that are library-internal, i.e. should not be
		//* used from outside this library.
	}

:: CopyFileSettings =
	{ src :: !FilePath
	, dst :: !FilePath
	}

:: Rules =: Rules [(RuleCondition, RuleSettings)]

:: RuleCondition
	= RCAnd !RuleCondition !RuleCondition
	| RCOr !RuleCondition !RuleCondition
	| RCArchitectureFamily !ArchitectureFamily
	| RCBitwidth !Bitwidth
	| RCPlatform !Platform

:: RuleSettings =
	{ extra_dependencies :: !DependencyList
	, extra_src          :: !?[SourceDirectory]
	, extra_build        :: !NamedList Build
	, extra_package      :: !?PackageOptions
	, extra_tests        :: !NamedList TestSpec
	, extra_actions      :: !NamedList Action
	}

:: TestSpec =
	{ depends               :: !?[String]
	, script                :: !?[BuildCommand]
	, expected_result       :: !?FilePath
	, properties            :: !?PropertyTesterOptions
	, compilation           :: !?CompilationTestOptions
	, required_dependencies :: !?[String]
	}

:: PropertyTesterOptions =
	{ test_options  :: !?[String] // for Gast's Testoption type
	, clm_options   :: !?(ClmBuildOptions (?())) // allows main to be unset
	, output_format :: !?TestRunnerOutputFormat
	, junit_xml     :: !?FilePath
	}

:: CompilationTestOptions =
	{ clm_options    :: !?(ClmBuildOptions (?())) // allows main to be unset
	, except_modules :: !?[String]
	, only_modules   :: !?[String]
	}

:: Action =
	{ script :: ![String]
	}

:: WatchOptions =
	{ exclude :: !?[String]
	}

derive gConstructFromYAML Package, Version

dependenciesInScope :: !UsageScope !Package -> NamedList (Bool, VersionConstraint)

srcInScope :: !UsageScope !Package -> [FilePath]

evaluateCondition :: !Target !RuleCondition -> Bool

/**
 * This performs a number of operations:
 *
 * - Uses rule settings like `extra_build` for rules matching the given target
 *   to update fields like `build`.
 * - Applies the global `clm_options` anywhere where local options are
 *   specified or needed.
 * - On Windows, translates file paths to use Windows path separators.
 *
 * In the case of `extra_src`, `extra_src` fields from later `rules` are added
 * before those from earlier `rules`, so that later rules take precedence. For
 * example, with `src = [A, B]`, a first rule with `extra_src = [C, D]`, and a
 * second rule with `extra_src = [E, F]`, the result will have
 * `src = [E, F, C, D, A, B]`.
 *
 * In the case of other fields (`extra_build`, `extra_tests`), the order of the
 * elements in `build` / `tests` matches that in the YAML. For
 * `extra_dependencies` and `extra_package`, order is irrelevant.
 */
normalizePackage :: !Target !Package -> Package

/**
 * This does some sanity checks on the package. This function should be used
 * after `normalizePackage`.
 *
 * - Checks whether `required_dependencies` of `build` and `tests` jobs are
 *   mentioned in the `dependencies` field.
 */
verifyPackage :: !Package -> MaybeError [String] ()

markAllDependenciesAsNonOptional :: !Package -> Package

markDependenciesAsNonOptional :: ![String] !Package -> Package
