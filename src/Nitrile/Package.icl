implementation module Nitrile.Package

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Control.Applicative
import Control.Monad
import Data.Bifunctor
import Data.Error
import Data.Func
import Data.Functor
import Data.List
import Data.Maybe
from Data.Set import :: Set
import qualified Data.Set
import Data.Tuple
import SPDX.License
import StdEnv
import System.FilePath
import System.OS
from Text import class Text(split), instance Text String, concat3, concat4
import Text.YAML
import Text.YAML.Compose
import Text.YAML.Construct

import Data.NamedList
import Data.Nullable
import Data.SemVer
import Data.SemVer.Constraint
import Nitrile.Build
import Nitrile.Package.Migrate
import Nitrile.Target

fromDependencyList :: !DependencyList -> [(String, Dependency)]
fromDependencyList (DependencyList deps) = deps

derive gConstructFromYAML Action, Build, ClmBuildOptions,
	CompilationTestOptions, Fusion, GarbageCollector, PackageOptions,
	PackageType, Platform, Profiling, PropertyTesterOptions, RuleSettings,
	TestRunnerOptions, TestRunnerOutputFormat, WatchOptions

gConstructFromYAML{|Package|} schema in_record [node] =
	migrate schema node >>= \node ->
	gConstructFromYAML schema in_record [node] >>= \res=:(pkg,_) ->
	bifmap
		(withoutErrorLocations o InvalidContent)
		(const res)
		(checkPackage pkg)
where
	derive gConstructFromYAML Package

	checkPackage {name,type,src} = case catMaybes errors of
		[] -> Ok ()
		[e:_] -> Error e
	where
		errors =
			[ checkName =<< name
			, checkSrc type =<< src
			]

	checkName name
		| size name < 1
			= ?Just "package name may not be empty"
		| not (isAlpha name.[0])
			= ?Just "package name must start with a letter"
		| not (isAlphanum name.[size name-1])
			= ?Just "package name must end with a letter or digit"
		| not (all valid [c \\ c <-: name])
			= ?Just "package name may contain only lowercase letters, digits, and hyphens"
			= ?None
	where
		valid c = ('a' <= c && c <= 'z') || isDigit c || c == '-'

	checkSrc type src
		| type=:Application && any (\src -> src.SourceDirectory.scope=:(?Just UseScope)) src
			= ?Just "source directories of applications cannot have scope: Use"
			= ?None

local_gConstructFromYAML = gConstructFromYAML{|*|}

gConstructFromYAML{|DependencyList|} schema _ nodes = case nodes of
	[{content=YAMLScalar _ ""}:rest] ->
		Ok (DependencyList [], rest)
	[{content=YAMLMapping kvs}:rest] ->
		parseList [] 0 kvs <$&> \kvs -> (DependencyList kvs, rest)
	_ ->
		Error (withoutErrorLocations (InvalidContent "expected mapping"))
where
	parseList settings _ [] = Ok []
	parseList settings i [{key={content=YAMLMapping extra_settings},value={content=YAMLMapping children}}:kvs]
		| not (isEmpty (intersect [key \\ {key} <- settings] [key \\ {key} <- extra_settings])) =
			Error (pushErrorLocation (SequenceIndex i) (withoutErrorLocations
				(InvalidContent "duplicate setting specification in mapping shorthand")))
		| any (\{key} -> (constructFromYAML schema key)=:(Ok "version")) extra_settings =
			Error (pushErrorLocation (SequenceIndex i) (withoutErrorLocations
				(InvalidContent "mapping shorthand cannot set version")))
		| otherwise =
			mapError (pushErrorLocation (SequenceIndex i)) (parseList (settings ++ extra_settings) 0 children) >>= \children ->
			parseList settings (i+1) kvs >>= \kvs ->
			Ok (children ++ kvs)
	parseList settings i [{key,value}:kvs] =
		mapError (pushErrorLocation (SequenceIndex i)) (constructFromYAML schema key) >>= \key ->
		mapError (pushErrorLocation (Field key)) (parseDependency settings value) >>= \value ->
		parseList settings (i+1) kvs >>= \kvs ->
		Ok [(key,value):kvs]

	parseDependency settings node=:{content=YAMLMapping kvs}
		| not (isEmpty (intersect [key \\ {key} <- settings] [key \\ {key} <- kvs])) =
			Error (withoutErrorLocations (InvalidContent "duplicate setting specification in mapping shorthand"))
		| otherwise =
			constructFromYAML schema {node & content=YAMLMapping (kvs ++ settings)}
	parseDependency settings node =
		constructFromYAML schema (simpleNode (YAMLMapping [{key=simpleNode (YAMLScalar Plain "version"),value=node}:settings]))
	where
		simpleNode content = {properties={tag="?", anchor= ?None}, content=content}

gConstructFromYAML{|Dependency|} schema in_record nodes =
	case local_gConstructFromYAML schema in_record nodes of
		Ok (version, rest) ->
			Ok (withDefaultOptions version, rest)
		_ ->
			gConstructFromYAML schema in_record nodes
where
	derive gConstructFromYAML Dependency

	withDefaultOptions version =
		{ Dependency
		| version  = version
		, scope    = ?None
		, optional = ?None
		}

gConstructFromYAML{|UsageScope|} _ _ nodes = case nodes of
	[{content=YAMLScalar _ value}:rest] -> case value of
		"Use" -> Ok (UseScope, rest)
		"Build" -> Ok (BuildScope, rest)
		"Test" -> Ok (TestScope, rest)
		_ -> Error $ withoutErrorLocations $
			InvalidContent (concat3 "unknown value '" value "' for scope")
	_ -> Error $ withoutErrorLocations $
		InvalidContent "expected scalar for scope"

gConstructFromYAML{|SourceDirectory|} schema in_record nodes =
	case local_gConstructFromYAML schema in_record nodes of
		Ok (path, rest) ->
			Ok (withDefaultOptions path, rest)
		_ ->
			gConstructFromYAML schema in_record nodes
where
	derive gConstructFromYAML SourceDirectory

	withDefaultOptions path =
		{ path = path
		, scope = ?None
		}

gConstructFromYAML{|ListTypes|} _ _ nodes = case nodes of
	[{content=YAMLScalar _ value}:rest] -> case value of
		"None" -> Ok (ListTypesNone, rest)
		"Inferred" -> Ok (ListTypesInferred, rest)
		"LackingStrictness" -> Ok (ListTypesLackingStrictness, rest)
		"All" -> Ok (ListTypesAll, rest)
		_ -> Error $ withoutErrorLocations $
			InvalidContent (concat3 "unknown value '" value "' for list_types")
	_ -> Error $ withoutErrorLocations $
		InvalidContent "expected scalar for list_types"

gConstructFromYAML{|PartialFunctionHandling|} _ _ nodes = case nodes of
	[{content=YAMLScalar _ value}:rest] -> case value of
		"Ignore" -> Ok (IgnorePartialFunctions, rest)
		"Warning" -> Ok (WarningPartialFunctions, rest)
		"Error" -> Ok (ErrorPartialFunctions, rest)
		_ -> Error $ withoutErrorLocations $
			InvalidContent ("unknown value '" +++ value +++ "' for partial_functions")
	_ -> Error $ withoutErrorLocations $
		InvalidContent "expected scalar for partial_functions"

gConstructFromYAML{|ByteCodeOptions|} schema in_record nodes = case nodes of
	[{content=YAMLScalar _ "prelinked"}:rest] ->
		Ok ({optimize_abc= ?Just True, prelink= ?Just True}, rest)
	[bool=:{content=YAMLScalar _ _}:rest] ->
		constructFromYAML schema bool >>= \b
			| b ->
				Ok ({optimize_abc= ?Just True, prelink= ?Just False}, rest)
			| otherwise ->
				Error $ withoutErrorLocations $ InvalidContent
					"uninterpretable value 'false' for bytecode options"
	_ ->
		gConstructFromYAML schema in_record nodes
where
	derive gConstructFromYAML ByteCodeOptions

gConstructFromYAML{|License|} _ _ nodes = case nodes of
	[{content=YAMLScalar _ value}:rest] ->
		bifmap
			(withoutErrorLocations o InvalidContent)
			(flip tuple rest)
			(mb2error "unknown license identifier" (fromLicenseId value))
	_ ->
		Error $ withoutErrorLocations $
			InvalidContent "expected scalar for version"

gConstructFromYAML{|Version|} _ _ nodes = case nodes of
	[{content=YAMLScalar _ value}:rest] ->
		bifmap
			(withoutErrorLocations o InvalidContent)
			(flip tuple rest)
			(parseVersion value)
	_ ->
		Error $ withoutErrorLocations $
			InvalidContent "expected scalar for version"

gConstructFromYAML{|VersionConstraint|} _ _ nodes = case nodes of
	[{content=YAMLScalar _ value}:rest] ->
		bifmap
			(withoutErrorLocations o InvalidContent)
			(flip tuple rest)
			(parseVersionConstraint value)
	_ ->
		Error $ withoutErrorLocations $
			InvalidContent "expected scalar for version constraint"

gConstructFromYAML{|BuildCommand|} schema _ nodes = case nodes of
	[{content=YAMLScalar _ value}:rest] ->
		Ok (SystemCommand value, rest)
	[{content=YAMLMapping [{key={content=YAMLScalar _ "clm"},value}]}:rest] ->
		bifmap
			(pushErrorLocation (ADT "Build") o pushErrorLocation (ADT "Clm"))
			(\options -> (Clm options, rest))
			(constructFromYAML schema value)
	[{content=YAMLMapping [{key={content=YAMLScalar _ "test-runner"},value}]}:rest] ->
		bifmap
			(pushErrorLocation (ADT "Build") o pushErrorLocation (ADT "TestRunner"))
			(\options -> (TestRunner options, rest))
			(constructFromYAML schema value)
	_ ->
		Error $ withoutErrorLocations $
			InvalidContent "invalid build command"

gConstructFromYAML{|TestRunnerTest|} schema in_record nodes = case nodes of
	[exe=:{content=YAMLScalar _ _}:rest] ->
		(\exe -> ({executable=exe, options= ?None}, rest)) <$>
		constructFromYAML schema exe
	_ ->
		gConstructFromYAML schema in_record nodes
where
	derive gConstructFromYAML TestRunnerTest

gConstructFromYAML{|CopyFileSettings|} schema in_record nodes = case nodes of
	[{content=YAMLScalar _ value}:rest] ->
		Ok ({src=value, dst=value}, rest) // shorthand when src=dst
	nodes ->
		gConstructFromYAML schema in_record nodes
where
	derive gConstructFromYAML CopyFileSettings

gConstructFromYAML{|Rules|} schema _ nodes = case nodes of
	[{content=YAMLMapping rules}:rest] ->
		bifmap
			(pushErrorLocation (ADT "Rules"))
			(\rules -> (Rules rules, rest))
			(sequence
				[ first
					(pushErrorLocation (Field (case key.content of YAMLScalar _ v -> v; _ -> "???")))
					(liftM2 tuple (constructFromYAML schema key) (constructFromYAML schema value))
				\\ {key,value} <- rules // TODO push sequence index error location
				])
	[{content=YAMLScalar Plain ""}:rest] -> /* no value specified */
		Ok (Rules [], rest)
	_ ->
		Error $ withoutErrorLocations $
			InvalidContent "expected mapping for rules"

gConstructFromYAML{|RuleCondition|} _ _ nodes = case nodes of
	[{content=YAMLScalar _ value}:rest] ->
		bifmap
			(pushErrorLocation (ADT "RuleCondition") o withoutErrorLocations o InvalidContent)
			(flip tuple rest)
			(parse (split " " value))
	_ ->
		Error $ withoutErrorLocations $
			InvalidContent "expected scalar for rule condition"
where
	parse [c,"and":rest] = liftM2 RCAnd (parseOne c) (parse rest)
	parse [c] = parseOne c
	parse _ = Error "failed to parse condition"

	parseOne "posix" = Ok $ RCOr (RCPlatform Linux) (RCPlatform Mac)
	parseOne cond = case parsePlatform cond of
		Ok platform | not (platform=:AnyPlatform) ->
			Ok $ RCPlatform platform
		_ -> case parseArchitecture cond of
			Ok {family=AnyArchitectureFamily, bitwidth=AnyBitwidth} ->
				Error ("unknown condition " +++ cond)
			Ok {family=AnyArchitectureFamily, bitwidth} ->
				Ok $ RCBitwidth bitwidth
			Ok {family, bitwidth=AnyBitwidth} ->
				Ok $ RCArchitectureFamily family
			Ok {family, bitwidth} ->
				Ok $ RCAnd (RCArchitectureFamily family) (RCBitwidth bitwidth)
			_ ->
				Error ("unknown condition " +++ cond)

gConstructFromYAML{|TestSpec|} schema in_record nodes =
	gConstructFromYAML schema in_record nodes >>= \result=:(testSpec, _) ->
	bifmap
		(withoutErrorLocations o InvalidContent)
		(const result)
		(check testSpec)
where
	derive gConstructFromYAML TestSpec

	check {script,expected_result,properties,compilation}
		| length (filter id [hasScript, hasProperties, hasCompilation]) <> 1
			= Error "precisely one of script, properties, and compilation must be given"
		| hasScript
			| isEmpty (fromJust script)
				= Error "script cannot be empty"
				= Ok ()
		| isJust expected_result
			= Error "expected_result must be used with script"
			= Ok ()
	where
		hasScript = isJust script
		hasExpectedResult = isJust expected_result
		hasProperties = isJust properties
		hasCompilation = isJust compilation

defaultScope :: !Package -> UsageScope
defaultScope pkg = case pkg.type of
	Application -> BuildScope
	Library -> UseScope
	Miscellaneous -> UseScope

(matches) scope UseScope = scope=:UseScope
(matches) scope BuildScope = scope=:UseScope || scope=:BuildScope
(matches) scope TestScope = True

dependenciesInScope :: !UsageScope !Package -> NamedList (Bool, VersionConstraint)
dependenciesInScope requested pkg=:{Package|dependencies=DependencyList deps} = NamedList
	[ (pkg, (fromMaybe False optional, version))
	\\ (pkg,{version,scope,optional}) <- deps
	| fromMaybe default scope matches requested
	]
where
	default = defaultScope pkg

srcInScope :: !UsageScope !Package -> [FilePath]
srcInScope requested pkg =
	[ path
	\\ {path,scope} <- fromMaybe [] pkg.Package.src
	| fromMaybe default scope matches requested
	]
where
	default = defaultScope pkg

evaluateCondition :: !Target !RuleCondition -> Bool
evaluateCondition target c = eval c
where
	eval (RCAnd c1 c2) = eval c1 && eval c2
	eval (RCOr c1 c2) = eval c1 || eval c2
	eval (RCArchitectureFamily family) = architectureFamilyMatches target.architecture.family family
	eval (RCBitwidth bw) = bitwidthMatches target.architecture.bitwidth bw
	eval (RCPlatform platform) = platformMatches target.platform platform

resolveRules :: !Target !Package -> Package
resolveRules target pkg=:{dependencies,src,clm_options,build,package,tests,actions,rules=Rules rules} =
	{ pkg
	& dependencies = DependencyList (
			fromDependencyList dependencies ++
			flatten [ds \\ {extra_dependencies=DependencyList ds} <- settings]
		)
	, src = ?Just $
		flatten [extra_src \\ {extra_src = ?Just extra_src} <- reverse settings] ++
		fromMaybe [] src
	, build = NamedList (
			fromNamedList build ++
			flatten [bs \\ {extra_build=NamedList bs} <- settings]
		)
	, package = ?Just
		{ package`
		& include = ?Just $
			fromMaybe [] package`.include ++
			flatten [fs \\ {extra_package = ?Just {include = ?Just fs}} <- settings]
		, exclude = ?Just $
			fromMaybe [] package`.PackageOptions.exclude ++
			flatten [fs \\ {extra_package = ?Just {PackageOptions | exclude = ?Just fs}} <- settings]
		, extra_files = ?Just $
			fromMaybe [] package`.extra_files ++
			flatten [fs \\ {extra_package = ?Just {extra_files = ?Just fs}} <- settings]
		}
	, tests = NamedList (
			fromNamedList tests ++
			flatten [ts \\ {extra_tests=NamedList ts} <- settings]
		)
	, actions = NamedList (
			fromNamedList actions ++
			flatten [as \\ {extra_actions=NamedList as} <- settings]
		)
	}
where
	package` = fromMaybe defaultPackageOptions package
	defaultPackageOptions =
		{ include = ?None
		, exclude = ?None
		, extra_files = ?None
		, core_modules = ?None
		}

	settings = [settings \\ (cond, settings) <- rules | evaluateCondition target cond]

:: UpdatePackageOptions = !
	{ clm_options :: !?(ClmBuildOptions (?()))
	}

generic gUpdatePackage a :: UpdatePackageOptions !a -> a
gUpdatePackage{|Bool|} _ x = x
gUpdatePackage{|Int|} _ x = x
gUpdatePackage{|String|} _ x = x
gUpdatePackage{|UNIT|} _ x = x
gUpdatePackage{|PAIR|} fx fy opts (PAIR x y) = PAIR (fx opts x) (fy opts y)
gUpdatePackage{|EITHER|} fl fr opts (LEFT x) = LEFT (fl opts x)
gUpdatePackage{|EITHER|} fl fr opts (RIGHT x) = RIGHT (fr opts x)
gUpdatePackage{|CONS|} f opts (CONS x) = CONS (f opts x)
gUpdatePackage{|FIELD|} f opts (FIELD x) = FIELD (f opts x)
gUpdatePackage{|OBJECT|} f opts (OBJECT x) = OBJECT (f opts x)
gUpdatePackage{|RECORD|} f opts (RECORD x) = RECORD (f opts x)

derive gUpdatePackage (), ?, [], (,), NamedList, Nullable

derive gUpdatePackage Action, Architecture, ArchitectureFamily, Bitwidth,
	Build, BuildCommand, ByteCodeOptions, Dependency, DependencyList, Fusion,
	GarbageCollector, ListTypes, Package, PackageType, PartialFunctionHandling,
	Platform, Profiling, Rules, RuleCondition, RuleSettings,
	TestRunnerOutputFormat, UsageScope, Version, VersionConstraint,
	VersionConstraintPart, WatchOptions

gUpdatePackage{|License|} _ x = x

translatePath path = IF_WINDOWS
	{if (c == '/') pathSeparator c \\ c <-: path}
	path
addExe path = IF_WINDOWS
	(if (takeExtension path == "exe") path (path <.> "exe"))
	path

gUpdatePackage{|ClmBuildOptions|} fx opts settings =
	maybe id combineClmBuildOptions opts.UpdatePackageOptions.clm_options
	{ gUpdatePackage fx opts settings
	& target = addExe <$> translatePath <$> settings.target
	, compiler = addExe <$> settings.compiler
	, post_link = fmap addExe <$> settings.post_link
	, src = map translatePath <$> settings.ClmBuildOptions.src
	, link = map translatePath <$> settings.link
	}
where
	derive gUpdatePackage ClmBuildOptions

gUpdatePackage{|CompilationTestOptions|} opts settings
	# settings = gUpdatePackage opts settings
	  settings & CompilationTestOptions.clm_options =
		settings.CompilationTestOptions.clm_options <|> opts.UpdatePackageOptions.clm_options
	= settings
where
	derive gUpdatePackage CompilationTestOptions

gUpdatePackage{|CopyFileSettings|} _ {src,dst} =
	{ src = translatePath src
	, dst = translatePath dst
	}

gUpdatePackage{|PackageOptions|} opts settings =
	{ PackageOptions
	| gUpdatePackage opts settings
	& exclude = map translatePath <$> settings.PackageOptions.exclude
	}
where
	derive gUpdatePackage PackageOptions

gUpdatePackage{|PropertyTesterOptions|} opts settings
	# settings = gUpdatePackage opts settings
	  settings & PropertyTesterOptions.junit_xml = translatePath <$> settings.PropertyTesterOptions.junit_xml
	  settings & PropertyTesterOptions.clm_options = settings.PropertyTesterOptions.clm_options <|> opts.UpdatePackageOptions.clm_options
	= settings
where
	derive gUpdatePackage PropertyTesterOptions

gUpdatePackage{|SourceDirectory|} opts dir =
	{ gUpdatePackage opts dir
	& path = translatePath dir.path
	}
where
	derive gUpdatePackage SourceDirectory

gUpdatePackage{|TestRunnerOptions|} opts settings =
	{ TestRunnerOptions
	| gUpdatePackage opts settings
	& junit_xml = translatePath <$> settings.TestRunnerOptions.junit_xml
	}
where
	derive gUpdatePackage TestRunnerOptions

gUpdatePackage{|TestRunnerTest|} opts settings =
	{ gUpdatePackage opts settings
	& executable = translatePath settings.executable
	}
where
	derive gUpdatePackage TestRunnerTest

gUpdatePackage{|TestSpec|} opts settings =
	{ gUpdatePackage opts settings
	& expected_result = translatePath <$> settings.expected_result
	}
where
	derive gUpdatePackage TestSpec

normalizePackage :: !Target !Package -> Package
normalizePackage target pkg
	// We first need to update the `clm_options`, so that paths there are
	// translated if needed:
	# clm_options = gUpdatePackage{|*|}
		{UpdatePackageOptions | clm_options = ?None}
		pkg.Package.clm_options
	// Now we can use the new `clm_options` to update the entire package:
	# pkg = gUpdatePackage{|*|}
		{UpdatePackageOptions | clm_options = clm_options}
		pkg
	// Finally we resolve rules:
	# pkg = resolveRules target pkg
	= pkg

verifyPackage :: !Package -> MaybeError [String] ()
verifyPackage pkg
	| not (isEmpty missingRequiredDependencies)
		= Error missingRequiredDependencies
		= Ok ()
where
	optionalDependencies = 'Data.Set'.fromList
		[ name
		\\ (name,dep) <- fromDependencyList pkg.dependencies
		| fromMaybe False dep.optional
		]

	missingRequiredDependencies = flatten $
		[checkRequiredDependencies "Build job " name build.Build.required_dependencies \\ (name,build) <- fromNamedList pkg.Package.build] ++
		[checkRequiredDependencies "Test job " name test.TestSpec.required_dependencies \\ (name,test) <- fromNamedList pkg.Package.tests]

	checkRequiredDependencies :: !String !String !(?[String]) -> [String]
	checkRequiredDependencies type name ?None = []
	checkRequiredDependencies type name (?Just rdeps) =
		[ concat4 type name ": required_dependencies lists unknown optional dependency " rdep
		\\ rdep <- rdeps | 'Data.Set'.notMember rdep optionalDependencies
		]

markAllDependenciesAsNonOptional :: !Package -> Package
markAllDependenciesAsNonOptional pkg =
	{ Package
	| pkg
	& dependencies = DependencyList
		[ (name, {dep & optional = ?Just False})
		\\ (name,dep) <- fromDependencyList pkg.dependencies
		]
	}

markDependenciesAsNonOptional :: ![String] !Package -> Package
markDependenciesAsNonOptional dependencies pkg =
	{ Package
	| pkg
	& dependencies = DependencyList
		[ (name, if (isMember name dependencies) {dep & optional= ?Just False} dep)
		\\ (name,dep) <- fromDependencyList pkg.Package.dependencies
		]
	}
