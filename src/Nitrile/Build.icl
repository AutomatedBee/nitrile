implementation module Nitrile.Build

/**
 * Copyright 2021-2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Control.Applicative
import Control.Monad
import Data.Error
import Data.Func
import Data.Functor
import Data.Maybe
import StdEnv
import System.FilePath
import System.OS
from Text import class Text(replaceSubString,startsWith), instance Text String,
	concat3
import qualified Text

import Data.NamedList
import Data.Nullable
import Data.SemVer
import Data.SemVer.Constraint

defaultClmBuildOptions :: !main -> ClmBuildOptions main
defaultClmBuildOptions main =
	{ main                  = main
	, target                = ?None
	, src                   = ?None
	, compiler              = ?None
	, compiler_options      = ?None
	, undecidable_instances = ?None
	, check_indices         = ?None
	, partial_functions     = ?None
	, profiling             = ?None
	, generate_descriptors  = ?None
	, export_local_labels   = ?None
	, bytecode              = ?None
	, list_types            = ?None
	, warnings              = ?None
	, fusion                = ?None
	, strip                 = ?None
	, link                  = ?None
	, post_link             = ?None
	, heap                  = ?None
	, stack                 = ?None
	, print_constructors    = ?None
	, print_result          = ?None
	, print_time            = ?None
	, wait_for_key_press    = ?None
	, garbage_collector     = ?None
	}

combineClmBuildOptions :: !(ClmBuildOptions a) !(ClmBuildOptions b) -> ClmBuildOptions b
combineClmBuildOptions default overriding =
	{ main                  = overriding.main
	, target                = overriding.target <|> default.target
	, src                   = overriding.src <|> default.src
	, compiler              = overriding.compiler <|> default.compiler
	, compiler_options      = overriding.compiler_options <|> default.compiler_options
	, undecidable_instances = overriding.undecidable_instances <|> default.undecidable_instances
	, check_indices         = overriding.check_indices <|> default.check_indices
	, partial_functions     = overriding.partial_functions <|> default.partial_functions
	, profiling             = overriding.profiling <|> default.profiling
	, generate_descriptors  = overriding.generate_descriptors <|> default.generate_descriptors
	, export_local_labels   = overriding.export_local_labels <|> default.export_local_labels
	, bytecode              = overriding.bytecode <|> default.bytecode
	, list_types            = overriding.list_types <|> default.list_types
	, warnings              = overriding.warnings <|> default.warnings
	, fusion                = overriding.fusion <|> default.fusion
	, strip                 = overriding.strip <|> default.strip
	, link                  = overriding.link <|> default.link
	, post_link             = overriding.post_link <|> default.post_link
	, heap                  = overriding.heap <|> default.heap
	, stack                 = overriding.stack <|> default.stack
	, print_constructors    = overriding.print_constructors <|> default.print_constructors
	, print_result          = overriding.print_result <|> default.print_result
	, print_time            = overriding.print_time <|> default.print_time
	, wait_for_key_press    = overriding.wait_for_key_press <|> default.wait_for_key_press
	, garbage_collector     = overriding.garbage_collector <|> default.garbage_collector
	}

toClmOptions :: !Version !(?String) !(?Int) ![FilePath] !(ClmBuildOptions m) -> MaybeError String ([String], [String], [String], [String])
toClmOptions version clm_linker max_parallel_jobs dirs options
	| fromMaybe False options.undecidable_instances && not (version satisfies VersionConstraint [VersionGe {major=1,minor=5,patch=0}])
		= Error "clm_option undecidable_instances requires clm version >= 1.5"
	| otherwise
		= Ok
			( configuration_options
			, path_options
			, code_options
			, application_options
			)
where
	configuration_options = flatten
		[ if (IF_WINDOWS False True)
			(maybe [] (\linker -> if (linker == "ld") [] ["-l", "-fuse-ld=" +++ linker]) clm_linker)
			[]
		, if (IF_WINDOWS False (version satisfies VersionConstraint [VersionGe {major=1,minor=4,patch=0}]))
			(maybe ["-j"] (\n -> ["-j", toString n]) max_parallel_jobs)
			[]
		]

	path_options = flatten [["-I", dir] \\ dir <- fromMaybe [] options.ClmBuildOptions.src ++ dirs]

	code_options = flatten $ catMaybes
		[ ?Just ["-dynamics"]
		, (\cocl -> ["-clc", cocl]) <$> options.compiler
		, IF_WINDOWS (map \s -> concat3 "'" s "'") id <$>
			(\opts -> ['Text'.join "," ["-aC":opts]]) <$> options.compiler_options
		, if (fromMaybe False options.undecidable_instances) (?Just ["-aui"]) ?None
		, if` "-ci" "-nci" <$> options.check_indices
		, case fromMaybe IgnorePartialFunctions options.partial_functions of
			IgnorePartialFunctions -> ?None
			WarningPartialFunctions -> ?Just ["-warnfuncmayfail"]
			ErrorPartialFunctions -> ?Just ["-funcmayfail"]
		, case fromMaybe NoProfiling options.profiling of
			NoProfiling -> ?Just ["-npt", "-npg"]
			StackTracing -> ?Just ["-tst", "-ns"]
			TimeProfiling -> ?Just ["-pt", "-ns"]
			CallgraphProfiling -> ?Just ["-pg", "-ns"]
		, if (fromMaybe False options.generate_descriptors) (?Just ["-desc"]) ?None
		, if (fromMaybe False options.export_local_labels) (?Just ["-exl"]) ?None
		, case options.bytecode of
			?None -> ?None
			?Just Null -> ?None
			?Just (NonNull bc) -> ?Just $ catMaybes
				[ if (fromMaybe True bc.optimize_abc) (?Just "-optabc") ?None
				, ?Just "-bytecode"
				, if (fromMaybe False bc.prelink) (?Just "-prelink-bytecode") ?None
				]
		, case options.list_types of
			?None -> ?None
			?Just ListTypesNone -> ?None
			?Just ListTypesInferred -> ?Just ["-lt"]
			?Just ListTypesLackingStrictness -> ?Just ["-lset"]
			?Just ListTypesAll -> ?Just ["-lat"]
		, if` "-w" "-nw" <$> options.warnings
		, ?Just ["-Pfusion", "-Pgeneric_fusion"]
		, case fromMaybe NoFusion options.fusion of
			NoFusion -> ?Just ["-nfusion", "-ngeneric_fusion"]
			PlainFusion -> ?Just ["-fusion", "-ngeneric_fusion"]
			GenericFusion -> ?Just ["-nfusion", "-generic_fusion"]
			AllFusion -> ?Just ["-fusion", "-generic_fusion"]
		]

	application_options = flatten $ catMaybes $
		[ if (options.strip == ?Just False) (?Just ["-ns"]) ?None
		] ++
		[ ?Just ["-l", path]
		\\ path <- fromMaybe [] options.link
		] ++
		[ fmap (\pl -> ["-post-link", pl]) (join (toMaybe <$> options.post_link))
		, fmap (\h -> ["-h", h]) options.heap
		, fmap (\s -> ["-s", s]) options.stack
		, case options.print_result of
			?Just False -> ?Just ["-nr"]
			_ -> case options.print_constructors of
				?Just False -> ?Just ["-b"]
				_ -> ?Just ["-sc"]
		, if` "-t" "-nt" <$> options.print_time
		, IF_WINDOWS (if (options.wait_for_key_press == ?Just True) ?None (?Just ["-con"])) ?None
		, (\gc -> case gc of CopyingCollector -> ["-gcc"]; MarkingCollector -> ["-gcm"]) <$> options.garbage_collector
		]

	if` t e b = if b [t] [e]
