implementation module Nitrile.Package.Migrate

/**
 * Copyright 2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Control.Monad
import Data.Bifunctor
import Data.Error
import Data.Func
import Data.List
import Data.Tuple
import StdDebug
import StdEnv
import StdMaybe
from Text import concat3, concat5
import Text.YAML
import Text.YAML.Compose
import Text.YAML.Construct
import Text.YAML.Schemas

import Data.SemVer
import Nitrile.Constants
import Nitrile.Package

migrate :: !YAMLSchema !YAMLNode -> MaybeError YAMLErrorWithLocations YAMLNode
migrate schema node=:{content=YAMLMapping kvs} = case mbFormatVersion of
	?Just {value} ->
		first (pushErrorLocation (Field "format_version")) (constructFromYAML schema value) >>= \version ->
		first
			(withoutErrorLocations o InvalidContent)
			(up version {node & content=YAMLMapping rest})
	?None ->
		first
			(withoutErrorLocations o InvalidContent)
			(up {major=0,minor=4,patch=3} (trace_n "no format_version set; assuming 0.4.4" node))
where
	(mbFormatVersion,rest) = appFst listToMaybe $ partition (\{key} -> key=:{content=YAMLScalar _ "format_version"}) kvs

//* Updates the structure to the current format version
up :: !Version !YAMLNode -> MaybeError String YAMLNode
up v n | v == currentVersion =
	Ok n
up v=:{major=0,minor=4,patch} n | 3 <= patch && patch <= 19 =
	up {v & patch=20} n
up v _
	| v > currentVersion = Error $ concat5
		"this version of nitrile cannot parse nitrile.yml with format_version: " (toString v)
		" (max is " (toString CURRENT_VERSION) "); download a newer version"
	| otherwise = Error $ concat3
		"migration from format version " (toString v) " is not implemented"
