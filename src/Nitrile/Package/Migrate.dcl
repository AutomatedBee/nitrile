definition module Nitrile.Package.Migrate

/**
 * Copyright 2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError
from Text.YAML import :: YAMLErrorWithLocations
from Text.YAML.Compose import :: YAMLNode
from Text.YAML.Schemas import :: YAMLSchema

/**
 * Migrates a `Package` configuration according to the specified
 * `format_version`. This allows projects to continue using an old
 * configuration format with a newer version of Nitrile.
 *
 * Migration proceeds in stages: if the `format_version` is 0.4.3 and the
 * current Nitrile version is 0.4.6, the configuration will be updated first to
 * 0.4.4, then to 0.4.5, and then to 0.4.6. Migration is guaranteed to succeed
 * only between minor updates (or patch updates in the 0.x major range).
 */
migrate :: !YAMLSchema !YAMLNode -> MaybeError YAMLErrorWithLocations YAMLNode
