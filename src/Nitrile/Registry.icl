implementation module Nitrile.Registry

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Control.Applicative
import Control.Monad
import Data.Bifunctor
import Data.Error
import Data.Func
import Data.Functor
import Data.List
from Data.Map import :: Map, instance Functor (Map k)
import qualified Data.Map
import Data.Maybe
import Data.Tuple
import StdEnv
import System.Time
import Text.GenJSON

import Data.SemVer
import Data.SemVer.Constraint
import Data.SemVer.ConstraintSet
import Nitrile.Target

// For robustness, we just ignore packages that we cannot parse. For example,
// the maintainer field can be null if a package has been submitted to the
// registry but the information about that package has not been submitted to
// GitLab yet.
JSONDecode{|Registry|} _ [JSONArray items:rest] = (?Just (Registry (catMaybes (map fromJSON items))), rest)
JSONDecode{|Registry|} _ json = (?None, json)

JSONDecode{|RegistryPackage|} _ [JSONObject fields:rest] = (p, rest)
where
	p =
		lookup "id" fields >>= \id ->
		lookup "name" fields >>= \name ->
		lookup "url" fields >>= \url ->
		lookup "maintainer" fields >>= \maintainer ->
		lookup "description" fields >>= \description ->
		lookup "versions" fields >>= \versions ->
		case (id,name,url,maintainer,description,versions) of
			(JSONInt id, JSONString name, JSONString url, JSONString maintainer, JSONString description, JSONObject versions) ->
				(\versions -> {id=id, name=name, url=url, maintainer=maintainer, description=description, versions=versions}) <$>
				'Data.Map'.fromList <$>
				mapM (uncurry (liftM2 tuple) o bifmap (error2mb o parseVersion) fromJSON) versions
			_ ->
				?None
JSONDecode{|RegistryPackage|} _ json = (?None, json)

JSONDecode{|RegistryVersionInfo|} _ [JSONObject fields:rest] = (p, rest)
where
	p =
		lookup "created" fields >>= \created ->
		lookup "targets" fields >>= \targets ->
		case (created, targets) of
			(JSONInt created, JSONObject targets) ->
				(\targets -> {created=Timestamp created, targets=targets}) <$>
				'Data.Map'.fromList <$>
				mapM parseOne targets

	parseOne (target, info) = liftM2 tuple (error2mb (parseTarget target)) (fromJSON info)
JSONDecode{|RegistryVersionInfo|} _ json = (?None, json)

JSONDecode{|RegistryTargetInfo|} _ [JSONObject fields:rest] = (p, rest)
where
	p =
		lookup "url" fields >>= \url ->
		lookup "dependencies" fields >>= \dependencies ->
		case (url, dependencies) of
			(JSONString url, JSONObject dependencies) ->
				(\dependencies -> {RegistryTargetInfo | url=url, dependencies=dependencies}) <$>
				'Data.Map'.fromList <$>
				mapM parseConstraint dependencies
	parseConstraint (pkg, cstr) = tuple pkg <$> fromJSON cstr
JSONDecode{|RegistryTargetInfo|} _ json = (?None, json)

// The registry contains dependencies in two formats:
// - The old format, in which a dependency only has a version constraint;
// - The new format, in which a dependency is a record of version, optionality
//   bool, and in the future possibly other fields.
// The first alternative accepts the old format; the second alternative accepts
// the new format.
JSONDecode{|RegistryDependency|} _ [node=:(JSONString _):rest] =
	((\v -> {RegistryDependency|version=v, optional= ?None}) <$> fromJSON node, rest)
JSONDecode{|RegistryDependency|} _ [JSONObject fields:rest] = (p, rest)
where
	p =
		lookup "version" fields >>= \version ->
		case (version, lookup "optional" fields) of
			(JSONString version, ?Just (JSONBool optional)) ->
				(\v -> {RegistryDependency|version=v, optional= ?Just optional}) <$> error2mb (parseVersionConstraint version)
			(JSONString version, ?None) ->
				(\v -> {RegistryDependency|version=v, optional= ?None}) <$> error2mb (parseVersionConstraint version)
			_ ->
				?None
JSONDecode{|RegistryDependency|} _ json = (?None, json)

JSONEncode{|ConstraintSet|} _ (ConstraintSet cstrs) = encodeStringMap vToJSON cstrs
where
	vToJSON versions = JSONObject
		[ (toString v, toJSON deps)
		\\ (v, deps) <- 'Data.Map'.toList versions
		]
JSONDecode{|ConstraintSet|} _ json = decodeStringMap (\x -> ConstraintSet x) vFromJSON json
where
	vFromJSON (JSONObject versions) = 'Data.Map'.fromList <$> mapM decode versions
	where
		decode (v, deps) = liftM2 tuple (error2mb (parseVersion v)) (fromJSON deps)
	vFromJSON _ = ?None

JSONEncode{|Dependencies|} _ (Dependencies deps) = encodeStringMap toJSON deps
JSONDecode{|Dependencies|} _ json = decodeStringMap (\x -> Dependencies x) fromJSON json

encodeStringMap :: !(v -> JSONNode) !(Map String v) -> [JSONNode]
encodeStringMap toJSON kvs = [JSONObject [(k, toJSON v) \\ (k,v) <- 'Data.Map'.toList kvs]]

decodeStringMap :: !((Map String v) -> a) !(JSONNode -> ?v) ![JSONNode] -> (!?a, ![JSONNode])
decodeStringMap cons fromJSON [JSONObject kvs:rest] = (cons o 'Data.Map'.fromList <$> mapM decode kvs, rest)
where
	decode (k, v) = tuple k <$> fromJSON v
decodeStringMap _ _ json = (?None, json)

toConstraintSet :: !Target !Registry -> ConstraintSet
toConstraintSet target (Registry pkgs) = ConstraintSet ('Data.Map'.fromList
	[ (name, toVersions versions)
	\\ {name,versions} <- pkgs
	])
where
	// NB: it may be that a package version occurs multiple times for the given
	// target, if there are packages using 'any' in the target specification.
	// For example, if there is a `pkg-0.1.0-linux-any` and a
	// `pkg-0.1.0-linux-x64`, there are two entries on x64 Linux. In this case,
	// it is not specified which is chosen. Package authors are responsible for
	// making sure there is no overlap.
	toVersions versions = 'Data.Map'.fromList
		[ (v, Dependencies (toTuple <$> tInfo.RegistryTargetInfo.dependencies))
		\\ (v, vInfo) <- 'Data.Map'.toList versions
		, (thisTarget, tInfo) <- 'Data.Map'.toList vInfo.targets
		| targetMatches thisTarget target
		]
	toTuple {RegistryDependency|version,optional} = (fromMaybe False optional, version)
