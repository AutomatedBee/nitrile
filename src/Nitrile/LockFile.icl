implementation module Nitrile.LockFile

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Control.Applicative
import Control.Monad
import Data.Bifunctor
import Data.Error
import Data.Functor
from Data.Map import :: Map
import qualified Data.Map
import Data.Maybe
import Data.Tuple
import StdEnv
import System.File
import System.FilePath
import Text.GenJSON

import Data.SemVer
import Nitrile.Constants
import Nitrile.Target

derive JSONEncode LockPackage
derive JSONDecode LockPackage

JSONEncode{|LockFile|} _ file = [JSONObject
		[ ("packages", JSONObject (map (bifmap toString toJSON) ('Data.Map'.toList file.packages)))
		]
	]
JSONDecode{|LockFile|} _ [JSONObject [("packages", targetPackages)]:rest] =
	( (\targetPackages -> {packages=targetPackages}) <$> decodeTargetPackages targetPackages
	, rest
	)
where
	decodeTargetPackages (JSONObject kvs) = 'Data.Map'.fromList <$> mapM decodeOne kvs
	where
		decodeOne (target, packages) = liftM2 tuple (error2mb (parseTarget target)) (fromJSON packages)
	decodeTargetPackages _ = ?None
JSONDecode{|LockFile|} _ json = (?None, json)

emptyLockFile :: LockFile
emptyLockFile =
	{ packages = 'Data.Map'.newMap
	}

readLockFile :: !FilePath !*World -> (!MaybeError String LockFile, !*World)
readLockFile path w
	# (mbContents,w) = readFile path w
	| isError mbContents = (Error (toString (fromError mbContents)), w)
	# mbLockFile = fromJSON (fromString (fromOk mbContents))
	= (mb2error "failed to parse lock file contents" mbLockFile, w)

writeLockFile :: !FilePath !LockFile !*World -> (!MaybeError String (), !*World)
writeLockFile path lockFile w
	# (mbError,w) = writeFile path (jsonPrettyPrint (toJSON lockFile) +++ "\n") w
		// NB: pretty-printing is better for version control
	= (mapError toString mbError, w)

getLockedPackages :: !Target !LockFile -> [LockPackage]
getLockedPackages target {packages} = fromMaybe [] ('Data.Map'.get target packages)

setLockedPackages :: !Target ![LockPackage] !LockFile -> LockFile
setLockedPackages target packages lockfile =
	{ lockfile
	& packages = 'Data.Map'.put target packages lockfile.packages
	}
