definition module Nitrile.Metadata

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from SPDX.License import :: License
from Text.GenJSON import :: JSONNode, generic JSONEncode

from Data.SemVer.Constraint import :: VersionConstraint
from Nitrile.Package import :: NamedList, :: Package

:: Metadata =
	{ maintainer         :: !?String
	, contact_email      :: !?String
	, description        :: !?String
	, license            :: !?License
	, git_ref            :: !?String
	, dependencies       :: !NamedList MetadataDependency
	, extra_dependencies :: !NamedList (NamedList MetadataDependency)
	}

:: MetadataDependency =
	{ version  :: !VersionConstraint
	, optional :: !?Bool
	}

derive JSONEncode Metadata

/**
 * @param The `git_ref`.
 * @param The package.
 */
toMetadata :: !(?String) !Package -> Metadata
