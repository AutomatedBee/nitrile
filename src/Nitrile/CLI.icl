implementation module Nitrile.CLI

/**
 * Copyright 2021-2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Control.Applicative
from Control.Monad import class Monad(..), foldM, mapM, sequence
import Data.Error
import Data.Func
import Data.Functor
import Data.List
import qualified Data.Map
import Data.Maybe
import Data.Tuple
import StdEnv
import System.Directory
import System.File
import System.FilePath
import Text
import Text.GenJSON

import Data.SemVer
import qualified Nitrile.CLI.Build
from Nitrile.CLI.Build import :: CLIBuildOptions{..}
import qualified Nitrile.CLI.Do
from Nitrile.CLI.Do import :: CLIDoOptions{..}
import qualified Nitrile.CLI.Env
import qualified Nitrile.CLI.Fetch
from Nitrile.CLI.Fetch import :: CLIFetchOptions{..}
import qualified Nitrile.CLI.GenerateTags
import qualified Nitrile.CLI.Global
from Nitrile.CLI.Global import :: CLIGlobalCommand(..)
import qualified Nitrile.CLI.Init
import qualified Nitrile.CLI.Package
import qualified Nitrile.CLI.Publish
from Nitrile.CLI.Publish import :: CLIPublishOptions{..}
import qualified Nitrile.CLI.Run
from Nitrile.CLI.Run import :: CLIRunOptions{..}
import Nitrile.CLI.Settings
import qualified Nitrile.CLI.Test
from Nitrile.CLI.Test import :: CLITestOptions{..}, :: CLIPropertyTesterOptions{..}, :: CLITestRunnerOptions{..}, :: CLITestRunnerParallelization{..}
import qualified Nitrile.CLI.Update
import qualified Nitrile.CLI.Watch
from Nitrile.CLI.Watch import :: CLIWatchOptions{..}, :: ShellCommand{..}, :: WatchCommand(..)
import Nitrile.CLI.Util
import Nitrile.CLI.Util.DependencySources
import Nitrile.CLI.Util.Legal
import Nitrile.Constants
import Nitrile.Package
import Nitrile.Target

parseCommandLine :: !GlobalSettings ![String] -> MaybeError String (CLICommand, GlobalOptions)
parseCommandLine _ [] = Error (usage "nitrile") // should not occur
parseCommandLine settings [prog:args] =
	case parseArgs args (?None, defaultOptions) of
		Error e -> Error e
		Ok (?None, _) -> Error (usage prog)
		Ok (?Just cmd, opts) -> Ok (cmd, opts)
where
	defaultOptions =
		{ name = ?None
		, target = currentTarget
		, verbose = False
		, no_header = False
		, clm_linker = maybe ?None (\clm -> clm.linker) settings.clm
		, parallel_jobs = maybe ?None (\clm -> clm.GlobalClmSettings.parallel_jobs) settings.clm
		, local_dependencies = 'Data.Map'.newMap
		, scope = UseScope
		}

	parseArgs ["--do":rest] (?Just (Watch watchOpts), opts) =
		(\cmds -> (?Just (Watch {watchOpts & commands=cmds}), opts)) <$>
		mapM parseWatchCommand (split [] rest)
	where
		split acc [] = [reverse acc]
		split acc ["--do":rest] = [reverse acc:split [] rest]
		split acc [arg:rest] = split [arg:acc] rest

		parseWatchCommand ["shell","-c",cmd] = Ok (ShellCommand {command=cmd, cancellable=True})
		parseWatchCommand ["shell",cmd] = Ok (ShellCommand {command=cmd, cancellable=False})
		parseWatchCommand args =
			parseArgs args (?None, opts) >>= \(cmd, opts) ->
			case cmd of
				?Just (Watch _) -> Error "recursive watch is not allowed"
				?Just cmd -> Ok (CLICommand cmd opts)
				?None -> Error "no subcommand in --do block"

	parseArgs ["--local-dependency",dep,path:rest] (cmd, opts) =
		parseArgs rest (cmd, {opts & local_dependencies='Data.Map'.put dep path opts.local_dependencies})
	parseArgs ["--local-dependency":_] _ =
		Error "--local-dependency requires two arguments"

	parseArgs ["--":args] (?Just (Do options), global_opts) =
		Ok (?Just (Do {options & arguments=args}), global_opts)

	parseArgs ["global":"install":pkg:mbVersion] (_, opts) = case mbVersion of
		[] = Ok (?Just (Global (Install pkg ?None)), opts)
		[version] = case parseVersion version of
			Ok version = Ok (?Just (Global (Install pkg (?Just version))), opts)
			Error e = Error ("global install: could not parse version argument: " +++ e)
		_ = Error "global install: too many arguments"
	parseArgs ["global":"install":_] _ = Error "global install: missing package name"
	parseArgs ["global":"list":rest] (_, opts)
		| isEmpty rest = Ok (?Just (Global List), opts)
		| otherwise = Error ("global list: unexpected argument, usage: nitrile global list")
	parseArgs ["global":"remove":pkg:version] (_, opts) = case version of
		[version] = case parseVersion version of
			Ok version = Ok (?Just (Global (Remove pkg version)), opts)
			Error e = Error ("global remove: could not parse version argument: " +++ e)
		_ = Error "global remove: too few/many arguments"
	parseArgs ["global":"remove":_] _ = Error "global remove: missing package name"
	parseArgs ["global"] _ = Error "incorrect usage of nitrile global (missing argument), see the description provided by the nitrile command"

	parseArgs ["help":rest] (?None, opts) = case rest of
		[cmd] -> Ok (?Just (Help cmd), opts)
		_ -> Error "Usage: nitrile help COMMAND"

	parseArgs ["run":rest] (?None, opts) = case rest of
		[pkg:executable:args] ->
			Ok (?Just (Run {package=pkg, executable=executable, args=args}), opts)
		_ ->
			Error "nitrile run: usage: nitrile run PACKAGE BINARY [ARGS]"

	parseArgs [arg:rest] st =
		parseOne arg st >>=
		parseArgs rest

	parseArgs [] st =
		Ok st

	parseOne "build" =
		flip bind (option (\opts -> {GlobalOptions | opts & scope=BuildScope})) o
		command (Build {CLIBuildOptions | list=False, only = ?None})
	parseOne "do" = command (Do {CLIDoOptions | list=False, action = ?None, arguments=[]})
	parseOne "env" =
		flip bind (option (\opts -> {GlobalOptions | opts & scope=TestScope})) o
		command Env
	parseOne "fetch" = command (Fetch {CLIFetchOptions | noOptional=False})
	parseOne "generate-tags" = command GenerateTags
	parseOne "init" = command Init
	parseOne "package" = command Package
	parseOne "publish" = command (Publish {CLIPublishOptions | targets=[]})
	parseOne "terms" = command Terms
	parseOne "test" =
		flip bind (option (\opts -> {GlobalOptions | opts & scope=TestScope})) o
		command (Test
			{ list = False
			, only = ?None
			, except = ?None
			, property_tester_options =
				{ compile_only = False
				, no_regenerate = False
				, nr_of_tests = ?None
				, only_modules = ?None
				}
			, test_runner_options =
				{ parallelization = ?None
				}
			})
	parseOne "update" = command Update
	parseOne "watch" = command (Watch {CLIWatchOptions | commands = []})

	parseOne opt
		| startsWith "--arch=" opt
			= mbOption \opts -> (\a -> {GlobalOptions | opts & target.architecture=a}) <$> parseArchitecture (opt % (7, size opt-1))
		| startsWith "--clm-linker=" opt
			= option \opts -> {GlobalOptions | opts & clm_linker= ?Just (opt % (13, size opt-1))}
		| startsWith "--name=" opt
			= option \opts -> {GlobalOptions | opts & name= ?Just (opt % (7, size opt-1))}
		| startsWith "--parallel-jobs=" opt = mbOption \opts
			# ns = opt % (16, size opt-1)
			  n = toInt ns
			| n > 0 ->
				Ok {GlobalOptions | opts & parallel_jobs = ?Just n}
			| otherwise ->
				Error (concat3 "--parallel-jobs: could not parse '" ns "' as a positive integer")
		| startsWith "--platform=" opt
			= mbOption \opts -> (\p -> {GlobalOptions | opts & target.platform=p}) <$> parsePlatform (opt % (11, size opt-1))

	parseOne "--no-header" = option \opts -> {opts & no_header=True}
	parseOne "--verbose" = option \opts -> {opts & verbose=True}

	parseOne arg = \(mbCommand, globalOpts) -> case mbCommand of
		?Just (Build opts) -> flip tuple globalOpts <$> ?Just <$> Build <$> parseBuildOption arg opts
		?Just (Do opts) -> flip tuple globalOpts <$> ?Just <$> Do <$> parseDoOption arg opts
		?Just (Fetch opts) -> flip tuple globalOpts <$> ?Just <$> Fetch <$> parseFetchOption arg opts
		?Just (Test opts) -> flip tuple globalOpts <$> ?Just <$> Test <$> parseTestOption arg opts
		?Just (Publish opts) -> flip tuple globalOpts <$> ?Just <$> Publish <$> parsePublishOption arg opts
		_ -> Error ("unexpected argument '" +++ arg +++ "'")

	command cmd (?None, opts) = Ok (?Just cmd, opts)
	command _ (?Just _, _) = Error "two subcommands given"

	option mod (mbCmd, opts) = Ok (mbCmd, mod opts)
	mbOption mod (mbCmd, opts) = tuple mbCmd <$> mod opts

	parseBuildOption arg opts
		| arg == "--list" = Ok
			{CLIBuildOptions | opts & list=True}
		| startsWith "--only=" arg = Ok
			{CLIBuildOptions | opts & only = ?Just (split "," (arg % (7, size arg-1)))}
		| otherwise =
			Error ("unexpected argument '" +++ arg +++ "'")

	parseDoOption arg opts
		| arg == "--list" = Ok
			{CLIDoOptions | opts & list=True}
		| otherwise = case opts.action of
			?None -> Ok {opts & action = ?Just arg}
			?Just _ -> Error "cannot specify more than one action to nitrile do"

	parseFetchOption arg opts
		| arg == "--no-optional" = Ok
			{CLIFetchOptions | opts & noOptional=True}
		| otherwise =
			Error ("unexpected argument '" +++ arg +++ "'")

	parseTestOption arg opts
		| startsWith "--except=" arg
			| isJust opts.CLITestOptions.only = Error "--only and --except may not be used simultaneously"
			| otherwise = Ok {CLITestOptions | opts & except = ?Just (split "," (arg % (9, size arg-1)))}
		| arg == "--list" = Ok
			{CLITestOptions | opts & list=True}
		| startsWith "--only=" arg
			| isJust opts.CLITestOptions.except = Error "--only and --except may not be used simultaneously"
			| otherwise = Ok {CLITestOptions | opts & only = ?Just (split "," (arg % (7, size arg-1)))}
		| startsWith "--property-tester-options=" arg
			# options = arg % (26, size arg-1)
			| size options == 0 = Error "--property-tester-options may not be left empty"
			| otherwise = (\testerOptions -> {opts & property_tester_options = testerOptions}) <$>
				foldM parsePropertyTesterOption opts.property_tester_options (split "," options)
		| startsWith "--test-runner-options=" arg
			# options = arg % (22, size arg-1)
			| size options == 0 = Error "--test-runner-options may not be left empty"
			| otherwise = (\testerOptions -> {opts & test_runner_options = testerOptions}) <$>
				foldM parseTestRunnerOption opts.test_runner_options (split "," options)
		| otherwise
			= Error (concat3 "unexpected argument '" arg "'")
	where
		parsePropertyTesterOption :: !CLIPropertyTesterOptions !String -> MaybeError String CLIPropertyTesterOptions
		parsePropertyTesterOption opts "compile-only" = Ok {opts & compile_only = True}
		parsePropertyTesterOption opts "no-regenerate" = Ok {opts & no_regenerate = True}
		parsePropertyTesterOption _ "nr-of-tests" = Error "--property-tester-options=nr-of-tests expects an argument"
		parsePropertyTesterOption _ "only" = Error "--property-tester-options=only expects an argument"
		parsePropertyTesterOption opts arg
			| startsWith "nr-of-tests:" arg
				# nrTestsStr = arg % (12, size arg-1)
				| size nrTestsStr == 0 = Error "--property-tester-options=nr-of-tests expects an argument"
				# nrTests = toInt nrTestsStr
				| nrTests <= 0 = Error "--property-tester-options=nr-of-tests:N: N should be greater than 0"
				| otherwise = Ok {opts & nr_of_tests = ?Just nrTests}
			| startsWith "only:" arg
				# modules = arg % (5, size arg-1)
				| size modules == 0 = Error "--property-tester-options=only expects an argument"
				| otherwise = Ok {CLIPropertyTesterOptions | opts & only_modules = ?Just (split "+" modules)}
			| otherwise = Error (concat3 "unexpected argument to --property-tester-options '" arg "'")

		parseTestRunnerOption :: !CLITestRunnerOptions !String -> MaybeError String CLITestRunnerOptions
		parseTestRunnerOption opts arg
			| startsWith "parallel:" arg = case map toInt $ split ":" arg of
				[_,index,total]
					| total <= 0
						-> Error "--test-runner-options: total number of processes must be positive"
					| index <= 0 || index > total
						-> Error "--test-runner-options: process index must be in the range from 1 to TOTAL"
						-> Ok {opts & parallelization = ?Just {index=index, total=total}}
				_ ->
					Error "--test-runner-options: parallel should be of the format parallel:INDEX:TOTAL"
			| otherwise = Error (concat3 "unexpected argument to --test-runner-options '" arg "'")

	parsePublishOption arg opts=:{CLIPublishOptions | targets}
		| startsWith "--targets=" arg =
			(\ts -> {CLIPublishOptions | opts & targets=targets ++ ts}) <$>
			mapM parseTarget (split "," (arg % (10, size arg-1)))
		| otherwise =
			Error ("unexpected argument '" +++ arg +++ "'")

usage :: !String -> String
usage prog = join "\n"
	[ "Usage: " +++ prog +++ " COMMAND [OPTIONS]"
	, ""
	, "where COMMAND is one of:"
	, ""
	, "\tbuild    -- build the application"
	, "\tdo       -- run predefined actions"
	, "\tenv      -- output environment variable settings used by nitrile"
	, "\tfetch    -- fetch dependencies of the current project"
	, "\tglobal   -- perform actions on packages in the global (cross-project)"
	, "\t         -- scope"
	, "\tgenerate-tags"
	, "\t         -- generate ctags in .nitrile-tags, for use by your editor"
	, "\thelp     -- get help on a subcommand"
	, "\tinit     -- create a new project in the current directory"
	, "\tpackage  -- create a .tar.gz package for the current project"
	, "\tpublish  -- publish the .tar.gz package to GitLab (use only in CI)"
	, "\trun      -- run binaries of packages that are installed in the local"
	, "\t         -- the global (cross-project) scope"
	, "\tterms    -- show full warranty and copying conditions"
	, "\ttest     -- test the current project"
	, "\tupdate   -- update the local copy of the registry"
	, "\twatch    -- watch source directories and run actions on changes"
	, ""
	, "and the global OPTIONS are:"
	, ""
	, "\t--arch=arm64|x86|x64|intel|arm|32bit|64bit|any"
	, "\t         -- pretend to be an arm64/x86/x64 target."
	, "\t         -- intel and arm are used when the bitwidth does not matter."
	, "\t         -- 32bit and 64bit are used when the architecture does not"
	, "\t         -- matter."
	, "\t         -- any is used when neither matters."
	, ""
	, "\t--clm-linker=LINKER"
	, "\t         -- use LINKER as the linker in `clm` jobs (e.g. `lld`; for"
	, "\t         -- possible values of LINKER see the `gcc` documentation on"
	, "\t         -- `-fuse-ld`; `ld` uses the default linker)"
	, ""
	, "\t--local-dependency DEP PATH"
	, "\t         -- use PATH as the source for dependency DEP, instead of a"
	, "\t         -- path from " +++ LOCAL_PACKAGES_DIR +++ "."
	, ""
	, "\t--name=NAME"
	, "\t         -- use the package NAME from nitrile.yml (in case there is"
	, "\t         -- more than one package)"
	, ""
	, "\t--no-header"
	, "\t         -- do not print copyright and license information."
	, ""
	, "\t--parallel-jobs=N"
	, "\t         -- run at most N jobs in parallel (this is only used for clm"
	, "\t         -- tasks in build and test scripts)"
	, ""
	, "\t--platform=linux|mac|windows|any"
	, "\t         -- pretend to be a linux/mac/windows/generic target."
	, "\t         -- any is used when the platform does not matter."
	, ""
	, "\t--verbose"
	, "\t         -- enable verbose output"
	, ""
	, "See `nitrile help COMMAND` for detailed information about subcommands."
	]

help :: !String -> [String]
help "build" =
	[ "Options specific to build:"
	, ""
	, "--list\t-- only list build goals, do not build."
	, ""
	, "--only=GOAL1[,GOAL2,...]"
	, "\t-- only build GOAL1, GOAL2, ..."
	]
help "do" =
	[ "Usage of nitrile do:"
	, ""
	, "nitrile do ACTION [-- ARG1 ARG2 ...]"
	, "\t-- run the action ACTION defined in nitrile.yml."
	, ""
	, "nitrile do --list"
	, "\t-- only list actions, do not run."
	]
help "fetch" =
	[ "Usage of nitrile fetch:"
	, ""
	, "--no-optional"
	, "\t-- do not fetch the optional dependencies."
	]
help "global" =
	[ "Usage of nitrile global:"
	, ""
	, "nitrile global install PACKAGE [VERSION]"
	, "\t-- install PACKAGE globally, allowing cross-project use. If VERSION is"
	, "\t-- not specified, the latest version is installed."
	, ""
	, "nitrile global list"
	, "\t-- list globally installed packages and their versions."
	, ""
	, "nitrile global remove PACKAGE VERSION"
	, "\t-- remove the globally installed version VERSION of PACKAGE."
	]
help "publish" =
	[ "Options specific to publish:"
	, ""
	, "--targets=PLATFORM-ARCH[,...]"
	, "\t-- required: specifies the targets that are published."
	, "\t-- publish fails if there are other packages in the"
	, "\t-- directory; this is a simple check that you have not"
	, "\t-- forgotten to list any targets."
	]
help "run" =
	[ "Usage of nitrile run:"
	, ""
	, "nitrile run PACKAGE EXECUTABLE [ARGS]"
	, "\t-- run EXECUTABLE from the bin directory of PACKAGE with arguments"
	, "\t-- ARGS. The package (and its dependencies) may be installed either"
	, "\t-- locally or globally."
	]
help "test" =
	[ "Options specific to test:"
	, ""
	, "--except=CASE1[,CASE2,...]"
	, "\t-- test everything except CASE1, CASE2, ..."
	, ""
	, "--list\t-- only list test names, do not test."
	, ""
	, "--only=CASE1[,CASE2,...]"
	, "\t-- only test CASE1, CASE2, ..."
	, ""
	, "--property-tester-options=OPTIONS"
	, "\t-- pass options to the property tester. Possible options are:"
	, "\t--  - compile-only"
	, "\t--  - no-regenerate"
	, "\t--  - only:MODULES"
	, "\t--  - nr-of-tests:N"
	, ""
	, "--test-runner-options=OPTIONS"
	, "\t-- pass options to the test runner. Possible options are:"
	, "\t--  - parallel:INDEX:TOTAL"
	]
help "watch" =
	[ "Options specific to watch:"
	, ""
	, "--do"
	, "\t-- starts a new subcommand to be run when changes are detected; can be"
	, "\t-- any nitrile subcommand (except watch), or one of the following:"
	, "\t--  - shell [-c] '...'; runs a shell command; with -c the command will"
	, "\t--    be terminated on new changes."
	]
help _ = []

performCommand :: !CLICommand !GlobalOptions !*World -> (!Bool, !*World)
performCommand cmd opts w
	# (opts,w) = checkOpts opts w
	= performCommand` cmd opts w
where
	checkOpts opts=:{target,clm_linker} w
		# (opts,w) = if
			(not (architectureMatches target.architecture {family=Intel,bitwidth=BW64}) && clm_linker == ?Just "lld")
			({opts & clm_linker = ?None}, log "Warning: --clm-linker=lld can only be used on x64, ignoring." w)
			(opts, w)
		= (opts, w)

performCommand` :: !CLICommand !GlobalOptions !*World -> (!Bool, !*World)
performCommand` (Global cmd) opts=:{GlobalOptions|target} w
	= 'Nitrile.CLI.Global'.global target cmd w
performCommand` (Help cmd) _ w = case help cmd of
	[] -> fail (concat3 "There is no help available for the command '" cmd "'.") w
	text -> succeed (?Just (join "\n" text)) w
performCommand` Init _ w
	= 'Nitrile.CLI.Init'.init w
performCommand` (Run runOptions) opts=:{GlobalOptions|name} w
	# (mbPackage,w) = readPackage name "." w
	= case mbPackage of
		Error (OtherPackageError e) -> fail e w
		_ -> 'Nitrile.CLI.Run'.run opts runOptions (error2mb mbPackage) w
performCommand` Terms _ w
	= (True, printFullLegalInfo w)
performCommand` Update _ w
	= 'Nitrile.CLI.Update'.update w
performCommand` cmd options=:{name,target,verbose} w
	# (mbPackage,w) = readPackage name "." w
	| isError mbPackage = fail (toString $ fromError mbPackage) w
	# org_pkg = fromOk mbPackage
	  pkg = normalizePackage target org_pkg
	  mbErr = verifyPackage pkg
	| isError mbErr = fail (join "\n" ["Malformed package configuration:":fromError mbErr]) w
	| otherwise = performCommandWithPackage cmd org_pkg pkg options w

/**
 * @param The command.
 * @param The package as described by the user.
 * @param The same package, after running makePlatformSpecificPaths and resolveRules.
 * @param Global options.
 */
performCommandWithPackage :: !CLICommand !Package !Package !GlobalOptions !*World -> (!Bool, !*World)
performCommandWithPackage (Build buildOptions) _ pkg options w
	= withTmpDir ('Nitrile.CLI.Build'.build options buildOptions ?None pkg) w
performCommandWithPackage (Do doOptions) _ pkg _ w
	= withTmpDir ('Nitrile.CLI.Do'.do doOptions pkg) w
performCommandWithPackage Env _ pkg options w
	= 'Nitrile.CLI.Env'.env options pkg w
performCommandWithPackage (Fetch fetchOptions) _ pkg {local_dependencies,target} w
	| 'Data.Map'.mapSize local_dependencies > 0
		= fail "--local-dependency cannot be used with fetch" w
		= 'Nitrile.CLI.Fetch'.fetch fetchOptions target pkg w
performCommandWithPackage GenerateTags _ pkg _ w
	= 'Nitrile.CLI.GenerateTags'.generateTags pkg w
performCommandWithPackage Package _ pkg {GlobalOptions|target} w
	= withTmpDir ('Nitrile.CLI.Package'.package target pkg) w
performCommandWithPackage (Publish publishOptions) org_pkg pkg _ w
	= 'Nitrile.CLI.Publish'.publish publishOptions org_pkg pkg w
performCommandWithPackage (Test testOptions) _ pkg options w
	= withTmpDir ('Nitrile.CLI.Test'.test options testOptions pkg) w
performCommandWithPackage (Watch watchOptions) org_pkg pkg options w
	= 'Nitrile.CLI.Watch'.watch options watchOptions org_pkg pkg w

withTmpDir :: !(FilePath *World -> (Bool, *World)) !*World -> (!Bool, !*World)
withTmpDir f w
	# (tmp_dir,w) = tmpDir w
	# (tmp_dir,w) = findNonExistingDirectory tmp_dir 0 w
	// NB: we don't use ensureDirectoryExists here because we want to be sure the directory is empty
	# (mbError,w) = createDirectory tmp_dir w
	| isError mbError
		= fail (concat4 "Failed to create " tmp_dir ": " (snd (fromError mbError))) w
	# (ok,w) = f tmp_dir w
	# (mbError,w) = recursiveDelete tmp_dir w
	| isError mbError
		= fail (concat4 "Failed to delete " tmp_dir ": " (snd (fromError mbError))) w
		= (ok, w)
where
	findNonExistingDirectory :: !FilePath !Int !*World -> (!FilePath, !*World)
	findNonExistingDirectory tmp_dir i w
		# path = if (i > 0) (concat3 tmp_dir "-" (toString i)) tmp_dir
		# (exi,w) = fileExists path w
		| exi = findNonExistingDirectory tmp_dir (i+1) w
		| otherwise = (path, w)
