definition module Nitrile.Target

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError
from StdOverloaded import class ==, class <, class toString

:: Target =
	{ platform     :: !Platform
	, architecture :: !Architecture
	}

:: Platform
	= AnyPlatform
	| Linux
	| Mac
	| Windows

:: Architecture =
	{ family   :: !ArchitectureFamily
	, bitwidth :: !Bitwidth
	}

:: ArchitectureFamily
	= AnyArchitectureFamily
	| ARM
	| Intel

:: Bitwidth
	= AnyBitwidth
	| BW32
	| BW64

instance == Target // for `Map`s only; use `targetMatches` instead.
instance < Target // for `Map`s
instance toString Target, Platform, Architecture

//* Like equality, but ignores `AnyPlatform`, `AnyArchitecture`, and `AnyBitwidth`.
targetMatches :: !Target !Target -> Bool
platformMatches :: !Platform !Platform -> Bool
architectureMatches :: !Architecture !Architecture -> Bool
architectureFamilyMatches :: !ArchitectureFamily !ArchitectureFamily -> Bool
bitwidthMatches :: !Bitwidth !Bitwidth -> Bool

currentTarget :: Target

allTargets :: [Target]

isValidTarget :: !Target -> Bool

parseTarget :: !String -> MaybeError String Target
parsePlatform :: !String -> MaybeError String Platform
parseArchitecture :: !String -> MaybeError String Architecture
