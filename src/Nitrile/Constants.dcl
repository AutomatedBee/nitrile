definition module Nitrile.Constants

/**
 * Copyright 2021-2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError
from System.FilePath import :: FilePath

from Data.SemVer import :: Version
from Nitrile.Target import :: Target

CURRENT_VERSION :== "0.4.20"
currentVersion :: Version

PACKAGE_FILE :== "nitrile.yml"
LOCK_FILE :== "nitrile-lock.json"
METADATA_FILE :== "nitrile-metadata.json"
PACKAGE_METADATA_FILE :== ".nitrile.json"
SETTINGS_FILE :== "nitrile-settings.yml"
TAGS_FILE :== ".nitrile-tags"

GLOBAL_PACKAGES_DIR :== "packages"
LOCAL_PACKAGES_DIR :== "nitrile-packages" // use localPackagesDir for the directory of a target
TESTS_DIR :== "nitrile-tests" // use testsDir for the directory of a target

REGISTRY_HOST :== "clean-lang.org"
REGISTRY_PORT :== 80
REGISTRY_COPY_FILE :== "registry.json"

globalNitrileDir :: !*World -> (!MaybeError String FilePath, !*World)
constraintSetFile :: !Target -> FilePath
localPackagesDir :: !Target -> FilePath
testsDir :: !Target -> FilePath
homeDir :: !Target -> FilePath
tmpDir :: !*World -> (!FilePath, !*World)
