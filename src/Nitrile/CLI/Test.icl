implementation module Nitrile.CLI.Test

/**
 * Copyright 2021-2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Clean.ModuleFinder
import Data.Error
import Data.Func
import Data.GenDiff
import Data.List
import qualified Data.Map
import Data.Maybe
import StdEnv
import System.Directory
import System.File
import System.FilePath
import System.OS
import System.Process
import System.SysCall
import Text

import Data.NamedList
import Data.SemVer
import Data.SemVer.Constraint
import Data.SemVer.ConstraintSet
import Nitrile.Build
import Nitrile.CLI
import Nitrile.CLI.Build
import Nitrile.CLI.Util
import Nitrile.CLI.Util.DependencySources
import Nitrile.Constants
import Nitrile.Package

test :: !GlobalOptions !CLITestOptions !Package !FilePath !*World -> (!Bool, !*World)
test global_options=:{GlobalOptions|target} options pkg tmp_dir w
	# w = logWarningForUnspecificTarget "test" target w
	| options.CLITestOptions.list
		= (True, seqSt log all_tests w)
	# mbTests = case options.CLITestOptions.only of
		?Just only -> Ok only // existence is checked in runTests
		?None -> case options.except of
			?Just es -> all_tests except es
			?None -> Ok all_tests
	| isError mbTests = fail (fromError mbTests) w
	# dependency_resolver = setupCachingDependencyResolver False global_options pkg
	# w = setupEnvironmentVariables target w
	= runTests global_options options.property_tester_options options.test_runner_options pkg (fromOk mbTests) tmp_dir dependency_resolver w
where
	all_tests = map fst (fromNamedList pkg.Package.tests)

	(except) :: ![String] ![String] -> MaybeError String [String]
	(except) org [] = Ok org
	(except) org [e:es]
		| isMember e org = removeMember e org except es
		| otherwise = Error (concat3 "Unknown test specified in --except: '" e "'")

runTests :: !GlobalOptions !CLIPropertyTesterOptions !CLITestRunnerOptions !Package ![String] !FilePath !*CachingDependencyResolver !*World -> (!Bool, !*World)
runTests _ _ _ _ [] _ dependency_resolver w
	= succeed ?None w
runTests global_options=:{clm_linker,target,parallel_jobs} property_tester_options test_runner_options=:{parallelization} pkg [name:tests] tmp_dir dependency_resolver w
	# mbTest = lookup name (fromNamedList pkg.Package.tests)
	  test = fromJust mbTest
	| isNone mbTest = fail (concat3 "Unknown test '" name "'") w
	# (mbError,dependency_resolver,w) = mbBuildDependencies test.TestSpec.depends dependency_resolver w
	| isError mbError = fail ("Failed to build test dependencies: " +++ fromError mbError) w
	# w = log (concat3 "Running " name "...") w
	# (mbError,dependency_resolver,w) = runTest test dependency_resolver w
	| isError mbError = fail (fromError mbError) w
	# w = log "Passed." w
	= runTests global_options property_tester_options test_runner_options pkg tests tmp_dir dependency_resolver w
where
	mbBuildDependencies :: !(?[String]) !*CachingDependencyResolver !*World -> *(!MaybeError String (), !*CachingDependencyResolver, !*World)
	mbBuildDependencies ?None dependency_resolver w = (Ok (), dependency_resolver, w)
	mbBuildDependencies (?Just depends) dependency_resolver w
		# w = log (concat3 "Building dependencies for " name "...") w
		# mbDependencies = onlyNames depends pkg.Package.build
		  dependencies = fromOk mbDependencies
		| isError mbDependencies = (Error ("Unknown build specified in depends: " +++ join ", " (fromError mbDependencies)), dependency_resolver, w)
		= runBuilds {GlobalOptions | global_options & scope=BuildScope} pkg parallelization dependencies tmp_dir dependency_resolver w

	runTest :: !TestSpec !*CachingDependencyResolver !*World -> (!MaybeError String (), !*CachingDependencyResolver, !*World)
	runTest {required_dependencies,script= ?Just script,expected_result = ?None} dependency_resolver w
		# (result,dependency_resolver,w) = runBuild global_options pkg parallelization {Build|required_dependencies=required_dependencies,depends= ?None,script= ?Just script} tmp_dir dependency_resolver w
		= (mapError ((+++) "Failed to run test script: ") result, dependency_resolver, w)

	runTest {required_dependencies,script= ?Just script,expected_result = ?Just expected_result} dependency_resolver w
		| isEmpty script = (Error "Empty run field for test case", dependency_resolver, w)
		# (build,command) = (init script, last script)
		| not (command=:(SystemCommand _)) =
			(Error "The last element of a test run must be a system command when expected_result is given", dependency_resolver, w)
		# (SystemCommand command) = command
		# script = build ++ IF_WINDOWS
			[SystemCommand (concat4 "(" command ") | out-file -encoding utf8 " (tmp_dir </> "output"))] // By default PowerShell uses UTF-16
			[SystemCommand (concat4 "(" command ") > " (tmp_dir </> "output"))]
		# (result,dependency_resolver,w) = runBuild global_options pkg parallelization {Build|required_dependencies=required_dependencies,depends= ?None,script= ?Just script} tmp_dir dependency_resolver w
		| isError result = (Error ("Failed to run build commands: " +++ fromError result), dependency_resolver, w)
		# (mbOutput,w) = readFile (tmp_dir </> "output") w
		| isError mbOutput = (Error ("Could not read command output: " +++ toString (fromError mbOutput)), dependency_resolver, w)
		# (mbExpected,w) = readFile expected_result w
		| isError mbExpected = (Error ("Failed to read expected result: " +++ toString (fromError mbExpected)), dependency_resolver, w)
		// On Windows we have to strip the UTF-8 file signature / BOM, and replace \r\n with \n
		# output = IF_WINDOWS (\s -> replaceSubString "\r\n" "\n" (s % (if (s % (0,2) == "\xef\xbb\xbf") 3 0, size s-1))) id (fromOk mbOutput)
		# expected = IF_WINDOWS (\s -> replaceSubString "\r\n" "\n" s) id (fromOk mbExpected)
		| output <> expected
			// gDiff may throw a stack overflow, so be sure to log the failure first
			# w = log "Expected result did not match:" w
			# diff = gDiff{|*|} (split "\n" expected) (split "\n" output)
			= (Error (diffToConsole diff), dependency_resolver, w)
		| otherwise = (Ok (), dependency_resolver, w)

	runTest {required_dependencies,properties= ?Just properties} dependency_resolver w
		# test_options = maybe
			properties.test_options
			// CLI option overwrites nitrile.yml option.
			(\nrTests -> ?Just (["Tests " +++ toString nrTests] ++ [opt \\ opt <- fromMaybe [] properties.test_options | not (startsWith "Tests " opt)]))
			property_tester_options.nr_of_tests
		# (mbSources,dependency_resolver,w) = getDependencySourcesWithCache TestScope (fromMaybe [] required_dependencies) dependency_resolver ?None w
		  dependency_sources = fromOk mbSources
		| isError mbSources = (liftError mbSources, dependency_resolver, w)
		# test_paths = flatten [srcInScope BuildScope pkg, srcInScope TestScope pkg, dependencyIncludePaths dependency_sources]
		# (mbClmVersion,w) = getClmVersion dependency_sources w
		| isNone mbClmVersion = (Error "property tests were used but clm is not installed as a dependency", dependency_resolver, w)
		# mbClmOptions = toClmOptions
			(fromJust mbClmVersion)
			clm_linker
			parallel_jobs
			test_paths
			(fromMaybe (defaultClmBuildOptions ?None) properties.PropertyTesterOptions.clm_options)
		  clmOptions = fromOk mbClmOptions
		| isError mbClmOptions = (liftError mbClmOptions, dependency_resolver, w)
		# (mbModuleNames,w) = findAllSrcModules TestScope False property_tester_options.CLIPropertyTesterOptions.only_modules w
		  moduleNames = fromOk mbModuleNames
		| isError mbModuleNames = (liftError mbModuleNames, dependency_resolver, w)
		# (mbErr,w) = if property_tester_options.no_regenerate (Ok (), w) (generateTestModules clmOptions (fromOk mbModuleNames) test_options w)
		| isError mbErr = (mbErr, dependency_resolver, w)
		| property_tester_options.compile_only = (Ok (), dependency_resolver, w)
		# (errors,generatedModules,w) = scanDirectory
			(\p fi found w -> (if (IF_WINDOWS (takeExtension p == "exe") (fi.mode bitand 0111 <> 0 && not fi.directory)) [p:found] found, w))
			[] tests_dir w
		| not (isEmpty errors)
			= (Error (join "\n" ["Errors while scanning for generated property tests:":map snd errors]), dependency_resolver, w)
		# generatedModules = sort generatedModules
		# test_run = TestRunner
			{ TestRunnerOptions
			| output_format = properties.PropertyTesterOptions.output_format
			, junit_xml = properties.PropertyTesterOptions.junit_xml
			, tests =
				[ {executable=m , options= ?Just ["-O", "OutputTestEvents"]}
				\\ m <- generatedModules
				| maybe True (any (flip endsWith m o (+++) ".")) property_tester_options.CLIPropertyTesterOptions.only_modules
				]
			}
		# (result,dependency_resolver,w) = runBuild global_options pkg parallelization {Build|required_dependencies=required_dependencies,depends= ?None,script= ?Just [test_run]} tmp_dir dependency_resolver w
		= (mapError ((+++) "Failed to run tests: ") result, dependency_resolver, w)
	where
		tests_dir = testsDir target

		generateTestModules _ [] _ w = (Ok (), w)
		generateTestModules clmOptions=:(config_options, path_options, code_options, application_options) [mod:mods] test_options w
			# (mbExitCode,w) = callProcessAndPassIO "property-tester"
				[ "--output-directory", tests_dir
				, "--module", mod
				, "--compile"
				, "--verbose"
				: flatten [["--directory", d] \\ d <- srcInScope TestScope pkg] ++
					flatten [["--test-option", opt] \\ opt <- fromMaybe [] test_options] ++
					flatten [["--clm-arg", opt] \\ opt <- config_options ++ path_options ++ code_options ++ application_options]
				]
				?None
				w
			| isError mbExitCode = (Error ("Failed to run property-tester: " +++ snd (fromError mbExitCode)), w)
			| fromOk mbExitCode <> 0 = (Error ("property-tester exited with " +++ toString (fromOk mbExitCode)), w)
			| otherwise = generateTestModules clmOptions mods test_options w

	runTest {required_dependencies,compilation= ?Just {clm_options,only_modules,except_modules}} dependency_resolver w
		# extraDependencies = ?Just $ Dependencies ('Data.Map'.fromList
			[ ("base-clm", (False, VersionConstraint [VersionGe {major=0,minor=0,patch=0}]))
			, ("base-compiler", (False, VersionConstraint [VersionGe {major=0,minor=0,patch=0}]))
			])
		# (mbSources,dependency_resolver,w) = getDependencySourcesWithCache UseScope (fromMaybe [] required_dependencies) dependency_resolver extraDependencies w
		  dependency_sources = fromOk mbSources
		| isError mbSources = (liftError mbSources, dependency_resolver, w)
		# test_paths = flatten [srcInScope UseScope pkg, dependencyIncludePaths dependency_sources]
		# (mbClmVersion,w) = getClmVersion dependency_sources w
		| isNone mbClmVersion = (Error "A compilation test was used but clm is not installed as a dependency", dependency_resolver, w)
		# mbClmOptions = toClmOptions
			(fromJust mbClmVersion)
			clm_linker
			parallel_jobs
			test_paths
			(fromMaybe (defaultClmBuildOptions ?None) clm_options)
		  clmOptions = fromOk mbClmOptions
		| isError mbClmOptions = (liftError mbClmOptions, dependency_resolver, w)
		# (mbModuleNames,w) = findAllSrcModules UseScope True ?None w
		  module_names = fromOk mbModuleNames
		| isError mbModuleNames = (liftError mbModuleNames, dependency_resolver, w)
		# mbModuleNames = case (only_modules, except_modules) of
			(?Just _,?Just _) ->
				Error "A compilation test cannot have both only_modules and except_modules specified"
			(?Just only,_)
				| all (flip isMember module_names) only
					-> Ok only
					-> Error "Not all modules in only_modules could be found"
			(_,?Just except)
				| all (flip isMember module_names) except
					-> Ok (filter (not o flip isMember except) module_names)
					-> Error "Not all modules in except_modules could be found"
			_ ->
				mbModuleNames
		  module_names = fromOk mbModuleNames
		| isError mbModuleNames = (liftError mbModuleNames, dependency_resolver, w)
		# (failed,w) = mapSt (checkCompile clmOptions) (fromOk mbModuleNames) w
		| any isJust failed =
			(Error (join "\n" ["Compilation of the following modules failed:":map ((+++) "\t") (catMaybes failed)]), dependency_resolver, w)
		| otherwise = (Ok (), dependency_resolver, w)
	where
		checkCompile (config_options, path_options, code_options, application_options) mod w
			# (mbExitCode,w) = callProcessAndPassIO "clm"
				(config_options ++ path_options ++ code_options ++ application_options ++ ["-c", mod])
				?None
				w
			| isError mbExitCode = (?Just mod, log ("Failed to run clm: " +++ snd (fromError mbExitCode)) w)
			| fromOk mbExitCode <> 0 = (?Just mod, w) // assume clm has given output
			| otherwise = (?None, w)

	findAllSrcModules :: !UsageScope !Bool !(?[String]) !*World -> (!MaybeError String [String], !*World)
	findAllSrcModules scope include_applications only_modules w
		# (errors, moduleFiles, w) = findAllModules moduleFindingOptions w
		| not (isEmpty errors)
			= (Error (join "\n" ["Errors while scanning for modules:":map snd errors]), w)
		# moduleNames = sort
			[ replaceSubString {pathSeparator} "." moduleWithoutDirectory
			\\ mf <- moduleFiles
			, let
				dirs = filter (flip startsWith mf) src // directories under which this file appears
				dir = maxListBy ((<) `on` size) dirs // the longest directory prefix
				moduleWithoutDirectory = mf % (size (dir </> ""), size mf-5) // strip prefix and extension
			]
		= case only_modules of
			?None
				-> (Ok moduleNames, w)
			?Just only
				# moduleNames = filter (flip isMember only) moduleNames
				| length moduleNames == length only -> (Ok moduleNames, w)
				# missing = removeMembers only moduleNames
				-> (Error ("Could not find the following module(s): " +++ join "; " missing), w)
	where
		src = srcInScope scope pkg

		moduleFindingOptions =
			{ include_paths = src
			, include_libraries = []
			, clean_home = ""
			, include_applications = include_applications
			}
