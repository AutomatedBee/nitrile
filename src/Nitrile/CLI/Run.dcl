definition module Nitrile.CLI.Run

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Nitrile.CLI import :: GlobalOptions
from Nitrile.Package import :: Package

:: CLIRunOptions =
	{ package    :: !String //* The package in which the executable is located.
	, executable :: !String //* The executable to execute.
	, args       :: ![String] //* The arguments to pass on to the executable.
	}

/**
 * Implementation for the `nitrile run PACKAGE BINARY [ARGS]` CLI command.
 * Package is `?None` if there is no nitrile.yml file.
 */
run :: !GlobalOptions !CLIRunOptions !(?Package) !*World -> (!Bool, !*World)
