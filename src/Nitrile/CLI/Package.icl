implementation module Nitrile.CLI.Package

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Error
import Data.Func
import Data.List
import Data.Maybe
import StdEnv
import System.CommandLine
import System.Directory
import System.File
import System.FilePath
import System.OS
import System.Process
import System.SysCall
import Text
import Text.GenJSON

import Data.NamedList
import Nitrile.Build
import Nitrile.CLI.Publish
import Nitrile.CLI.Util
import Nitrile.Constants
import Nitrile.Package
import Nitrile.Target
import System.FilePath.Match

:: PackageMetadata =
	{ fileinfo :: !NamedList PackageFileInfo
	, git_ref  :: !?String
	, version  :: !Version
	}

:: PackageFileInfo =
	{ src         :: !FilePath
	, core_module :: !?Bool
	}

derive JSONEncode PackageMetadata, PackageFileInfo

package :: !Target !Package !FilePath !*World -> (!Bool, !*World)
package target pkg tmp_dir w
	| isNone pkg_slur || isNone pkg_file
		= fail (concat3 "Could not create the package slur, are name and version set in " PACKAGE_FILE "?") w
	# dir = tmp_dir </> fromJust pkg_slur
	# (mbError,w) = createDirectory dir w
	| isError mbError
		= fail (concat4 "Failed to create " dir ": " (snd (fromError mbError))) w
	# (mbPathsAndIncludeDirs,w) = collectFilesAndDirectories paths_to_copy w
	| isError mbPathsAndIncludeDirs
		= fail ("Failed to collect paths: " +++ fromError (mbPathsAndIncludeDirs)) w
	# (paths,includeDirs) = fromOk mbPathsAndIncludeDirs
	# (paths,w) = foldr (collectFiles True "lib") (paths, w) (srcInScope UseScope pkg)
	# (paths,w) = foldr (\{src,dst} -> collectFiles False dst src) (paths,w) includeDirs
	# (ok,w) = foldr (copyToPackage tmp_dir) (True, w) paths
	| not ok
		= succeed ?None w
	# (git_ref,w) = getGitRefMatching pkg.Package.version w
	# metadata =
		{ fileinfo = NamedList (map toFileInfo paths)
		, git_ref = git_ref
		, version = fromJust pkg.Package.version
		}
	# (mbError,w) = writeFile (dir </> PACKAGE_METADATA_FILE) (toString (toJSON metadata)) w
	| isError mbError
		= fail (concat4 "Failed to create " PACKAGE_METADATA_FILE ": " (toString (fromError mbError))) w
	# (mbExitCode,w) = callProcessAndPassIO "tar" ["czpf", fromJust pkg_file, fromJust pkg_slur] (?Just tmp_dir) w
	| isError mbExitCode
		= fail ("Failed to run tar: " +++ snd (fromError mbExitCode)) w
	| fromOk mbExitCode <> 0
		= fail ("Tar failed with exit code " +++ toString (fromOk mbExitCode)) w
	# (mbError,w) = moveFile (tmp_dir </> fromJust pkg_file) (fromJust pkg_file) w
	| isError mbError
		= fail ("Failed to move package: " +++ snd (fromError mbError)) w
	# w = case fieldsMissingForPublish pkg of
		[] -> w
		fields -> log (concat3 "Warning: these fields should be set to run nitrile publish: " (join ", " fields) ".") w
	= succeed (?Just (concat3 "Created " (fromJust pkg_file) ".")) w
where
	pkg_slur = packageSlur pkg target
	pkg_file = packageFile pkg target

	excludedPatterns = maybe [] (\po -> fromMaybe [] po.PackageOptions.exclude) pkg.package
	includedPatterns = maybe [] (\po -> fromMaybe [] po.include) pkg.package

	paths_to_copy =
		maybe [] (\po -> fromMaybe [] po.extra_files) pkg.package ++
		[ {src=path, dst=path}
		\\ (_,{Build|script}) <- fromNamedList pkg.Package.build
		, Clm clmOptions <- fromMaybe [] script
		, let path = fromMaybe clmOptions.main clmOptions.ClmBuildOptions.target
		| not (matchesAny excludedPatterns path)
		]

	collectFilesAndDirectories paths w
		# (mbInfos,w) = mapSt getFileInfo [p.CopyFileSettings.src \\ p <- paths] w
		| any isError mbInfos
			# e = hd [concat3 msg " while inspecting " src \\ Error (_,msg) <- mbInfos & {CopyFileSettings|src} <- paths]
			= (Error e, w)
		# files = [p \\ p <- paths & Ok info <- mbInfos | not info.directory]
		  dirs = [p \\ p <- paths & Ok info <- mbInfos | info.directory]
		= (Ok (files, dirs), w)

	collectFiles checkInclusion dst dir (paths,w)
		# (errors,paths,w) = scanDirectory (includeFile checkInclusion dstPath) paths dir w
		# w = foldr (\(_,e) w -> snd (fclose (stderr <<< e <<< "\n") w)) w errors
		= (paths, w)
	where
		dstPath path = dst </> path % (size dir + if (endsWith pathSeparatorString dir) 0 1, size path-1)

	includeFile :: !Bool !(FilePath -> FilePath) !FilePath !FileInfo ![CopyFileSettings] !*World -> (![CopyFileSettings], !*World)
	includeFile checkInclusion dstPath path info paths w
		| included
			= ([{src=path, dst=dstPath path}:paths], w)
			= (paths, w)
	where
		ext = takeExtension path
		filename = dropDirectory path
		included =
			not info.directory &&
			not (matchesAny excludedPatterns path) &&
			(not checkInclusion || ext == "icl" || ext == "dcl" || matchesAny includedPatterns path)

	copyToPackage :: !FilePath !CopyFileSettings !(!Bool, !*World) -> (!Bool, !*World)
	copyToPackage tmp_dir {src,dst} (ok, w)
		| not ok
			= (ok, w)
		# dst = tmp_dir </> fromJust pkg_slur </> dst
		# (mbError,w) = ensureDirectoryExists (takeDirectory dst) w
		| isError mbError
			= fail (snd (fromError mbError)) w
		# (mbError,w) = copyFile src dst w
		| isError mbError
			= fail (concat4 "Failed to copy " src ": " (snd (fromError mbError))) w
			= (True, w)

	toFileInfo :: !CopyFileSettings -> (!String, !PackageFileInfo)
	toFileInfo {src,dst} = (translate dst, info)
	where
		info =
			{ src = translate src
			, core_module = if (isCoreModule dst) (?Just True) ?None
			}

		translate = IF_POSIX id (replaceSubString pathSeparatorString "/")

	core_module_patterns = maybe [] (map fromString) (maybe ?None (\po -> po.core_modules) pkg.package)

	isCoreModule :: !FilePath -> Bool
	isCoreModule fp
		| isEmpty core_module_patterns
			= False
		| not (startsWith {#'l','i','b',pathSeparator} fp)
			= False
		| not (isMember (takeExtension fp) ["dcl", "icl"])
			= False
		# mod = replaceSubString pathSeparatorString "." (fp % (4, size fp-5))
		= any (matches (fromString mod)) core_module_patterns
	where
		matches [] [] = True
		matches ms ['*':ps] = any (\t -> matches ps t) (tails ms)
		matches [m:ms] [p:ps] = m == p && matches ms ps
		matches _ _ = False
