implementation module Nitrile.CLI.Do

/**
 * Copyright 2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Error
import Data.Func
import Data.List
import StdEnv
import StdMaybe
import System.FilePath
import Text

import Data.NamedList
import Nitrile.CLI.Util
import Nitrile.CLI.Util.Shell
import Nitrile.Package
import Nitrile.Target

do :: !CLIDoOptions !Package !FilePath !*World -> (!Bool, !*World)
do options pkg tmp_dir w
	| options.CLIDoOptions.list
		= (True, listActions pkg.Package.actions w)
	| isNone options.action
		= fail "No action specified." w
	# mbAction = lookup (fromJust options.action) (fromNamedList pkg.Package.actions)
	  action = fromJust mbAction
	| isNone mbAction
		= fail (concat3 "Specified action '" (fromJust options.action) "' does not exist.") w
	# (mbError,w) = runShellScript
		(\s st w -> (st, let (io,w`) = stdio w in snd (fclose (io <<< s) w`)))
		(\s st w -> (st, snd (fclose (stderr <<< s) w)))
		(\e st w -> (?Just e, w))
		action.Action.script options.arguments tmp_dir ?None w
	| isError mbError
		= fail ("Failed to run action: " +++ fromError mbError) w
		= succeed ?None w

listActions :: !(NamedList Action) !*World -> *World
listActions actions w = seqSt (log o fst) (fromNamedList actions) w
