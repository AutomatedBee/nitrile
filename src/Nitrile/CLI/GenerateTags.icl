implementation module Nitrile.CLI.GenerateTags

import Data.Func
import StdEnv
import System.Directory
import System.SysCall
import Text

import Data.CTags
import Nitrile.CLI.Util
import Nitrile.CLI.Util.CTags
import Nitrile.Constants
import Nitrile.Package

generateTags :: !Package !*World -> (!Bool, !*World)
generateTags pkg w
	# (errors,dcls,w) = scanDirectories
		(\path _ paths w -> (if (endsWith ".dcl" path) [path:paths] paths, w))
		[]
		(srcInScope TestScope pkg)
		w
	# w = seqSt (\(_,e) -> log ("Error while searching for dcls: " +++ e)) errors w
	# (tagsets,w) = mapSt (\dcl -> collectTags True "." (dcl % (0, size dcl-5))) dcls w
	# tags = combineTagSets tagsets
	| tagSetSize tags == 0 = succeed ?None w
	# (ok,f,w) = fopen TAGS_FILE FWriteText w
	| not ok = fail ("failed to open " +++ TAGS_FILE) w
	# f = f <<< tags
	# (ok,w) = fclose f w
	| not ok = fail ("failed to close " +++ TAGS_FILE) w
	| otherwise = succeed ?None w
where
	scanDirectories f st dirs w
		# (error_lists,(st,w)) = mapSt
			(\dir (st,w)
				# (errors,st,w) = scanDirectory f st dir w
				-> (errors, (st, w)))
			dirs (st, w)
		= (flatten error_lists, st, w)
