definition module Nitrile.CLI.Do

/**
 * Copyright 2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from System.FilePath import :: FilePath

from Nitrile.Package import :: Package

:: CLIDoOptions =
	{ list      :: !Bool //* Only list actions, do not run.
	, action    :: !?String //* The action to run.
	, arguments :: ![String] //* The command-line arguments for the action.
	}

do :: !CLIDoOptions !Package !FilePath !*World -> (!Bool, !*World)
