definition module Nitrile.CLI.Settings

/**
 * Copyright 2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError
from Text.YAML import :: YAMLErrorWithLocations
from Text.YAML.Compose import :: YAMLNode
from Text.YAML.Construct import generic gConstructFromYAML
from Text.YAML.Schemas import :: YAMLSchema

:: GlobalSettings =
	{ clm :: !?GlobalClmSettings
	}

:: GlobalClmSettings =
	{ linker        :: !?String //* The linker to use in clm build jobs
	, parallel_jobs :: !?Int //* The maximum number of jobs to run in parallel
	}

derive gConstructFromYAML GlobalSettings

defaultGlobalSettings :: GlobalSettings

getSettings :: !*World -> (!MaybeError String GlobalSettings, !*World)
