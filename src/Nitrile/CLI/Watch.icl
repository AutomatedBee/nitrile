implementation module Nitrile.CLI.Watch

/**
 * Copyright 2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Error
import Data.Func
import Data.Functor
import Data.List
from Data.Map import :: Map
import qualified Data.Map
import Data.Maybe
import Data.Tuple
import StdDebug
import StdEnv
import System.FilePath
import System.OS
import System.Process
import qualified System._Process
import System.SysCall
import Text

import Data.NamedList
import Nitrile.Build
import Nitrile.CLI
import Nitrile.CLI.Util
import Nitrile.CLI.Util.DependencySources
import Nitrile.Package
import System.Watchman

watch :: !GlobalOptions !CLIWatchOptions !Package !Package !*World -> (!Bool, !*World)
watch global_options options org_pkg=:{rules=Rules org_rules} pkg w
	# pkg = markAllDependenciesAsNonOptional pkg
	# (mbSources,w) = getDependencySources False global_options (?Just pkg) ?None w
	  dependency_sources = fromOk mbSources
	| isError mbSources = fail (fromError mbSources) w
	# paths =
		dependencyIncludePaths dependency_sources ++
		[ "." </> p
		\\ p <-
			srcInScope TestScope org_pkg ++
			flatten [maybe [] (map \src -> src.path) rule_settings.extra_src \\ (_,rule_settings) <- org_rules] ++
			flatten [fromMaybe [] clm.ClmBuildOptions.src \\ clm <- allClmOptions org_pkg]
		]
	# roots = ["./" : [path </> "" \\ DependencySource path (?Just _) <- 'Data.Map'.elems dependency_sources]]
		// NB: collectSubscriptions assumes trailing slashes
	# pathsByRoot = foldr (collectSubscriptions roots) 'Data.Map'.newMap paths
	# (mbWatchmans,w) = mapSt (uncurry watchDirectory o appSnd expr) ('Data.Map'.toList pathsByRoot) w
	| any isError mbWatchmans = fail (hd [e \\ Error e <- mbWatchmans]) w
	| otherwise = loop [] ?None options (map fromOk mbWatchmans) w
where
	expr source_dirs = AllOf
		[ AnyOf [Dirname dir (?Just (Ge 0)) \\ dir <- source_dirs]
		, FileType RegularFile
		, Not $ AnyOf $ map Suffix ["abc", "bc", "dcl~", "icl~", "o", "obj", "pbc", "tcl"]
		, Not $ WholenameRegex ".*\\.tmp[a-z0-9]*"
		, Not $ Wholename [fromMaybe clm.main clm.ClmBuildOptions.target \\ clm <- allClmOptions org_pkg]
		: map (Not o WholenameRegex) (maybe [] (\wo -> fromMaybe [] wo.WatchOptions.exclude) pkg.watch)
		]

	allClmOptions org_pkg =
		maybeToList ((\clm -> {clm & main=""}) <$> org_pkg.Package.clm_options) ++
		[options \\ {Build|script} <- allBuilds org_pkg, Clm options <- fromMaybe [] script] ++
		[options \\ {TestSpec|script} <- allTests org_pkg, Clm options <- fromMaybe [] script]
	allBuilds org_pkg=:{rules=Rules org_rules} =
		[b \\ (_,b) <- fromNamedList org_pkg.build] ++
		[b \\ (_,settings) <- org_rules, (_,b) <- fromNamedList settings.extra_build]
	allTests org_pkg=:{rules=Rules org_rules} =
		[t \\ (_,t) <- fromNamedList org_pkg.Package.tests] ++
		[t \\ (_,settings) <- org_rules, (_,t) <- fromNamedList settings.extra_tests]

	// Watchman can subscribe to changes within a certain `root` only, and each
	// watchman process is limited to a single root (see
	// https://github.com/facebook/watchman/issues/1063). When using local
	// dependencies, subscriptions may fall outside the current working
	// directory. Therefore we will start a separate process for each package
	// (either the current working directory or a local dependency), and in
	// this function determine which paths go with which root.
	collectSubscriptions :: ![FilePath] !FilePath !(Map FilePath [FilePath]) -> Map FilePath [FilePath]
	collectSubscriptions roots new seen = case root of
		?None -> abort "Internal error in collectSubscriptions\n"
		?Just root -> 'Data.Map'.alter (\paths -> ?Just (maybe [path] (\paths -> [path:paths]) paths)) root seen
			with path = new % (size root, size new-1)
	where
		root = listToMaybe (filter (flip startsWith new) roots)

loop :: ![WatchCommand] !(?(ProcessHandle, ProcessIO)) !CLIWatchOptions ![Watchman] !*World -> (!Bool, !*World)
loop queue mbNowRunning watchOptions wms w
	# (mbExitCodes,w) = mapSt (\wm -> checkProcess wm.processHandle) wms w
	| any isError mbExitCodes = fail "Failed to check watchman status." w
	| any isJust (map fromOk mbExitCodes) = fail "Watchman exited unexpectedly." w
	| isNone mbNowRunning = dispatchOrBlock queue wms w
	# (subH,subIO) = fromJust mbNowRunning
	# (mbExitCode,w) = checkProcess subH w
	| isError mbExitCode = fail "Failed to check subprocess status." w
	| isJust (fromOk mbExitCode) = dispatchOrBlock queue wms w
	/* We have a cancellable subprocess in mbNowRunning: pass IO from this
	 * process and check watchman for notifications */
	# (mbOutputs,w) = readPipeBlockingMulti [subIO.stdOut, subIO.stdErr : [wm.processIO.stdOut \\ wm <- wms]] w
	| isError mbOutputs = fail "Failed to read watchman/subprocess output." w
	# [subOut,subErr:wmOuts] = fromOk mbOutputs
	/* Forward subprocess output */
	# (io,w) = stdio w
	  io = io <<< subOut
	  (_,w) = fclose io w
	  (_,w) = fclose (stderr <<< subErr) w
	# mbWms = [appendOutput wmOut wm \\ wmOut <- wmOuts & wm <- wms]
	  wms = map fromOk mbWms
	| any isError mbWms = fail (hd [e \\ Error e <- mbWms]) w
	| any (\wm -> any (\r -> r=:({response= ?Just (ChangeNotification _)})) wm.responses) wms
		/* Stop the subprocess and start over */
		# w = log "Changes detected, stopping current command..." w
		# (_,w) = closeProcessIO subIO w
		# (_,w) = terminateProcess subH w
		= dispatchOrBlock [] wms w
	| otherwise
		= loop queue mbNowRunning watchOptions wms w
where
	dispatchOrBlock queue wms w
		# (mbWms,w) = readWatchmansNonBlocking wms w
		  wms = fromOk mbWms
		| isError mbWms
			= fail (fromError mbWms) w
		| any (\wm -> not (isEmpty wm.responses)) wms
			/* New notifications; discard any commands in the queue and restart */
			# files = take 10 [f \\ wm <- wms, {response= ?Just (ChangeNotification {files})} <- wm.responses, f <- files]
			# w = log (concat3 "Changes detected (" (join "; " files) "), rerunning...") w
			= dispatch (hd (watchOptions.commands)) (tl (watchOptions.commands)) [{wm & responses=[]} \\ wm <- wms] w
		| not (isEmpty queue)
			/* No new notifications; continue handling the queue */
			= dispatch (hd queue) (tl queue) wms w
		| otherwise
			/* Nothing to do; block on watchman */
			# w = log "Waiting for new changes..." w
			# (mbError,w) = 'System._Process'._blockAnyPipe [wm.processIO.stdOut \\ wm <- wms] w
			| isError mbError = fail "Failed to block on watchman." w
			| otherwise = loop queue ?None watchOptions wms w
	where
		readWatchmansNonBlocking [] w
			= (Ok [], w)
		readWatchmansNonBlocking [wm:wms] w
			# (mbOutput,w) = readPipeNonBlocking wm.processIO.stdOut w
			| isError mbOutput = (Error "Failed to read watchman output.", w)
			# mbWm = appendOutput (fromOk mbOutput) wm
			  wm = fromOk mbWm
			| isError mbWm = (liftError mbWm, w)
			# errors = concatMap (\r -> r.errors) wm.responses
			  warnings = concatMap (\r -> r.Response.warnings) wm.responses
			  wm & responses = filter (\r -> r=:({response= ?Just (ChangeNotification _)})) wm.responses
			| not (isEmpty errors)
				= (Error ("Watchman error: " +++ join "; " errors), w)
			# w = seqSt (\warn -> trace_n ("Watchman warning: " +++ warn)) warnings w
			# (mbWms,w) = readWatchmansNonBlocking wms w
			| isError mbWms = (liftError mbWms, w)
			| otherwise = (Ok [wm:fromOk mbWms], w)

	dispatch (CLICommand cmd opts) queue wm w
		# (ok, w) = performCommand cmd opts w
		| not ok
			# w = log ("Command failed" +++ if (isEmpty queue) "" "; terminating sequence...") w
			= loop [] ?None watchOptions wm w
		| otherwise
			= loop queue ?None watchOptions wm w
	dispatch (ShellCommand {command,cancellable}) queue wm w
		| not cancellable
			# (mbExitCode,w) = callProcessAndPassIO cmd args ?None w
			| isError mbExitCode
				# w = log (concat3 "Failed to run shell command (" command (if (isEmpty queue) ")..." "); terminating sequence...")) w
				= loop [] ?None watchOptions wm w
			| fromOk mbExitCode <> 0
				# w = log (concat3 "Shell command exited with " (toString (fromOk mbExitCode)) (if (isEmpty queue) "..." "; terminating sequence...")) w
				= loop [] ?None watchOptions wm w
			| otherwise
				= loop queue ?None watchOptions wm w
		| otherwise
			# (mbHandleAndIO,w) = runProcessPty cmd args ?None defaultPtyOptions w
			= case mbHandleAndIO of
				Ok (h,io)
					-> loop queue (?Just (h,io)) watchOptions wm w
				Error _
					# w = log (concat3 "Failed to run shell command (" command (if (isEmpty queue) ")..." "); terminating sequence...")) w
					-> loop queue ?None watchOptions wm w
	where
		// The POSIX script is based on http://unix.stackexchange.com/a/588275.
		// This kills the entire child process tree, without using a separate
		// process group ID for shell. (That would cause the child to hang
		// around if nitrile exits abnormally.)
		(cmd,args) = IF_WINDOWS
			("pwsh", ["-NonInteractive", "-Command", command])
			("/bin/sh", ["-e", "-c", "trap \"pkill -9 -g $$\" INT TERM; " +++ command])
