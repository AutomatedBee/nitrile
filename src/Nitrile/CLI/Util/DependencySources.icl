implementation module Nitrile.CLI.Util.DependencySources

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Control.Applicative
from Control.Monad import sequence, class Monad(bind), >>=, =<<, instance Monad ?
import Data.Error
import Data.Func
import Data.Functor
import Data.List
import qualified Data.Map
from Data.Map import :: Map, instance Functor (Map k)
import Data.Maybe
import Data.Tuple
import StdEnv
import System.File
import System.FilePath
import Text
import Text.GenJSON

import Data.NamedList
import Data.SemVer
import Data.SemVer.Constraint
import Data.SemVer.ConstraintSet
import Nitrile.CLI
import Nitrile.CLI.Util
import Nitrile.CLI.Util.InstalledPackages
import Nitrile.Constants
import Nitrile.Package

getDependencySources :: !Bool !GlobalOptions !(?Package) !(?Dependencies) !*World -> (!MaybeError String DependencySources, !*World)
getDependencySources include_global_pkgs opts=:{name,target,local_dependencies,scope,verbose} mbPkg mbExtraDependencies w
	// Get globally installed packages
	# (mbGloballyInstalledPackages,w) = if include_global_pkgs
		(getGloballyInstalledPackages (?Just target) w)
		(Ok 'Data.Map'.newMap, w)
	| isError mbGloballyInstalledPackages = (liftError mbGloballyInstalledPackages, w)
	# globallyInstalledPackages =
		[ ((name, version), path)
		\\ (name, versions) <- 'Data.Map'.toList (fromOk mbGloballyInstalledPackages)
		, {GlobalPackageInfo|version,path} <- versions
		]
	// Get dependencies and installed packages from all local dependencies
	# local_dependencies_list = 'Data.Map'.toList local_dependencies
	| isEmpty local_dependencies_list &&
		isEmpty (maybe [] (\pkg -> fromDependencyList pkg.Package.dependencies) mbPkg) &&
		isNone mbExtraDependencies =
			(Ok 'Data.Map'.newMap, w)
	# (mbLocalDependencies,w) = appFst sequence $ mapSt packageInfo local_dependencies_list w
	  localDependencies = fromOk mbLocalDependencies
	| isError mbLocalDependencies = (liftError mbLocalDependencies, w)
	# (mbAllInstalledPackages,w) = appFst sequence $ mapSt (installedDependencies o snd) local_dependencies_list w
	  allInstalledPackages = flatten (fromOk mbAllInstalledPackages)
	| isError mbAllInstalledPackages = (Error ("Failed to list installed dependencies: " +++ fromError mbAllInstalledPackages), w)
	// Get dependencies and installed packages of current package; combine
	# (theseInstalledPackages,w) = appFst (fromMaybe [] o error2mb) (installedDependencies "." w)
	# dependencies = combineDependencies $
		// See the rules documented in the dcl.
		map (\deps -> Dependencies ('Data.Map'.fromList (fromNamedList deps)))
		// Dependencies of the current package:
		[ maybe (NamedList []) (dependenciesInScope scope) mbPkg
		// Dependencies of local_dependencies:
		: map (dependenciesInScope UseScope) localDependencies
		] ++
		// Extra constraints:
		// - Have to use the current package's exact version (prevent taking the current package from a local_dependency's dependencies):
		[ if (isJust mbPkg)
			(Dependencies ('Data.Map'.singleton current_package (False, VersionConstraint [VersionEq current_version])))
			(Dependencies 'Data.Map'.newMap)
		// - Same for all the local_dependencies:
		, Dependencies ('Data.Map'.fromList
			[ (name, (False, VersionConstraint [VersionEq version]))
			\\ {Package|name= ?Just name,version= ?Just version} <- localDependencies
			])
		// - We only want packages that are actually installed somewhere:
		, (Dependencies ('Data.Map'.fromList
			[ (name, (True, VersionConstraint [VersionEq v \\ ((_, v), _) <- pkgGroup]))
			\\ pkgGroup <- partitionGroup ((==) `on` (fst o fst)) (theseInstalledPackages ++ allInstalledPackages ++ globallyInstalledPackages)
			, let ((name, _), _) = hd pkgGroup
			| not ('Data.Map'.member name local_dependencies) &&
				?Just name <> (mbPkg >>= \pkg -> pkg.Package.name)
			]))
		// - Extra dependencies specified by parameter
		, fromMaybe (Dependencies 'Data.Map'.newMap) mbExtraDependencies
		]
	// Get global constraint set
	# (mbConstraintSet,w) = getConstraintSet target w
	  (ConstraintSet constraintSet) = fromOk mbConstraintSet
	| isError mbConstraintSet = (liftError mbConstraintSet, w)
	// Remove current package and local_dependencies from constraint set:
	// the versions in nitrile.yml are enforced above and may be different if there are local changes.
	// name and version can only be ?None for the current package; ?Just-ness of local dependencies
	// is checked in `packageInfo`.
	# constraintSet = ConstraintSet (foldr
		(\{Package|name,version} -> 'Data.Map'.put
			(fromMaybe "CURRENT_PACKAGE" name)
			('Data.Map'.singleton (fromMaybe {major=0,minor=0,patch=0} version) (Dependencies 'Data.Map'.newMap)))
		constraintSet
		(maybeToList mbPkg ++ localDependencies))
	// Satisfy constraints
	# mbVersions = satisfyConstraints dependencies constraintSet
	| isError mbVersions = (Error ("Failed to satisfy constraints: " +++ fromError mbVersions +++ "."), w)
	// Get the selected sources. See the precedence order documented in the dcl.
	# sources = // globally installed packages
		'Data.Map'.fromList (map (appSnd \path -> DependencySource path ?None) globallyInstalledPackages)
	# sources = foldr // local_dependencies' LOCAL_PACKAGES_DIR
		(\(pkg_version, path) -> 'Data.Map'.put pkg_version (DependencySource path ?None))
		sources allInstalledPackages
	# sources = foldr // current directory's LOCAL_PACKAGES_DIR
		(\(pkg_version, path) -> 'Data.Map'.put pkg_version (DependencySource path ?None))
		sources theseInstalledPackages
	# sources = foldr // local_dependencies
		(\pkg=:{Package|name= ?Just name,version= ?Just version} -> 'Data.Map'.put
			(name, version)
			(DependencySource (fromJust ('Data.Map'.get name local_dependencies)) (?Just pkg)))
		sources localDependencies
	# sources = if (isJust mbPkg)
		('Data.Map'.put (current_package, current_version) (DependencySource "." mbPkg) sources)
		sources
	// Find the sources for the selected versions:
	# sources = 'Data.Map'.mapWithKey (\pkg v -> 'Data.Map'.get (pkg, v) sources) (fromOk mbVersions)
	= case [pkg \\ (pkg, ?None) <- 'Data.Map'.toList sources] of
		[pkg:_]
			-> (Error (concat3 "Could not find a source for dependency " pkg " (did you run nitrile fetch / nitrile global install?)."), w)
		[]
			# sources = fromJust <$> sources
			# w = if verbose (logSources ('Data.Map'.toList sources) w) w
			-> (Ok sources, w)
where
	current_package = fromMaybe "CURRENT_PACKAGE" (mbPkg >>= \pkg -> pkg.Package.name)
	current_version = fromMaybe {major=0,minor=0,patch=0} (mbPkg >>= \pkg -> pkg.Package.version)

	packageInfo :: !(!String, !FilePath) !*World -> (!MaybeError String Package, !*World)
	packageInfo (pkg, path) w
		# (mbPackage,w) = readPackage (?Just pkg) path w
		# mbPackage = mapError toString mbPackage
		# mbPackage =
			normalizePackage target <$> mbPackage >>= \info -> case info.Package.name of
				?Just name | name == pkg -> case info.Package.version of
					?Just _ -> Ok info
					_ -> Error (concat4 pkg " in " path " must have version set to use it as a local dependency")
				_ -> Error (concat4 path " does not contain a package called '" pkg "'")
		= (mbPackage, w)

	installedDependencies :: !FilePath !*World -> (!MaybeError String [((String, Version), FilePath)], !*World)
	installedDependencies path w
		# path = path </> localPackagesDir target
		# (mbPackages,w) = getLocallyInstalledPackages path w
		= ((\pkgs -> [(item, path </> pkg) \\ item=:(pkg,_) <- 'Data.Map'.toList pkgs]) <$> mbPackages, w)

	logSources sources w
		# (with_pkg,without_pkg) = partition (\(_,src) -> src=:(DependencySource _ (?Just _))) sources
		# w = log "Package sources:" w
		# w = foldl print w with_pkg
		# w = log "Dependency sources:" w
		# w = foldl print w without_pkg
		= w
	where
		print w (pkg, DependencySource path _) = log (concat4 "- " pkg " from " path) w

/* This is a record so that it can be extended more easily in the future and a
 * custom compare and equality can be defined. */
:: Key =
	{ dependencies :: ![(String, (Bool, VersionConstraint))] //* tThe names, optionality and version constraint of the dependencies in scope.
	}

:: *CachingDependencyResolver =
	{ include_global_pkgs :: !Bool
	, opts                :: !GlobalOptions
	, package             :: !Package
	, cache               :: !Map Key DependencySources
	}

instance < Key where (<) l r = l.Key.dependencies < r.Key.dependencies
instance == Key derive gEq

instance < Bool
where
	(<) False True = True
	(<) _ _ = False

setupCachingDependencyResolver :: !Bool !GlobalOptions !Package -> *CachingDependencyResolver
setupCachingDependencyResolver include_global_pkgs opts pkg =
	{ include_global_pkgs = include_global_pkgs
	, opts = opts
	, package = pkg
	, cache = 'Data.Map'.newMap
	}

getDependencySourcesWithCache :: !UsageScope ![String] !*CachingDependencyResolver !(?Dependencies) !*World
	-> *(!MaybeError String DependencySources, !*CachingDependencyResolver, !*World)
getDependencySourcesWithCache scope required_dependencies cdr=:{include_global_pkgs,opts,package,cache} mbExtraDependencies w
	# package = markDependenciesAsNonOptional required_dependencies package
	# (Dependencies combinedDependencies) = combineDependencies
		[ Dependencies ('Data.Map'.fromList $ fromNamedList $ dependenciesInScope scope package)
		, fromMaybe (Dependencies 'Data.Map'.newMap) mbExtraDependencies
		]
	# key = {Key | dependencies = 'Data.Map'.toList combinedDependencies}
	# (mbSources,cache) = 'Data.Map'.getU key cache
	  cdr & cache = cache
	| isJust mbSources = (Ok (fromJust mbSources), cdr, w)
	# (mbSources,w) = getDependencySources include_global_pkgs {GlobalOptions | opts & scope=scope} (?Just package) mbExtraDependencies w
	| isError mbSources = (mbSources, cdr, w)
	# cdr & cache = 'Data.Map'.put key (fromOk mbSources) cache
	= (mbSources, cdr, w)

dependencyIncludePaths :: !DependencySources -> [FilePath]
dependencyIncludePaths sources = concatMap paths ('Data.Map'.elems sources)
where
	paths (DependencySource path ?None) = [path </> "lib"]
	paths (DependencySource path (?Just pkg)) = [path </> dir \\ dir <- srcInScope UseScope pkg]

getClmVersion :: !DependencySources !*World -> *(!?Version, !*World)
getClmVersion sources w = case 'Data.Map'.get "base-clm" sources of
	?Just (DependencySource _ (?Just {Package|version})) ->
		(version, w)
	?Just (DependencySource path ?None)
		# (mbMetadata,w) = readFile (path </> PACKAGE_METADATA_FILE) w
		| isError mbMetadata -> (?None, w)
		| otherwise -> case fromJSON (fromString (fromOk mbMetadata)) of
			?Just (JSONObject kvs) -> (fromJSON =<< lookup "version" kvs, w)
			_ -> (?None, w)
	?None ->
		(?None, w)
