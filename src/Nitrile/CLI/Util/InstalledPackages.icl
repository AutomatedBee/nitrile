implementation module Nitrile.CLI.Util.InstalledPackages

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Control.Monad => qualified join
import Data.Error
import Data.Func
import Data.List
from Data.Map import :: Map
import qualified Data.Map
import Data.Maybe
import StdEnv
import System.Directory
import System.File
import System.FilePath
import System.OS
import System.OSError
import System._Pointer
import System.Process
import System.SysCall
import Text
import Text.GenJSON

import Data.SemVer
import Nitrile.CLI.Util
import Nitrile.Constants
import Nitrile.Registry
import Nitrile.Target

getGloballyInstalledPackages :: !(?Target) !*World -> (!MaybeError String (Map String [GlobalPackageInfo]), !*World)
getGloballyInstalledPackages mbTarget w
	# (mbGlobalDir,w) = globalNitrileDir w
	  globalPackagesDir = fromOk mbGlobalDir </> GLOBAL_PACKAGES_DIR
	| isError mbGlobalDir = (liftError mbGlobalDir, w)
	# (mbPaths,w) = readDirectory globalPackagesDir w
	  paths = [p \\ p <- fromOk mbPaths | p <> "." && p <> ".."]
	| isError mbPaths = (mapError (\(_,e) -> concat4 "Error searching for global packages in " globalPackagesDir ": " e) (liftError mbPaths), w)
	# (mbInfos,w) = mapSt (\pkg -> getGloballyInstalledVersions` globalPackagesDir pkg mbTarget) paths w
	| any isError mbInfos = (liftError (hd (filter isError mbInfos)), w)
	# infos = 'Data.Map'.fromList (zip2 paths (map fromOk mbInfos))
	| otherwise = (Ok infos, w)

getGloballyInstalledVersions :: !String !Target !*World -> (!MaybeError String [GlobalPackageInfo], !*World)
getGloballyInstalledVersions pkg target w
	# (mbGlobalDir,w) = globalNitrileDir w
	| isError mbGlobalDir = (liftError mbGlobalDir, w)
	| otherwise = getGloballyInstalledVersions` (fromOk mbGlobalDir </> GLOBAL_PACKAGES_DIR) pkg (?Just target) w

getGloballyInstalledVersions` :: !FilePath !String !(?Target) !*World -> (!MaybeError String [GlobalPackageInfo], !*World)
getGloballyInstalledVersions` globalPackagesDir pkg mbTarget w
	# (mbPaths,w) = readDirectory dir w
	  paths = fromOk mbPaths
	| isError mbPaths = (mapError (\(_,e) -> concat ["Error searching for global package ",pkg," in ",dir,": ",e]) (liftError mbPaths), w)
	# versions =
		[ {GlobalPackageInfo|version=fromOk mbVersion, path=dir </> path, target = fromJust mbPkgTarget}
		\\ path <- paths
		, let
			hyphen = indexOf "-" path
			mbVersion = parseVersion (path % (0, hyphen-1))
			mbPkgTarget = error2mb (parseTarget (path % (hyphen+1, size path-1)))
		| hyphen >= 0 &&
			isOk mbVersion &&
			maybe False (\pkgTarget -> maybe True (targetMatches pkgTarget) mbTarget) mbPkgTarget
		]
	= (Ok versions, w)
where
	dir = globalPackagesDir </> pkg

getLatestGloballyInstalledVersion :: !String !Target !*World -> (!MaybeError String GlobalPackageInfo, !*World)
getLatestGloballyInstalledVersion pkg target w
	# (mbInstalledVersions,w) = getGloballyInstalledVersions pkg target w
	  installedVersions = sortBy (\pkgInfo0 pkgInfo1 -> pkgInfo0.GlobalPackageInfo.version > pkgInfo1.GlobalPackageInfo.version) $ fromOk mbInstalledVersions
	| isError mbInstalledVersions = (liftError mbInstalledVersions, w)
	| isEmpty installedVersions = (Error $ "No globally installed version for " +++ pkg, w)
	| otherwise = (Ok $ hd installedVersions, w)

getLocallyInstalledPackages :: !FilePath !*World -> (!MaybeError String (Map String Version), !*World)
getLocallyInstalledPackages path w
	# (mbDir,w) = readDirectory path w
	| isError mbDir = (Error ("failed to read directory " +++ path), w)
	# pkgs = [path \\ path <- fromOk mbDir | path <> "." && path <> ".." && path <> ".home"]
	# (pkgVersions,w) = mapSt getVersion pkgs w
	| any isError pkgVersions
		= (liftError (hd (filter isError pkgVersions)), w)
		= (Ok ('Data.Map'.fromList (map fromOk pkgVersions)), w)
where
	getVersion pkg w
		# (mbMetadata,w) = readFile (path </> pkg </> PACKAGE_METADATA_FILE) w
		| isError mbMetadata
			= (Error ("failed to read metadata for " +++ pkg), w)
		| otherwise
			= case fromJSON (fromString (fromOk mbMetadata)) of
				?Just (JSONObject kvs) -> case lookup "version" kvs of
					?Just (JSONString v) -> case parseVersion v of
						Ok v -> (Ok (pkg, v), w)
						_ -> (Error ("unexpected version in metadata of " +++ pkg), w)
					_ ->
						(Error ("no version info in metadata of " +++ pkg), w)
				_ ->
					(Error ("failed to parse metadata of " +++ pkg), w)

:: PackageDownloads :== Map String (Version, FilePath)

prepareInstallPackages :: ![(String, Version)] !Registry !Target !FilePath !*World -> (!MaybeError String PackageDownloads, !*World)
prepareInstallPackages versions (Registry allPackages) target tmp_dir w
	# w = log (join " " ["Fetching" : [concat3 pkg "@" (toString version) \\ (pkg,version) <- versions]]) w
	# mbUrls = sequence
		[ mb2error (pkg +++ " not found in the registry.") (find (\p -> p.RegistryPackage.name == pkg) allPackages) >>= \regpkg ->
			mb2error (concat4 pkg "@" (toString version) " not found in the registry.") ('Data.Map'.get version regpkg.versions) >>= \regversion ->
			mb2error
				(concat4 pkg "@" (toString version) " has no package for this target.")
				(listToMaybe
					[ (pkg, version, url, pkg +++ ".tar.gz")
					\\ (t,{RegistryTargetInfo|url}) <- 'Data.Map'.toList regversion.RegistryVersionInfo.targets
					| targetMatches t target
					])
		\\ (pkg,version) <- versions
		]
	  urls = fromOk mbUrls
	| isError mbUrls = (liftError mbUrls, w)
	# (mbErr,w) = ensureDirectoryExists tmp_dir w
	| isError mbErr = (liftError (mapError snd mbErr), w)
	# (mbExitCode,w) = callProcessAndPassIO "curl"
		[ "-L", "-f", "--retry", "5", "-Z"
		: flatten [[url, "-o", tmp_file] \\ (_,_,url,tmp_file) <- urls]
		]
		(?Just tmp_dir) w
	# downloads = 'Data.Map'.fromList [(pkg, (version, file)) \\ (pkg,version,_,file) <- urls]
	| isError mbExitCode
		# w = cleanupPackageDownloads tmp_dir downloads w
		= (Error ("Failed to run curl: " +++ snd (fromError mbExitCode)), w)
	| fromOk mbExitCode <> 0
		# w = cleanupPackageDownloads tmp_dir downloads w
		= (Error "Download failed.", w)
	| otherwise
		= (Ok downloads, w)

installPackage :: !String !FilePath !PackageDownloads !*World -> (!MaybeError String (), !*World)
installPackage pkg dir downloads w
	# mbVersionAndFile = 'Data.Map'.get pkg downloads
	  (version,tmp_file) = fromJust mbVersionAndFile
	| isNone mbVersionAndFile = abort "Internal error in installPackage: package has not been downloaded yet\n"
	# (mbError,w) = ensureDirectoryExists dir w
	| isError mbError = (Error ("Failed to create directory: " +++ snd (fromError mbError)), w)
	# (mbExitCode,w) = callProcessAndPassIO "tar" ["xzmf", tmp_file, "--strip-components=1", "-C", dropDirectory dir] (?Just parent_dir) w
	| isError mbExitCode = (Error ("Failed to run tar: " +++ snd (fromError mbExitCode)), w)
	| fromOk mbExitCode <> 0 = (Error "Extraction failed.", w)
	# (mbError,w) = deleteFile (parent_dir </> tmp_file) w
	| isError mbError = (Error ("Failed to delete temporary file: " +++ snd (fromError mbError)), w)
	# (mbError,w) = patchMetadata version (dir </> PACKAGE_METADATA_FILE) w
	| isError mbError = (Error (concat4 "Failed to patch " PACKAGE_METADATA_FILE ": " (fromError mbError)), w)
	| otherwise = (Ok (), w)
where
	parent_dir = takeDirectory dir

	// The metadata file should always contain the version as it is used to
	// keep track of which versions are installed. If the package was generated
	// using an older version of nitrile and does not contain the version,
	// patch the file here.
	patchMetadata version path w
		# (mbContents,w) = readFile path w
		| isError mbContents
			= write (JSONObject [("version", toJSON version)]) w
		| otherwise
			= case fromJSON (fromString (fromOk mbContents)) of
				?Just (JSONObject kvs)
					| isJust (find ((==) "version" o fst) kvs)
						-> (Ok (), w)
						-> write (JSONObject [("version", toJSON version) : kvs]) w
				?Just _ ->
					(Error "unexpected contents", w)
				?None ->
					(Error "failed to parse contents", w)
	where
		write json w
			# (mbError,w) = writeFile path (toString json) w
			| isError mbError
				= (Error "failed to write", w)
				= (Ok (), w)

cleanupPackageDownloads :: !FilePath !PackageDownloads !*World -> *World
cleanupPackageDownloads tmp_dir downloads w = seqSt cleanup ('Data.Map'.toList downloads) w
where
	cleanup (_,(_,tmp_file)) w
		# (exi,w) = fileExists path w
		= if exi (snd (deleteFile path w)) w
	where
		path = tmp_dir </> tmp_file

linkExecutables :: !FilePath !FilePath !FilePath !*World -> (!MaybeError String (), !*World)
linkExecutables src dst rel w
	# (mbPaths,w) = readDirectory src w
	| isError mbPaths = (Ok (), w) // the package just doesn't have this type of binaries
	# (mbError,w) = ensureDirectoryExists dst w
	| isError mbError = (Error (concat4 "Failed to create " dst ": " (snd (fromError mbError))), w)
	# (mbErrors,w) = mapSt linkOrCopyFile [p \\ p <- fromOk mbPaths | p <> "." && p <> ".."] w
	| any isError mbErrors = (Error (fromError (hd (filter isError mbErrors))), w)
	| otherwise = (Ok (), w)
where
	linkOrCopyFile path w
		# (exi,w) = fileExists (dst </> path) w
		# w = if exi (log ("Removing old link/copy " +++ (dst </> path)) w) w
		# (mbError,w) = if exi (deleteFile (dst </> path) w) (Ok (), w)
		| isError mbError = (Error ("Failed to remove old link/copy: " +++ snd (fromError mbError)), w)
		# (mbError,w) = linkOrCopy w
		| isError mbError = (Error ("Failed to create link/copy: " +++ snd (fromError mbError)), w)
		| otherwise = (Ok (), w)
	where
		linkOrCopy :: !*World -> (!MaybeOSError (), !*World)
		linkOrCopy w
			| IF_WINDOWS True False // Windows has no `symlink`
				= copyFile (src </> path) (dst </> path) w
			# (ok,w) = symlink (packString (rel </> path)) (packString (dst </> path)) w
			| ok <> 0 = getLastOSError w
			| otherwise = (Ok (), w)
		where
			symlink :: !String !String !*World -> (!Int, !*World)
			symlink _ _ _ = code {
				ccall symlink "ss:I:A"
			}
