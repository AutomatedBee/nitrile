implementation module Nitrile.CLI.Util.Shell

/**
 * Copyright 2021-2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Error
import StdEnv
import StdMaybe
import System.File
import System.FilePath
import System.OS
import System.Process
import System.SysCall
import Text

runShellScript ::
	!(String .st *World -> (.st, *World))
	!(String .st *World -> (.st, *World))
	!(Int .st *World -> (.st, *World))
	![String] ![String] !FilePath
	!.st !*World -> (!MaybeError String .st, !*World)
runShellScript do_stdout do_stderr do_exitcode script args tmp_dir st w
	# cmd = join "\n" (script_header ++ script)
	# (mbError,w) = IF_WINDOWS (writeFile win_script_file cmd w) (Ok (), w)
	| isError mbError = (Error ("could not write " +++ win_script_file), w)
	# (mbHandleIO,w) = startProcess cmd w
	| isError mbHandleIO = (Error (snd (fromError mbHandleIO)), w)
	# (handle,io) = fromOk mbHandleIO
	# (runResult,w) = run do_stdout do_stderr do_exitcode handle io st w
	# (mbError,w) = IF_WINDOWS (deleteFile win_script_file w) (Ok (), w)
	= case runResult of
		Error e -> (Error (snd e), w)
		Ok res -> case mbError of
			Error e -> (Error ("could not delete " +++ win_script_file), w)
			Ok _ -> (Ok res, w)
where
	win_script_file = tmp_dir </> "nitrile-commands.ps1"
	script_header = IF_WINDOWS
		[ "$ErrorActionPreference = 'Stop'" // Stop on errors
		, "$PSNativeCommandUseErrorActionPreference = $true" // This includes non-zero exit codes from exes
		// Check that we can and do use PSNativeCommandUseErrorActionPreference:
		, "if ($PSVersionTable.PSVersion.Major -lt 7 -or ($PSVersionTable.PSVersion.Major -eq 7 -and $PSVersionTable.PSVersion.Minor -lt 3)) {"
		, "\tWrite-Output \"Warning: running pwsh < 7.3; exit codes of exes will not be recognized.\""
		, "} elseif ($PSVersionTable.PSVersion.Major -eq 7 -and $PSVersionTable.PSVersion.Minor -eq 3 -and !(Get-ExperimentalFeature PSNativeCommandErrorActionPreference).Enabled) {"
		, "\tWrite-Output \"Warning: PSNativeCommandErrorActionPreference is disabled; exit codes of exes will not be recognized.\""
		, "\tWrite-Output \"         Run 'pwsh -Command `\"Enable-ExperimentalFeature PSNativeCommandErrorActionPreference`\"' to fix this.\""
		, "}"
		]
		[]

	startProcess cmd w
		| IF_WINDOWS True False =
			runProcessIO "pwsh"
				[ "-NonInteractive"
				, "-Command"
				, "Set-ExecutionPolicy -Scope Process -Force RemoteSigned; . " +++ win_script_file
				: args
				]
				?None w
		| otherwise =
			runProcessPty "/bin/sh" ["-e", "-c", cmd, "nitrile-command" : args] ?None defaultPtyOptions w

	// NB: type is needed to ensure strictness of st and World
	run ::
		!(String .st *World -> (.st, *World))
		!(String .st *World -> (.st, *World))
		!(Int .st *World -> (.st, *World))
		!ProcessHandle !ProcessIO
		!.st !*World -> (!MaybeOSError .st, !*World)
	run do_stdout do_stderr do_exit_code handle io st w
		# (mbExitCode, w) = checkProcess handle w
		| isError mbExitCode = (liftError mbExitCode, w)
		# mbExitCode = fromOk mbExitCode
		| isJust mbExitCode = finish do_stdout do_stderr do_exit_code (fromJust mbExitCode) io st w
		# (mbSt, w) = readPipes do_stdout do_stderr io st w
		= case mbSt of
			Error e -> (Error e, w)
			Ok st -> run do_stdout do_stderr do_exit_code handle io st w

	readPipes do_stdout do_stderr io st w
		| IF_WINDOWS True False
			# (mbOutErr, w) = readPipeBlockingMulti [io.stdOut, io.stdErr] w
			| isError mbOutErr = (liftError mbOutErr, w)
			# [out,err:_] = map (replaceSubString "\r" "") (fromOk mbOutErr)
			# (st, w) = if (size out <> 0) (do_stdout out st w) (st, w)
			# (st, w) = if (size err <> 0) (do_stderr err st w) (st, w)
			= (Ok st, w)
		| otherwise /* We use a pseudo-terminal, which does not distinguish stdout and stdin */
			# (mbOut, w) = readPipeBlocking io.stdOut w
			| isError mbOut = (liftError mbOut, w)
			# out = replaceSubString "\r" "" (fromOk mbOut)
			# (st, w) = if (size out <> 0) (do_stdout out st w) (st, w)
			= (Ok st, w)

	finish do_stdout do_stderr do_exit_code exit_code io st w
		# (st, w) = do_exit_code exit_code st w
		# (res, w) = readPipes do_stdout do_stderr io st w
		# (_, w) = closePipe io.stdIn w
		  (_, w) = closePipe io.stdOut w
		  (_, w) = closePipe io.stdErr w
		= (res, w)
