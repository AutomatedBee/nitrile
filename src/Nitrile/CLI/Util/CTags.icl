implementation module Nitrile.CLI.Util.CTags

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Clean.Doc
import Clean.Parse
import Clean.Parse.Comments
import qualified Clean.PrettyPrint
from Clean.PrettyPrint import class cpp, instance cpp ParsedDefinition
import qualified Clean.Types
from Clean.Types import :: Type
import Clean.Types.CoclTransform
from Clean.Types.Util import class print(print), instance print Type
import Control.Applicative
import Control.Monad => qualified join
import Data.Either
import Data.Error
import Data.Func
import Data.Functor
import Data.List
import Data.Maybe
import qualified Data.Set
import Data.Tuple
import StdDebug
import StdEnv
import qualified syntax
from syntax import
	:: ClassDef{class_ident,class_pos},
	:: FileName,
	:: FunKind,
	:: FunSpecials,
	:: GenericDef{gen_ident,gen_pos,gen_type,gen_vars},
	:: Ident{id_name},
	:: LineNr,
	:: Module{mod_defs,mod_ident},
	:: Optional(Yes),
	:: ParsedDefinition(PD_Class,PD_Function,PD_Generic,PD_Type,PD_TypeSpec),
	:: ParsedExpr,
	:: ParsedTypeDef,
	:: Position(LinePos),
	:: Priority,
	:: Rhs,
	:: RhsDefsOfType,
	:: SymbolType,
	:: TypeDef{td_pos},
	:: TypeVar
import System.FilePath
import System.OSError
import Text

import Data.CTags

collectTags :: !Bool !FilePath !FilePath !*World -> *(!TagSet, !*World)
collectTags show_warnings_and_errors base path_without_extension w
| isMember path_without_extension ["_system", "_startup", "_startupTrace", "_startupProfile", "_startupProfileGraph"] = (toTagSet [], w)
# (mbDclComments,w) = scanComments (base </> path_without_extension +++ ".dcl") w
  dclComments = fromOk mbDclComments
# (mbDcl,w) = readModule (base </> path_without_extension +++ ".dcl") w
  dcl = fst (fromOk mbDcl)
  dcldefs = dcl.mod_defs
| isError mbDclComments || isError mbDcl = (toTagSet [], w)
# documentation = collectComments dclComments dcl
# (mbIcl,w) = readModule (base </> path_without_extension +++ ".icl") w
  icl = fst (fromOk mbIcl)
  icldefs = icl.mod_defs
  modname = icl.mod_ident.id_name
| isError mbIcl = (toTagSet [], w)
# tags = collectTags` show_warnings_and_errors path_without_extension modname dcldefs documentation icldefs
= (tags, w)

collectTags` :: !Bool !FilePath !String ![ParsedDefinition] !CollectedComments ![ParsedDefinition] -> TagSet
collectTags` show_warnings_and_errors path_without_extension modname dcldefs documentation icldefs
	# functions = combine pd_typespecs    dcldefs documentation icldefs
	  rules     = combine pd_rewriterules dcldefs documentation icldefs
	  generics  = combine pd_generics     dcldefs documentation icldefs
	  typedefs  = combine pd_types        dcldefs documentation icldefs
	  classes   = combine pd_classes      dcldefs documentation icldefs
	  modules   = if (modname=="") [] [{file=path_without_extension +++ ".dcl", ident=modname, address=Line 1, comments=[("module", modname),("thing","module")]}]
	= toTagSet (functions ++ rules ++ generics ++ typedefs ++ classes ++ modules)
where
	combine :: !([ParsedDefinition] CollectedComments -> [Tag]) ![ParsedDefinition] !CollectedComments ![ParsedDefinition] -> [Tag]
	combine find dcl dclComments [] = find dcl dclComments // Special case for things like _library.dcl
	combine find dcl dclComments icl = map addIclLine (find dcl dclComments)
	where
		addIclLine dcl_tag = case matching of
			[{address=Line l}:_] -> setComment ("icl", toString l) dcl_tag
			_ -> dcl_tag
		where
			matching =
				[ icl_tag
				\\ icl_tag <- icl_tags
				| dcl_tag.ident == icl_tag.ident &&
					lookup "thing" dcl_tag.comments == lookup "thing" icl_tag.comments
				]

		icl_tags = find icl emptyCollectedComments

	tag :: !String !String !Position ![Comment] -> Tag
	tag thing ident pos comments =
		{ ident    = ident
		, file     = path_without_extension +++ ".dcl"
		, address  = address
		, comments =
			[ ("thing", thing)
			, ("module", modname)
			: comments
			]
		}
	where
		address = case pos of
			LinePos _ l -> Line l
			_ -> if show_warnings_and_errors
				(trace_n ("Warning: no line number for " +++ ident +++ " in " +++ modname))
				id
				(Line 0)

	pd_typespecs :: ![ParsedDefinition] !CollectedComments -> [Tag]
	pd_typespecs defs comments =
		[ tag "function" id_name pos (functionComments (?Just ('Clean.Types'.toType t)))
		\\ PD_TypeSpec pos {id_name} p (Yes t) funspecs <- defs
		]

	pd_rewriterules :: ![ParsedDefinition] !CollectedComments -> [Tag]
	pd_rewriterules defs comments =
		[ tag "rule" id_name pos (functionComments type)
		\\ pd=:(PD_Function pos id=:{id_name} isinfix args rhs _) <- defs
		, let
			infixts = findTypeSpec id defs
			doc = findDoc pd comments <|> (flip findDoc comments =<< infixts)
			type = (docType =<< doc) <|> pdType pd
		]
	where
		findTypeSpec :: Ident [ParsedDefinition] -> ?ParsedDefinition
		findTypeSpec _ [] = ?None
		findTypeSpec id [pd:defs] = case pd of
			PD_TypeSpec _ id` prio _ _ | id`.id_name == id.id_name -> ?Just pd
			_ -> findTypeSpec id defs

		findDoc :: a CollectedComments -> ?FunctionDoc | commentIndex a
		findDoc id coll = getComment id coll >>= \doc -> docParseResultToMaybe (not o \w -> w=:UsedReturn) $ parseDoc doc

	pd_generics :: ![ParsedDefinition] !CollectedComments -> [Tag]
	pd_generics defs comments =
		[ tag "generic" id_name gen_pos
			[ ("generic_vars", join " " (map 'Clean.Types'.toTypeVar gen_vars))
			: functionComments (?Just ('Clean.Types'.toType gen_type))
			]
		\\ PD_Generic {gen_ident=id=:{id_name},gen_pos,gen_type,gen_vars} <- defs
		]

	pd_types :: ![ParsedDefinition] !CollectedComments -> [Tag]
	pd_types defs comments = flatten
		[
			[ tag "type" ('Clean.Types'.td_name td) td_pos
				[ ("typedef", toString (cleanWhiteSpaceAndTruncate 0 (fromString ('Clean.PrettyPrint'.cpp pd))))
				]
			] ++
			[ tag "constructor" cons td_pos
				[ ("type", ('Clean.Types'.td_name td))
				: functionComments (?Just type)
				]
			\\ (cons,type,_) <- 'Clean.Types'.constructorsToFunctions td
			] ++
			[ tag "recfield" field td_pos
				[ ("type", ('Clean.Types'.td_name td))
				: functionComments (?Just type)
				]
			\\ (field,type) <- 'Clean.Types'.selectorsToFunctions td
			]
		\\ pd=:(PD_Type ptd=:{td_pos}) <- defs
		, let td = 'Clean.Types'.toTypeDef ptd
		]
	where
		cleanWhiteSpaceAndTruncate _ [] = []
		cleanWhiteSpaceAndTruncate i [c:cs]
			| isSpace c
				| i >= 195 = ['// ...'] // generate no more than 200 characters
				# (_,cs) = span isSpace cs
				| isEmpty cs = []
				| isMember (hd cs) [',}'] = cleanWhiteSpaceAndTruncate i cs
				| otherwise = [' ':cleanWhiteSpaceAndTruncate (i+1) cs]
			| otherwise =
				[c:cleanWhiteSpaceAndTruncate (i+1) cs]

	pd_classes :: ![ParsedDefinition] !CollectedComments -> [Tag]
	pd_classes defs comments = flatten
		[
			[ tag "class" id_name class_pos []
			: map (setComments [("thing", "classmem"), ("class", id_name)]) $
				pd_typespecs clsdefs comments ++
				pd_rewriterules clsdefs comments
			]
		\\ pd=:(PD_Class {class_ident=id=:{id_name},class_pos} clsdefs) <- defs
		]

	functionComments :: !(?Type) -> [Comment]
	functionComments mbType = catMaybes
		[ tuple "funtype" <$> concat <$> print False <$> mbType
		]

	docParseResultToMaybe :: (ParseWarning -> Bool) (Either ParseError (d, [ParseWarning])) -> ?d
	docParseResultToMaybe showw (Left e) =
		if show_warnings_and_errors (traceParseError e) id ?None
	docParseResultToMaybe showw (Right (doc,ws)) =
		if show_warnings_and_errors (traceParseWarnings (filter showw ws)) id (?Just doc)
