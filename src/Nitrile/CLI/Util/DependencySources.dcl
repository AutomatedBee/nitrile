definition module Nitrile.CLI.Util.DependencySources

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError
from Data.Map import :: Map
from System.FilePath import :: FilePath

from Data.SemVer import :: Version
from Data.SemVer.ConstraintSet import :: Dependencies
from Nitrile.CLI import :: GlobalOptions
from Nitrile.Package import :: Package, :: UsageScope

:: DependencySources :== Map String DependencySource

:: DependencySource = DependencySource !FilePath !(?Package)

/**
 * Satisfies constraints and returns a map of packages, their location, and
 * their `Package` (in case of local dependencies). Dependencies are satisfied
 * according to the following rules:
 *
 * 1. The current package must be selected if the parameter is not `?None`.
 * 2. The extra dependencies must be satisfied if the parameter is not `?None`.
 * 3. Any local dependencies must be selected.
 * 4. Any dependencies of selected packages as specified in (a) their
 *    nitrile.yml or (b) the local copy of the registry must be satisfied.
 * 5. Only packages that are installed can be returned. The `Bool` parameter
 *    indicates whether only locally installed packages are included (`False`)
 *    or whether globally installed packages are included as well (`True`).
 *
 * The constraint solver will try to maximize the selected versions.
 *
 * If a package version is installed in more than one directory, the directory
 * is selected as follows:
 *
 * 1. The current directory.
 * 2. Specified local dependency directories.
 * 3. Packages from the `localPackagesDir`.
 * 4. Packages from the local dependency `localPackagesDir`s.
 * 5. Globally installed packages (if the corresponding `Bool` parameter is
 *    `True`).
 *
 * @param Whether globally installed packages can be returned.
 * @param The global CLI options.
 * @param The current package selected from `nitrile.yml`, if any (this may be
 *   `?None` when running `nitrile run`, for instance).
 * @param Any other dependencies that must be satisfied.
 * @result The dependency sources for the package, or a descriptive error.
 */
getDependencySources :: !Bool !GlobalOptions !(?Package) !(?Dependencies) !*World -> (!MaybeError String DependencySources, !*World)

:: *CachingDependencyResolver

/**
 * Create an initial (empty) dependencyresolver cache.
 *
 * @param Whether globally installed packages can be returned.
 * @param The global CLI options.
 * @param The current package selected from `nitrile.yml`.
 */
setupCachingDependencyResolver :: !Bool !GlobalOptions !Package -> *CachingDependencyResolver

/**
 * Get the dependency sources while maintaining a cache.
 *
 * @param The usage scope.
 * @param The required dependencies, i.e., packages that are marked as optional
 *   in the package configuration but must be included in the result.
 * @param The cache.
 * @param Any other dependencies that must be satisfied.
 * @result The dependency sources for the package, or a descriptive error.
 * @result The updated cache.
 */
getDependencySourcesWithCache :: !UsageScope ![String] !*CachingDependencyResolver !(?Dependencies) !*World
	-> *(!MaybeError String DependencySources, !*CachingDependencyResolver, !*World)

/**
 * Retrieves the paths to the dependencies to include when compiling given the
 * dependency sources of a package.
 *
 * @param The dependency sources.
 * @result The paths to include.
 */
dependencyIncludePaths :: !DependencySources -> [FilePath]

//* Retrieve the clm version from the dependencies.
getClmVersion :: !DependencySources !*World -> *(!?Version, !*World)
