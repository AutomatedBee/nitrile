definition module Nitrile.CLI.Util.InstalledPackages

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError
from Data.Map import :: Map
from System.FilePath import :: FilePath

from Data.SemVer import :: Version
from Nitrile.Registry import :: Registry
from Nitrile.Target import :: Target

:: GlobalPackageInfo =
	{ version :: !Version //* The version of the package.
	, target  :: !Target //* The target of the package.
	, path    :: !FilePath //* Path in which the global package is located.
	}

//* Get all globally installed packages, grouped by package name.
getGloballyInstalledPackages :: !(?Target) !*World -> (!MaybeError String (Map String [GlobalPackageInfo]), !*World)

//* Get all globally installed versions of a given package.
getGloballyInstalledVersions :: !String !Target !*World -> (!MaybeError String [GlobalPackageInfo], !*World)

//* Get latest globally installed version of a given package.
getLatestGloballyInstalledVersion :: !String !Target !*World -> (!MaybeError String GlobalPackageInfo, !*World)

getLocallyInstalledPackages :: !FilePath !*World -> (!MaybeError String (Map String Version), !*World)

//* See `prepareInstallPackages`.
:: PackageDownloads

/**
 * Prepare for installing packages. Separating this function from
 * `installPackage` allows for parallelizing downloads using curl.
 *
 * @param A list of package names and versions to be installed.
 * @param The registry.
 * @param The target to install for.
 * @param The parent directory of the directory that will be used to install
 *   packages.
 */
prepareInstallPackages :: ![(String, Version)] !Registry !Target !FilePath !*World -> (!MaybeError String PackageDownloads, !*World)

/**
 * Fetches and installs a package from the registry in the specified directory.
 * This should follow a call to `prepareInstallPackages`.
 *
 * @param The name of the package to install. The pacakge must have been
 *   included in the `prepareInstallPackages` call.
 * @param The directory the package should be installed to. The directory must
 *   be a subdirectory of that used for `prepareInstallPackages`.
 * @param The result of `prepareInstallPackages`.
 * @result In case of failure, a string describing the error that occurred.
 */
installPackage :: !String !FilePath !PackageDownloads !*World -> (!MaybeError String (), !*World)

/**
 * Deletes temporary files from `PackageDownloads`. This is only needed when
 * (a) not every package prepared with `prepareInstallPackages` has been
 * installed with `installPackage`, or (b) one or more calls to
 * `installPackage` failed.
 *
 * @param The directory from the `prepareInstallPackages` call.
 */
cleanupPackageDownloads :: !FilePath !PackageDownloads !*World -> *World

/**
 * Creates symlkinks for the executables within a directory.
 *
 * @param The directory to search for the executables.
 * @param The directory which will contain the symlink.
 * @param The relative path from the directory which contains the symlink to the directory to search.
 */
linkExecutables :: !FilePath !FilePath !FilePath !*World -> (!MaybeError String (), !*World)
