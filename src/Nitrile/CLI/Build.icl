implementation module Nitrile.CLI.Build

/**
 * Copyright 2021-2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Control.Monad
import Data.Error
import Data.Func
import Data.Functor
import Data.List
import qualified Data.Map
import Data.Maybe
import qualified Data.Set
import StdEnv
import System.Directory
import System.File
import System.FilePath
import System.OS
from Text import class Text, instance Text String, concat3
import qualified Text
import Text.GenJSON

import Data.NamedList
import Nitrile.Build
import Nitrile.CLI
import Nitrile.CLI.Test
import Nitrile.CLI.Util
import Nitrile.CLI.Util.DependencySources
import Nitrile.CLI.Util.Shell
import Nitrile.Constants
import Nitrile.Package

build :: !GlobalOptions !CLIBuildOptions !(?CLITestRunnerParallelization) !Package !FilePath !*World -> (!Bool, !*World)
build global_options options parallelization pkg tmp_dir w
	# w = logWarningForUnspecificTarget "build" global_options.GlobalOptions.target w
	| options.CLIBuildOptions.list
		= (True, listBuilds pkg.Package.build w)
	# dependency_resolver = setupCachingDependencyResolver False global_options pkg
	# w = setupEnvironmentVariables global_options.GlobalOptions.target w
	# mbGoals = maybe Ok onlyNames options.CLIBuildOptions.only pkg.Package.build
	  goals = fromOk mbGoals
	| isError mbGoals = fail ("Unknown build specified with --only: " +++ 'Text'.join ", " (fromError mbGoals)) w
	# (result,_,w) = runBuilds global_options pkg parallelization goals tmp_dir dependency_resolver w
	| isError result
		= fail ("Failed to build: " +++ fromError result) w
		= succeed ?None w

listBuilds :: !(NamedList Build) !*World -> *World
listBuilds builds w = seqSt (log o fst) (fromNamedList builds) w

runBuilds :: !GlobalOptions !Package !(?CLITestRunnerParallelization) !(NamedList Build) !FilePath !*CachingDependencyResolver !*World -> *(!MaybeError String (), !*CachingDependencyResolver, !*World)
runBuilds options pkg parallelization (NamedList goals) tmp_dir dependency_resolver w
	# mbBuildOrder = determineBuildOrder (map fst goals) pkg.Package.build
	| isError mbBuildOrder = (liftError mbBuildOrder, dependency_resolver, w)
	| otherwise = runBuilds` (fromOk mbBuildOrder) dependency_resolver w
where
	runBuilds` :: ![(String, Build)] !*CachingDependencyResolver !*World -> *(!MaybeError String (), !*CachingDependencyResolver, !*World)
	runBuilds` [] dependency_resolver w = (Ok (), dependency_resolver, w)
	runBuilds` [(goal,build):goals] dependency_resolver w
		# (mbErr,dependency_resolver,w) = runBuild options pkg parallelization build tmp_dir dependency_resolver w
		| isError mbErr = (mbErr, dependency_resolver, w)
		| otherwise = runBuilds` goals dependency_resolver w

runBuild :: !GlobalOptions !Package !(?CLITestRunnerParallelization) !Build !FilePath !*CachingDependencyResolver !*World -> *(!MaybeError String (), !*CachingDependencyResolver, !*World)
runBuild options pkg parallelization build tmp_dir dependency_resolver w
	# (mbSources,dependency_resolver,w) = getDependencySourcesWithCache options.GlobalOptions.scope (fromMaybe [] build.Build.required_dependencies) dependency_resolver ?None w
	  dependency_sources = fromOk mbSources
	| isError mbSources = (liftError mbSources, dependency_resolver, w)
	# (mbClmVersion,w) = getClmVersion dependency_sources w
	# (mbError,w) = runCommands
		(\s st w -> (st, let (io,w`) = stdio w in snd (fclose (io <<< s) w`)))
		(\s st w -> (st, snd (fclose (stderr <<< s) w)))
		(\e st w -> (?Just e, w))
		parallelization options.GlobalOptions.clm_linker options.GlobalOptions.parallel_jobs mbClmVersion
		(flatten [srcInScope options.GlobalOptions.scope pkg, dependencyIncludePaths dependency_sources])
		(fromMaybe [] build.Build.script)
		tmp_dir ?None w
	| isError mbError
		= (liftError mbError, dependency_resolver, w)
	| fromOk mbError <> ?Just 0
		= (Error ("commands failed with exit code " +++ toString (fromJust (fromOk mbError))), dependency_resolver, w)
		= (Ok (), dependency_resolver, w)

determineBuildOrder :: ![String] !(NamedList Build) -> MaybeError String [(String, Build)]
determineBuildOrder goals allBuilds = reverse <$> doMany 'Data.Set'.newSet [] goals
where
	doMany seen done [] = Ok done
	doMany seen done [goal:goals] =
		doOne seen done goal >>= \done ->
		doMany seen done goals

	doOne seen done goal
		| isJust (lookup goal done)
			= Ok done
		| 'Data.Set'.member goal seen
			= Error ("circular dependency involving " +++ goal)
		| otherwise
			= case lookup goal (fromNamedList allBuilds) of
				?None ->
					Error ("unknown goal " +++ goal)
				?Just build ->
					(\done -> [(goal, build):done]) <$>
					doMany ('Data.Set'.insert goal seen) done (fromMaybe [] build.Build.depends)

runCommands ::
	!(String .st *World -> (.st, *World))
	!(String .st *World -> (.st, *World))
	!(Int .st *World -> (.st, *World))
	!(?CLITestRunnerParallelization) !(?String) !(?Int) !(?Version) ![FilePath] ![BuildCommand]
	!FilePath !.st !*World -> (!MaybeError String .st, !*World)
runCommands do_stdout do_stderr do_exitcode parallelization clm_linker max_parallel_jobs clm_version dirs commands tmp_dir st w
	# (mbErrors,w) = mapSt ensureDirectoryExists (filter (\s -> size s > 0) requiredDirectories) w
	| any isError mbErrors = (Error (snd (fromError (hd (filter isError mbErrors)))), w)
	# mbScript = flatten <$> mapM (toCommands parallelization clm_linker max_parallel_jobs clm_version dirs) commands
	  script = fromOk mbScript
	| isError mbScript = (liftError mbScript, w)
	| otherwise = runShellScript do_stdout do_stderr do_exitcode script [] tmp_dir st w
where
	requiredDirectories =
		[ takeDirectory (fromMaybe options.main options.ClmBuildOptions.target)
		\\ Clm options <- commands
		]

toCommands :: !(?CLITestRunnerParallelization) !(?String) !(?Int) !(?Version) ![FilePath] !BuildCommand -> MaybeError String [String]
toCommands _ _ _ ?None _ (Clm _) = Error "clm is used but not installed as a dependency"
toCommands _ clm_linker max_parallel_jobs (?Just clm_version) dirs (Clm options) =
	toClmOptions clm_version clm_linker max_parallel_jobs dirs options <$&> \(config_options, path_options, code_options, application_options) ->
		[ 'Text'.join " " $
			["clm":config_options ++ path_options] ++
			[opt \\ opt <- code_options | opt <> "-fusion" && opt <> "-generic_fusion"] ++
			["-O","_system"]
		, 'Text'.join " " $
			["clm":config_options ++ path_options ++ code_options ++ application_options ++ name_options]
		]
		with name_options = [options.main, "-o", fromMaybe options.main options.ClmBuildOptions.target]
toCommands parallelization _ _ _ _ (TestRunner {TestRunnerOptions|junit_xml,output_format,tests}) = Ok [cmd]
where
	cmd = 'Text'.join " " $
		[ "test-runner"
		, "--output-format", if (output_format=:(?Just JSON)) "json" "human"
		] ++
		other_options ++
		runs
	other_options = flatten $ catMaybes
		[ (\path -> ["--junit", path]) <$> junit_xml
		, (\{index,total} -> ["--parallel-job", concat3 (toString index) "/" (toString total)]) <$> parallelization
		]
	runs = flatten
		[ ["-r", executable] ++ flatten [["-O", opt] \\ opt <- fromMaybe [] options]
		\\ {executable,options} <- tests
		]
toCommands _ _ _ _ _ (SystemCommand cmd) = Ok [cmd]
