implementation module Nitrile.CLI.Global

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Error
from Data.Foldable import class Foldable
import Data.Func
import Data.Functor
import qualified Data.Map
import Data.Maybe
from Data.Set import :: Set, instance Foldable Set
import qualified Data.Set
import StdEnv
import System.Directory
import System.File
import System.FilePath
import System.OS
import System.OSError
from System._Pointer import packString
import System.Process
import System.SysCall
import Text

import Data.SemVer
import Nitrile.CLI.Util
import Nitrile.CLI.Util.InstalledPackages
import Nitrile.Constants
import Nitrile.Registry
import Nitrile.Target

NITRILE_BIN_FILES :== IF_WINDOWS ["Nitrile.exe", "libz3.dll"] ["nitrile"]

global :: !Target !CLIGlobalCommand !*World -> (!Bool, !*World)
global target (Install pkg mbVersion) w
	# (mbRegistry,w) = getRegistry w
	  registry = fromOk mbRegistry
	| isError mbRegistry = fail (fromError mbRegistry) w
	# (mbGlobalDir,w) = globalNitrileDir w
	  globalDir = fromOk mbGlobalDir
	| isError mbGlobalDir = fail (fromError mbGlobalDir) w
	# (mbVersion,w) = selectVersion registry mbVersion w
	  version = fromOk mbVersion
	| isError mbVersion = fail (fromError mbVersion) w
	# path = globalDir </> GLOBAL_PACKAGES_DIR </> pkg </> concat3 (toString version) "-" (toString target)
	# (alreadyInstalled,w) = fileExists path w
	| alreadyInstalled
		= succeed (?Just (concat [pkg," ",toString version," is already installed in ",path,"; skipping installation."])) w
	# (mbDownloadedPackages,w) = prepareInstallPackages [(pkg,version)] registry target (takeDirectory path) w
	  downloads = fromOk mbDownloadedPackages
	| isError mbDownloadedPackages = fail (fromError mbDownloadedPackages) w
	# (mbErr,w) = installPackage pkg path downloads w
	| isError mbErr = fail (fromError mbErr) (cleanupPackageDownloads (takeDirectory path) downloads w)
	# (mbErr,w) = regenerateGlobalBinDirectory w
	| isError mbErr = fail (fromError mbErr) w
	# (mbErr,w) = if (pkg == "nitrile" && version > currentVersion)
		(handleChangeToNitrile globalDir target (?Just {version=version, target=target, path=path}) w)
		(Ok (), w)
	| isError mbErr = fail (fromError mbErr) w
	| otherwise = succeed ?None w
where
	selectVersion _ (?Just version) w = (Ok version, w)
	selectVersion (Registry registry) _ w
		| isEmpty versions = (Error "There are no versions of this package in the registry.", w)
		| otherwise = (Ok (maxList versions), w)
	where
		versions = [v \\ {name,versions} <- registry, v <- 'Data.Map'.keys versions | name == pkg]

global target List w
	# (mbPkgs,w) = getGloballyInstalledPackages (?Just target) w
	| isError mbPkgs = fail (fromError mbPkgs) w
	# pkgs = sort
		[ (pkg,version,target)
		\\ (pkg,vs) <- 'Data.Map'.toList (fromOk mbPkgs)
		, {GlobalPackageInfo|version,target} <- vs
		]
	# w = seqSt (log o entry) pkgs w
	= succeed ?None w
where
	specificTarget = not $
		target.platform=:AnyPlatform ||
		target.architecture.family=:AnyArchitectureFamily ||
		target.architecture.bitwidth=:AnyBitwidth

	entry (pkg,version,target) = if specificTarget
		(concat3 pkg " " (toString version))
		(concat5 pkg " " (toString version) " " (toString target))

global target (Remove pkg version) w
	# (mbGlobalDir,w) = globalNitrileDir w
	  globalDir = fromOk mbGlobalDir
	| isError mbGlobalDir = fail (fromError mbGlobalDir) w
	# path = globalDir </> GLOBAL_PACKAGES_DIR </> pkg </> concat3 (toString version) "-" (toString target)
	# (exi,w) = fileExists path w
	| not exi = fail (concat4 pkg " " (toString version) " is not installed globally.") w
	# (mbInstalledVersions,w) = getGloballyInstalledVersions pkg target w
	| isError mbInstalledVersions = fail (fromError mbInstalledVersions) w
	# installedVersions = fromOk mbInstalledVersions
	| installedVersions=:[_] && pkg == "nitrile" && target == currentTarget = fail ("The last version of nitrile may not be removed.") w
	# (mbErr,w) = recursiveDelete path w
	| isError mbErr = fail (concat3 "Failed to remove the package: " (snd (fromError mbErr)) ".") w
	# (mbErr,w) = if (pkg == "nitrile" && version == currentVersion) (handleChangeToNitrile globalDir target ?None w) (Ok (), w)
	# (mbErr,w) = regenerateGlobalBinDirectory w
	| isError mbErr = fail (fromError mbErr) w
	| otherwise = succeed (?Just (concat5 "Removed " pkg " " (toString version) ".")) w

// Regenerate the complete bin directory.
regenerateGlobalBinDirectory :: !*World -> (MaybeError String (), !*World)
regenerateGlobalBinDirectory w
	// Tell the user what we're going to do as this function may generate errors that
	// have nothing to do with the package that is currently being installed / removed.
	# w = log "Regenerating scripts for global executables..." w
	# (mbGlobalDir,w) = globalNitrileDir w
	  globalBinDir = fromOk mbGlobalDir </> "bin"
	| isError mbGlobalDir = (liftError mbGlobalDir, w)
	# (mbBins,w) = readDirectory globalBinDir w
	  bins = fromOk mbBins
	| isError mbBins = (Error (concat5 "Failed to read " globalBinDir ": " (snd (fromError mbBins)) "."), w)
	# (mbErrs,w) = mapSt
		(\bin w -> if (isMember bin NITRILE_BIN_FILES) (Ok (), w) (recursiveDelete (globalBinDir </> bin) w))
		(filter (\p -> p <> "." && p <> "..") bins) w
	| any isError mbErrs = (hd [Error (snd e) \\ Error e <- mbErrs], w)
	# (mbPackages,w) = getGloballyInstalledPackages ?None w
	| isError mbPackages = (liftError mbPackages, w)
	# packages =
		[ (pkg, info)
		\\ (pkg, installedVersions) <- 'Data.Map'.toList (fromOk mbPackages)
		, info <- installedVersions
		| pkg <> "nitrile"
		]
	# (mbBinToPkgMap,w) = collectBinaries packages 'Data.Map'.newMap w
	  binToPkgMap = fromOk mbBinToPkgMap
	| isError mbBinToPkgMap = (liftError mbBinToPkgMap, w)
	# (mbErrs,w) = mapSt (uncurry (createRunScript globalBinDir)) ('Data.Map'.toList binToPkgMap) w
	| any isError mbErrs = (hd [err \\ err=:(Error e) <- mbErrs], w)
	| otherwise = (Ok (), w)
where
	//* Retrieves all packages (values of map) that distribute a binary (key of map).
	collectBinaries :: ![(String, GlobalPackageInfo)] !(Map String [(String, Target)]) !*World -> (!MaybeError String (Map String [(String, Target)]), !*World)
	collectBinaries [] acc w = (Ok acc, w)
	collectBinaries [(pkg, info=:{version,target,path}):rest] acc w
		# pkgBinPath = path </> "bin"
		# (exi,w) = fileExists pkgBinPath w
		| not exi = collectBinaries rest acc w // The package has no binaries
		# (mbBins,w) = readDirectory pkgBinPath w
		  bins = [p \\ p <- fromOk mbBins | p <> "." && p <> ".."]
		| isError mbBins = (Error (concat5 "Failed to read " pkgBinPath ": " (snd (fromError mbBins)) "."), w)
		# acc = foldr ('Data.Map'.alter (?Just o add (pkg, target) o fromMaybe [])) acc bins
		= collectBinaries rest acc w
	where
		add x xs = if (isMember x xs) xs [x:xs]

	createRunScript :: !FilePath !String ![(String, Target)] !*World -> (!MaybeError String (), !*World)
	createRunScript globalBinDir bin pkgs w
		# ((pkg,target),w) = choosePackage pkgs w
		# (mbErr,w) = writeFile file (script pkg target) w
		| isError mbErr = (mapError (\e -> concat ["Error creating run script for ",pkg," in ",globalBinDir,": ",toString e,"."]) mbErr, w)
		| IF_WINDOWS True False
			= (Ok (), w)
		# (res,w) = chmod (packString file) 0755 w
		| res <> 0
			# (Error (_,err),w) = getLastOSError w
			= (Error (concat ["Error making run script executable for ",pkg," in ",globalBinDir,": ",err,"."]), w)
		| otherwise = (Ok (), w)
	where
		file = IF_WINDOWS (globalBinDir </> bin +++ ".ps1") (globalBinDir </> bin)

		script pkg target = concat
			[ IF_WINDOWS "" "#!/bin/sh\n"
			, "nitrile --arch=", toString target.architecture, " --platform=", toString target.platform
			, " run ", pkg, " ", bin, IF_WINDOWS " @args\r\n" " $@\n"
			]

		choosePackage pkgs w = case pkgs of
			[pkg]
				-> (pkg, warnForOtherTarget pkg w)
			pkgs
				// Prefer packages with a matching target, otherwise pick one at random
				# chosen = hd (filter (targetMatches currentTarget o snd) pkgs ++ pkgs)
				# w = warnForOtherTarget chosen w
				// Print warnings for the packages that we ignored
				# w = foldr (warnForConflict chosen) w pkgs
				-> (chosen, w)
		where
			warnForOtherTarget (pkg,target) w
				| targetMatches currentTarget target
					= w
					= log warning w
			where
				warning = concat
					[ "Warning: adding run script for ",bin," from ",pkg,":",toString target," to ",globalBinDir
					, " (no binaries for ",toString currentTarget," were found)."
					]

			warnForConflict chosen=:(cpkg,ctarget) other=:(opkg,otarget) w
				| chosen == other
					= w // This may happen if the same package is installed with multiple targets
					= log warning w
			where
				warning = concat
					[ "Warning: not adding run script for ",bin," from ",opkg,":",toString otarget
					, " (",cpkg,":",toString ctarget," contains an executable with the same name)."
					]

		chmod :: !String !Int !*env -> (!Int, !*env)
		chmod _ _ _ = code {
			ccall chmod "sI:I:A"
		}

handleChangeToNitrile :: !FilePath !Target !(?GlobalPackageInfo) !*World -> (!MaybeError String (), !*World)
handleChangeToNitrile globalDir target mbInfo w
	# desc = if (isJust mbInfo) "this" "the latest"
	# (mbInfo,w) = case mbInfo of
		?Just info -> (Ok info, w)
		?None -> getLatestGloballyInstalledVersion "nitrile" target w
	  {GlobalPackageInfo|version,path} = fromOk mbInfo
	| isError mbInfo = (liftError mbInfo, w)
	| IF_WINDOWS True False
		# message = concat
			[ "To use ",desc," Nitrile version from the command line, please run the following powershell command: \n"
			, "Copy-Item -Path ",path </> "bin" </> "*"," -Destination ",globalBinDir," -Recurse"
			]
		= (Ok (), log message w)
	| targetMatches currentTarget target
		= linkExecutables
			(path </> "bin")
			globalBinDir
			(".." </> "packages" </> "nitrile" </> concat3 (toString version) "-" (toString target) </> "bin")
			w
	| otherwise
		# message = concat
			[ "Not updating Nitrile symlink in ", globalBinDir
			, " as the installed target ", (toString target)
			, " does not match the current target ", (toString currentTarget), "."
			]
		= (Ok (), log message w)
where
	globalBinDir = globalDir </> "bin"
