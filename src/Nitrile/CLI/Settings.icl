implementation module Nitrile.CLI.Settings

/**
 * Copyright 2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Error
import StdEnv
import System.File
import System.FilePath
import Text
import Text.YAML

import Nitrile.CLI.Util
import Nitrile.Constants

derive gConstructFromYAML GlobalSettings, GlobalClmSettings

defaultGlobalSettings :: GlobalSettings
defaultGlobalSettings =
	{ clm = ?None
	}

getSettings :: !*World -> (!MaybeError String GlobalSettings, !*World)
getSettings w
	# (mbDir,w) = globalNitrileDir w
	  dir = fromOk mbDir
	| isError mbDir =
		(liftError mbDir, w)
	# (exi,w) = fileExists (dir </> SETTINGS_FILE) w
	| not exi = (Ok defaultGlobalSettings, w)
	# (mbSettings,w) = readFile (dir </> SETTINGS_FILE) w
	| isError mbSettings
		= (Error (concat4 "Failed to read " (dir </> SETTINGS_FILE) ": " (toString (fromError mbSettings))), w)
	# (mbSettings) = loadYAML coreSchema (fromOk mbSettings)
	  settings = fst (fromOk mbSettings)
	| isError mbSettings
		= (Error (concat4 "Failed to parse " (dir </> SETTINGS_FILE) ": " (printYAMLError (fromError mbSettings))), w)
		= (Ok settings, w)
