definition module Nitrile.CLI.Build

/**
 * Copyright 2021-2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError
from Data.Map import :: Map
from System.FilePath import :: FilePath

from Data.NamedList import :: NamedList
from Nitrile.Build import :: Build
from Nitrile.CLI import :: GlobalOptions
from Nitrile.CLI.Test import :: CLITestRunnerParallelization
from Nitrile.CLI.Util.DependencySources import :: CachingDependencyResolver
from Nitrile.Package import :: Package

:: CLIBuildOptions =
	{ list :: !Bool //* Only list build goals, do not build.
	, only :: !?[String] //* Build only the specified goals.
	}

/**
 * The build CLI command.
 *
 * @param Global options.
 * @param Build options.
 * @param Test parallelization options.
 * @param Package.
 * @param Temporary directory.
 */
build :: !GlobalOptions !CLIBuildOptions !(?CLITestRunnerParallelization) !Package !FilePath !*World -> (!Bool, !*World)

/**
 * Build targets. Errors are not capitalized.
 *
 * @param Global options.
 * @param Package.
 * @param Test parallelization options.
 * @param The targets to build.
 * @param Temporary directory.
 */
runBuilds :: !GlobalOptions !Package !(?CLITestRunnerParallelization) !(NamedList Build) !FilePath !*CachingDependencyResolver !*World -> *(!MaybeError String (), !*CachingDependencyResolver, !*World)

/**
 * Build one target. Errors are not capitalized.
 *
 * @param Global options.
 * @param Package.
 * @param Test parallelization options.
 * @param The target to build.
 * @param Temporary directory.
 */
runBuild :: !GlobalOptions !Package !(?CLITestRunnerParallelization) !Build !FilePath !*CachingDependencyResolver !*World -> *(!MaybeError String (), !*CachingDependencyResolver, !*World)
