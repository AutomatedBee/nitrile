implementation module Nitrile.CLI.Publish

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Error
import Data.Functor
import qualified Data.Map
import Internet.HTTP
import Internet.HTTP.TCPIP
import StdEnv
import StdMaybe
import System.Directory
import System.Environment
import System.File
import System.OS
import System.Process
import System.SysCall
import Text
import Text.GenJSON

import Data.SemVer
import Nitrile.CLI.Util
import Nitrile.Constants
import Nitrile.Metadata
import Nitrile.Package

fieldsMissingForPublish :: !Package -> [String]
fieldsMissingForPublish pkg = catMaybes
	[ "name"          unless pkg.name
	, "maintainer"    unless pkg.Package.maintainer
	, "contact_email" unless pkg.Package.contact_email
	, "url"           unless pkg.url
	, "description"   unless pkg.Package.description
	, "version"       unless pkg.Package.version
	, "license"       unless pkg.Package.license
	]
where
	(unless) field val = if (isJust val) ?None (?Just field)

publish :: !CLIPublishOptions !Package !Package !*World -> (!Bool, !*World)
publish options org_pkg pkg w
	# missingFields = fieldsMissingForPublish pkg
	| not (isEmpty missingFields) = fail ("The following fields are required for publication: " +++ join ", " missingFields) w
	// Get necessary variables
	# (mbToken,w) = getEnvironmentVariable "CI_JOB_TOKEN" w
	| isNone mbToken = fail "CI_JOB_TOKEN is not set. Are you running this from GitLab CI?" w
	# token = fromJust mbToken
	# (mbUrl,w) = getEnvironmentVariable "CI_API_V4_URL" w
	| isNone mbUrl = fail "CI_API_V4_URL is not set. Are you running this from GitLab CI?" w
	# api_url = fromJust mbUrl
	# (mbProject,w) = getEnvironmentVariable "CI_PROJECT_ID" w
	| isNone mbProject = fail "CI_PROJECT_ID is not set. Are you running this from GitLab CI?" w
	# project = fromJust mbProject
	// Create metadata
	# (git_ref,w) = getGitRefMatching pkg.Package.version w
	# (mbError,w) = writeFile METADATA_FILE (toString (toJSON (toMetadata git_ref org_pkg)) +++ "\n") w
	| isError mbError = fail ("Failed to create metadata file: " +++ toString (fromError mbError)) w
	# w = log ("Created " +++ METADATA_FILE +++ ".") w
	// Check if files exist locally
	# (mbError,w) = checkLocalPackageFiles w
	| isError mbError = fail (fromError mbError) w
	// Check if files already exist on GitLab
	# w = log "Checking if files already exist on GitLab..." w
	# (mbError,w) = checkIfFilesExistOnGitLab api_url project allFiles w
	| isError mbError = fail (fromError mbError) w
	// Upload files
	# (mbError,w) = uploadFiles token api_url project allFiles w
	| isError mbError = fail (fromError mbError) w
	// Ping registry to fetch new information from GitLab
	# w = log "Requesting registry update..." w
	# (mbResponse,w) = doHTTPRequestFollowRedirects updateRequest 10000 5 w
	| isError mbResponse = fail ("Failed to push updates to registry: " +++ fromError mbResponse) w
	# {rsp_code,rsp_data} = fromOk mbResponse
	| rsp_code >= 300
		= fail (concat4 "Unexpected response from registry (" (toString rsp_code) "): " rsp_data) w
		= succeed (?Just "Update succeeded.") w
where
	allFiles = [METADATA_FILE : packageFiles]
	packageFiles = map (fromJust o packageFile pkg) (options.CLIPublishOptions.targets)

	checkLocalPackageFiles w
		# (mbFiles,w) = readDirectory "." w
		| isError mbFiles = (Error ("Failed to read current directory: " +++ snd (fromError mbFiles)), w)
		# foundPackageFiles = filter isPackageFile (fromOk mbFiles)
		| otherwise = case removeMembers packageFiles foundPackageFiles of
			[] -> case removeMembers foundPackageFiles packageFiles of
				[] -> (Ok (), w)
				unspecified -> (Error (join "\n- " ["The following package files were found but not specified:":unspecified]), w)
			not_found -> (Error (join "\n- " ["The following package files could not be found:":not_found]), w)

	checkIfFilesExistOnGitLab _ _ [] w = (Ok (), w)
	checkIfFilesExistOnGitLab api_url project [f:fs] w
		# (mbFileExists,w) = fileExistsOnGitLab f w
		| isError mbFileExists = (Error (concat4 "Failed to check if " f " exists remotely: " (snd (fromError mbFileExists))), w)
		| fromOk mbFileExists = (Error (f +++ " already exists remotely."), w)
		| otherwise = checkIfFilesExistOnGitLab api_url project fs w
	where
		fileExistsOnGitLab file w
			# (mbExitCode,w) = callProcessAndPassIO
				"curl"
				[ "-L" // follow redirects
				, "-s" // silent
				, "-f" // fail on server error
				, fileUrl api_url project file
				, "-o", IF_WINDOWS "NUL" "/dev/null"
				]
				?None w
			= (((==) 0) <$> mbExitCode, w)

	uploadFiles _ _ _ [] w = (Ok (), w)
	uploadFiles token api_url project [f:fs] w
		# w = log ("Uploading " +++ f +++ "...") w
		# (mbError,w) = uploadFile f w
		| isError mbError = (mbError, w)
		| otherwise = uploadFiles token api_url project fs w
	where
		uploadFile file w
			# (mbExitCode,w) = callProcessAndPassIO "curl"
				[ "-L" // follow redirects
				, "-f" // fail on server error
				, "--retry", "5"
				, "-#" // progress bar
				, "--header", "JOB-TOKEN: " +++ token
				, "--upload-file", file
				, fileUrl api_url project file
				, "-o", IF_WINDOWS "NUL" "/dev/null"
				]
				?None w
			| isError mbExitCode
				= (Error ("Failed to run curl: " +++ snd (fromError mbExitCode)), w)
			| fromOk mbExitCode <> 0
				= (Error ("curl exited with error code " +++ toString (fromOk mbExitCode)), w)
				= (Ok (), w)

	fileUrl api_url project file = concat
		[ api_url, "/projects/", project, "/packages/generic/"
		, fromJust pkg.Package.name, "/", toString (fromJust pkg.Package.version), "/", file
		]

	updateRequest =
		{ newHTTPRequest
		& server_name = REGISTRY_HOST
		, server_port = REGISTRY_PORT
		, req_method = HTTP_POST
		, req_path = "/api/packages/" +++ fromJust pkg.Package.name
		, req_headers = 'Data.Map'.fromList
			[ ("Content-Type", "application/x-www-form-urlencoded")
			, ("Content-Length", toString (size data))
			]
		, req_data = data
		}
	where
		data = toString (JSONObject [("url", JSONString (fromJust pkg.Package.url))])
