implementation module Nitrile.CLI.Fetch

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Error
import Data.Func
import Data.List
import qualified Data.Map
import Data.Maybe
import Data.Tuple
import StdEnv
import System.Directory
import System.File
import System.FilePath
import System.SysCall
import Text

import Data.CTags
import Data.NamedList
import Data.SemVer
import Data.SemVer.ConstraintSet
import Nitrile.CLI.Util
import Nitrile.CLI.Util.CTags
import Nitrile.CLI.Util.InstalledPackages
import Nitrile.Constants
import Nitrile.LockFile
import Nitrile.Package
import Nitrile.Registry

fetch :: !CLIFetchOptions !Target !Package !*World -> (!Bool, !*World)
fetch options target pkg w
	# mbError = checkSpecificTarget target
	| isError mbError = fail (fromError mbError) w
	# (mbRegistry,w) = getRegistry w
	  registry = fromOk mbRegistry
	| isError mbRegistry = fail (fromError mbRegistry) w
	# (mbConstraintSet,w) = getConstraintSet target w
	| isError mbConstraintSet = fail (fromError mbConstraintSet) w
	# mbVersions = satisfyConstraints deps (fromOk mbConstraintSet)
	  versionsMap = fromOk mbVersions
	  versions = 'Data.Map'.toList versionsMap
	| isError mbVersions = fail ("Failed to satisfy constraints: " +++ fromError mbVersions +++ ".") w
	# (mbError,w) = ensureDirectoryExists local_packages_dir w
	| isError mbError = fail ("Failed to create " +++ local_packages_dir +++ ": " +++ snd (fromError mbError)) w
	// We don't want to reinstall installed packages that are already installed
	# (mbInstalledPackages,w) = getLocallyInstalledPackages local_packages_dir w
	  installedPackages = fromMaybe 'Data.Map'.newMap (error2mb mbInstalledPackages)
	# versionsToInstall = filter
		(\(pkg,v) -> maybe True ((<>) v) ('Data.Map'.get pkg installedPackages))
		versions
	// We want to delete directories that are not needed anymore
	# (mbDirectories,w) = readDirectory local_packages_dir w
	# directoriesToDelete = filter
		(\dir -> dir <> "." && dir <> ".." && dir <> ".home" && (not ('Data.Map'.member dir versionsMap) || any ((==) dir o fst) versionsToInstall))
		(fromMaybe [] (error2mb mbDirectories))
	| isEmpty versionsToInstall && isEmpty directoriesToDelete
		# w = listOutdatedPackages versions registry w
		= succeed (?Just "Everything up to date.") w
	# (mbErrors,w) = mapSt (\dir -> recursiveDelete (local_packages_dir </> dir)) directoriesToDelete w
	| any isError mbErrors = fail ("Failed to delete some old directories from " +++ local_packages_dir) w
	# (mbDownloadedPackages,w) = prepareInstallPackages versionsToInstall registry target local_packages_dir w
	  downloads = fromOk mbDownloadedPackages
	| isError mbDownloadedPackages = fail (fromError mbDownloadedPackages) w
	# (mbErrors,w) = mapSt (\(pkg,_) -> installPackageLocally target pkg local_packages_dir downloads) versionsToInstall w
	| any isError mbErrors = fail (hd [e \\ Error e <- mbErrors]) (cleanupPackageDownloads local_packages_dir downloads w)
	# w = listOutdatedPackages versions registry w
	# (lockFile,w) = appFst (fromMaybe emptyLockFile o error2mb) (readLockFile LOCK_FILE w)
	# lockFile = setLockedPackages target [{LockPackage | name=pkg, version=v} \\ (pkg,v) <- versions] lockFile
	# (mbError,w) = writeLockFile LOCK_FILE lockFile w
	| isError mbError = fail (fromError mbError) w
	# w = log (concat3 "Wrote versions of all dependencies to " LOCK_FILE ". You should commit this file to version control.") w
	= succeed ?None w
where
	local_packages_dir = localPackagesDir target
	deps = Dependencies ('Data.Map'.fromList
		[ (pkg, (if options.noOptional (fromMaybe False optional) False, version))
		\\ (pkg,{Dependency|version,optional}) <- fromDependencyList pkg.Package.dependencies
		])

listOutdatedPackages :: ![(String, Version)] !Registry !*World -> *World
listOutdatedPackages pkgs (Registry reg) w = seqSt (uncurry mbList) pkgs w
where
	mbList pkg v w
		| maxv <> v
			= log (concat ["Note: ",pkg,"@",toString v," is not the latest version (",toString maxv,")"]) w
			= w
	where
		regpkg = fromMaybe (abort "internal error in listOutdatedPackages\n") (find (\regp -> regp.RegistryPackage.name == pkg) reg)
		(maxv,_) = 'Data.Map'.findMax regpkg.versions

installPackageLocally :: !Target !String !FilePath !PackageDownloads !*World -> (!MaybeError String (), !*World)
installPackageLocally target name local_packages_dir downloads w
	# w = log (concat3 "Installing " name "...") w
	# (mbError,w) = installPackage name path downloads w
	| isError mbError = (mbError, w)
	# (mbError,w) = linkExecutables (path </> "bin") (home_dir </> "bin") (".." </> ".." </> name </> "bin") w // TODO no. of parent directories should not be hard-coded
	| isError mbError = (Error ("Failed to link binaries: " +++ fromError mbError), w)
	# (mbError,w) = linkExecutables (path </> "exe") (home_dir </> "exe") (".." </> ".." </> name </> "exe") w // TODO no. of parent directories should not be hard-coded
	| isError mbError = (Error ("Failed to link binaries: " +++ fromError mbError), w)
	# (mbError,w) = writeTagFile (path </> "lib") w
	| isError mbError = (Error ("Failed to write tagfile: " +++ fromError mbError), w)
	| otherwise = (Ok (), w)
where
	home_dir = homeDir target
	path = local_packages_dir </> name

	writeTagFile path w
		# (exi,w) = fileExists path w
		| not exi = (Ok (), w) // the package just doesn't have libraries to generate a tag file for.
		# (errors,dcls,w) = scanDirectory (\path _ paths w -> (if (endsWith ".dcl" path) [path:paths] paths, w)) [] path w
		# w = seqSt (\(_,e) -> log ("Error while searching for dcls: " +++ e)) errors w
		# (tagsets,w) = mapSt (\dcl -> collectTags False path (dcl % (size path+1, size dcl-5))) dcls w
		# tags = combineTagSets tagsets
		| tagSetSize tags == 0 = (Ok (), w)
		# (ok,f,w) = fopen (path </> TAGS_FILE) FWriteText w
		| not ok = (Error ("failed to open " +++ (path </> TAGS_FILE)), w)
		# f = f <<< tags
		# (ok,w) = fclose f w
		| not ok = (Error ("failed to close " +++ (path </> TAGS_FILE)), w)
		| otherwise = (Ok (), w)
