implementation module Nitrile.CLI.Run

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Error
import qualified Data.Map
import StdEnv
import System.FilePath
import System.Process
import Text

import Data.SemVer
import Data.SemVer.Constraint
import Data.SemVer.ConstraintSet
import Nitrile.CLI
import Nitrile.CLI.Util
import Nitrile.CLI.Util.DependencySources

run :: !GlobalOptions !CLIRunOptions !(?Package) !*World -> (!Bool, !*World)
run opts=:{verbose} {package,executable,args} mbPkg w
	| package == "nitrile" = fail "It is not allowed to run nitrile using the nitrile run command." w
	# deps = Dependencies ('Data.Map'.singleton package (False, VersionConstraint [VersionGe {major=0,minor=0,patch=0}]))
	# (mbDependencySources,w) = getDependencySources True opts mbPkg (?Just deps) w
	| isError mbDependencySources = fail (fromError mbDependencySources) w
	# path = case 'Data.Map'.get package (fromOk mbDependencySources) of
		?None -> abort "Internal error in run\n"
		?Just (DependencySource path _) -> path </> "bin" </> executable
	# w = if verbose (log ("Running executable in the following path: " +++ path) w) w
	# (mbErr,w) = replaceCurrentProcess path args w
	// replaceCurrentProcess does not return on success
	= fail ("Failed to run process: " +++ snd (fromError mbErr)) w
