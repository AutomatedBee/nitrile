definition module Nitrile.Registry

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Map import :: Map
from StdOverloaded import class <
from System.Time import :: Timestamp
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from Data.SemVer import :: Version
from Data.SemVer.Constraint import :: VersionConstraint
from Data.SemVer.ConstraintSet import :: ConstraintSet
from Nitrile.Target import :: Target

:: Registry =: Registry [RegistryPackage]

:: RegistryPackage =
	{ id          :: !Int
	, name        :: !String
	, url         :: !String
	, maintainer  :: !String
	, description :: !String
	, versions    :: !Map Version RegistryVersionInfo
	}

:: RegistryVersionInfo =
	{ created :: !Timestamp
	, targets :: !Map Target RegistryTargetInfo
	}

:: RegistryTargetInfo =
	{ url          :: !String
	, dependencies :: !Map String RegistryDependency
	}

:: RegistryDependency =
	{ version  :: !VersionConstraint
	, optional :: !?Bool
	}

derive JSONEncode ConstraintSet
derive JSONDecode Registry, ConstraintSet

toConstraintSet :: !Target !Registry -> ConstraintSet
