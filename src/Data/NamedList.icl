implementation module Data.NamedList

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Control.Monad
import Data.Error
import Data.Func
import Data.Functor
import Data.List
import Data.Tuple
import StdEnv
import Text.GenJSON
import Text.YAML
import Text.YAML.Compose
import Text.YAML.Construct

fromNamedList :: !(NamedList e) -> [(String, e)]
fromNamedList (NamedList xs) = xs

JSONEncode{|NamedList|} fx _ (NamedList kvs) =
	[JSONObject (map (appSnd (hd o fx False)) kvs)]

gConstructFromYAML{|NamedList|} fx schema _ nodes = case nodes of
	[{content=YAMLScalar _ ""}:rest] ->
		Ok (NamedList [], rest)
	[{content=YAMLMapping kvs}:rest] ->
		fmap (\kvs -> (NamedList kvs, rest)) (parseList 0 kvs)
	_ ->
		Error (withoutErrorLocations (InvalidContent "expected mapping"))
where
	parseList _ [] = Ok []
	parseList i [{key,value}:kvs] =
		mapError (pushErrorLocation (SequenceIndex i)) (constructFromYAML schema key) >>= \key ->
		mapError (pushErrorLocation (Field key)) (fst <$> fx schema False [value]) >>= \value ->
		parseList (i+1) kvs >>= \kvs ->
		Ok [(key,value):kvs]

onlyNames :: ![String] !(NamedList e) -> MaybeError [String] (NamedList e)
onlyNames names (NamedList kvs)
	# found = [t \\ t=:(k,_) <- kvs | isMember k names]
	| length found <> length names
		= Error (difference names (map fst kvs))
		= Ok (NamedList found)
