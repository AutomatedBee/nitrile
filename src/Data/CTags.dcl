definition module Data.CTags

/**
 * Module to generate tagfiles in the 'extended ctags' format used by Vim,
 * amongst others.
 *
 * Copyright 2017-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from StdFile import class <<<
from StdOverloaded import class toString, class <

:: TagSet (:== [Tag]) // Always sorted

:: Tag =
	{ ident    :: !String
	, file     :: !String
	, address  :: !TagAddress
	, comments :: ![Comment]
	}

:: TagAddress
	= Line !Int
	| Search !String // Enclosed in /../
	| CompoundAddress ![TagAddress]

:: Comment :== (String, String)

toTagSet :: ![Tag] -> TagSet
tagSetSize :: !TagSet -> Int
combineTagSets :: ![TagSet] -> TagSet
removeComments :: !TagSet -> TagSet

setComment :: !Comment !Tag -> Tag
setComments :: ![Comment] !Tag -> Tag

instance toString Tag
instance toString TagAddress

instance <<< Tag
instance <<< TagSet

instance < Tag
