implementation module Data.Nullable

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Bifunctor
import Data.Error
import Data.Functor
import Data.Tuple
import StdEnv
import Text.YAML
import Text.YAML.Compose
import Text.YAML.Construct
import Text.YAML.Schemas

instance Functor Nullable
where
	fmap :: !(a -> b) !(Nullable a) -> Nullable b
	fmap _ Null = Null
	fmap f (NonNull x) = NonNull (f x)

toMaybe :: !(Nullable a) -> ?a
toMaybe Null = ?None
toMaybe (NonNull x) = ?Just x

gConstructFromYAML{|Nullable|} fx schema in_record nodes = case nodes of
	[{properties={tag=""},content=YAMLScalar _ ""}:rest] ->
		Error (withoutErrorLocations (InvalidContent "cannot construct a Nullable from no content"))
	nodes ->
		case constructFromScalar [TAG_NULL] (const (Ok Null)) schema nodes of
			Ok res ->
				Ok res
			_ ->
				bifmap
					(pushErrorLocation (ADT "Nullable"))
					(appFst NonNull)
					(fx schema in_record nodes)
