implementation module Data.SemVer.Constraint

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Control.Monad import liftM2, mapM
import Data.Error
import Data.Func
import Data.Functor
import StdEnv
import Text
import Text.GenJSON

import Data.SemVer

instance == VersionConstraint derive gEq

instance < VersionConstraint
where
	(<) v0 v1 = toString v0 < toString v1

instance toString VersionConstraint
where
	toString (VersionConstraint parts) = join " || " (map toString parts)

instance == VersionConstraintPart derive gEq

instance toString VersionConstraintPart
where
	toString (KeepFirstNonZero v) = "^" +++ toString v
	toString (KeepMinor v) = "~" +++ toString v
	toString (VersionGt v) = ">" +++ toString v
	toString (VersionGe v) = ">=" +++ toString v
	toString (VersionLt v) = "<" +++ toString v
	toString (VersionLe v) = "<=" +++ toString v
	toString (VersionEq v) = "=" +++ toString v
	toString (VersionInclusiveRange fr to) = concat3 (toString fr) "-" (toString to)
	toString (VersionConstraintAnd c1 c2) = concat3 (toString c1) " " (toString c2)

JSONEncode{|VersionConstraint|} _ cstr = [JSONString (toString cstr)]
JSONDecode{|VersionConstraint|} _ [JSONString cstr:rest] = (error2mb (parseVersionConstraint cstr), rest)
JSONDecode{|VersionConstraint|} _ json = (?None, json)

parseVersionConstraint :: !String -> MaybeError String VersionConstraint
parseVersionConstraint s = case map trim (split "||" s) of
	[] -> Error "no input"
	parts -> (\parts -> VersionConstraint parts) <$> mapM parsePart parts
where
	// TODO: allow spaces between e.g. >= and the version
	parsePart s = case filter (\s -> size s > 0) $ map trim $ split " " s of
		[part1,part2] ->
			liftM2 VersionConstraintAnd (parseSinglePart part1) (parseSinglePart part2)
		[part] ->
			parseSinglePart part
		[] ->
			Error "no input"
		_ ->
			Error (concat3 "too many spaces in term '" s "'")

	parseSinglePart s = case s.[0] of
		'^' -> KeepFirstNonZero <$> parseVersion (s % (1, size s-1))
		'~' -> KeepMinor <$> parseVersion (s % (1, size s-1))
		'>'
			| size s >= 2 && s.[1] == '='
				-> VersionGe <$> parseVersion (s % (2, size s-1))
				-> VersionGt <$> parseVersion (s % (1, size s-1))
		'<'
			| size s >= 2 && s.[1] == '='
				-> VersionLe <$> parseVersion (s % (2, size s-1))
				-> VersionLt <$> parseVersion (s % (1, size s-1))
		'=' -> VersionEq <$> parseVersion (s % (1, size s-1))
		_ -> case split "-" s of
			[fr,to] -> liftM2 VersionInclusiveRange (parseVersion fr) (parseVersion to)
			_ -> Error (concat3 "failed to parse basic constraint '" s "'")

combineConstraints :: !VersionConstraint !VersionConstraint -> VersionConstraint
combineConstraints (VersionConstraint xs) (VersionConstraint ys) = VersionConstraint
	[VersionConstraintAnd x y \\ x <- xs, y <- ys]

(satisfies) infix 0 :: !Version !VersionConstraint -> Bool
(satisfies) version (VersionConstraint constraints) = any sat constraints
where
	sat (KeepFirstNonZero v=:{major=0,minor=0,patch}) = v == version
	sat (KeepFirstNonZero {major=0,minor,patch}) =
		version.major == 0 && version.minor == minor && version.patch >= patch
	sat (KeepFirstNonZero {major,minor,patch}) =
		version.major == major &&
			(version.minor == minor && version.patch >= patch || version.minor > minor)
	sat (KeepMinor {major,minor,patch}) =
		version.major == major && version.minor == minor && version.patch >= patch
	sat (VersionGt v) = version > v
	sat (VersionGe v) = version >= v
	sat (VersionLt v) = version < v
	sat (VersionLe v) = version <= v
	sat (VersionEq v) = version == v
	sat (VersionInclusiveRange fr to) = fr <= version && version <= to
	sat (VersionConstraintAnd c1 c2) = sat c1 && sat c2
