definition module Data.SemVer.Constraint

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError
from StdClass import class <
from StdOverloaded import class toString
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from Data.SemVer import :: Version

//* A constraint is a disjunction of several options (at least one).
:: VersionConstraint =: VersionConstraint [VersionConstraintPart]

instance == VersionConstraint
instance < VersionConstraint

:: VersionConstraintPart
	= KeepFirstNonZero !Version //* ^1.0.0
	| KeepMinor !Version //* ~2.2.0
	| VersionGt !Version
	| VersionGe !Version
	| VersionLt !Version
	| VersionLe !Version
	| VersionEq !Version
	| VersionInclusiveRange !Version !Version
	| VersionConstraintAnd !VersionConstraintPart !VersionConstraintPart

instance toString VersionConstraint

derive JSONEncode VersionConstraint
derive JSONDecode VersionConstraint

parseVersionConstraint :: !String -> MaybeError String VersionConstraint

combineConstraints :: !VersionConstraint !VersionConstraint -> VersionConstraint

(satisfies) infix 0 :: !Version !VersionConstraint -> Bool
