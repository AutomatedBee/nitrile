definition module Data.SemVer.ConstraintSet

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError
from Data.Map import :: Map

from Data.SemVer import :: Version
from Data.SemVer.Constraint import :: VersionConstraint

/**
 * For each package a version constraint.
 * The boolean is `True` if the package is optional.
 */
:: Dependencies =: Dependencies (Map String (Bool, VersionConstraint))

//* For each package and version the dependencies.
:: ConstraintSet =: ConstraintSet (Map String (Map Version Dependencies))

/**
 * Computes the union of the dependencies, and-ing the constraints for packages
 * that occur multiple times.
 */
combineDependencies :: ![Dependencies] -> Dependencies

/**
 * Try to satisfy a number of dependencies given a constraint set.
 * @param The dependencies to satisfy.
 * @param The constraint set.
 * @result The required package versions, if the dependencies could be
 *   satisfied. This includes versions for dependencies.
 */
satisfyConstraints :: !Dependencies !ConstraintSet -> MaybeError String (Map String Version)
