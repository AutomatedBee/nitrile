implementation module Data.SemVer.ConstraintSet

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Control.Monad
import Control.Monad.Fail
import Data.Error
import Data.Func
import Data.Functor
import Data.List
from Data.Map import :: Map, instance Functor (Map k)
import qualified Data.Map
import Data.Maybe
import Data.Tuple
import StdEnv
from Text import class Text, instance Text String, concat3, concat4, concat5
import qualified Text

import Data.SemVer
import Data.SemVer.Constraint
import Logic.Z3

:: Version` = {v :: !Version, cons :: !Z3FuncDecl, checkCons :: !Z3FuncDecl}
:: Package` = {pkg :: !String, const :: !Z3FuncDecl}

/**
 * The boolean is True if the package is optional. In this case, the list of
 * versions is the list of versions that is *excluded*. Otherwise, the list of
 * versions gives the versions from which one should be chosen.
 */
:: Dependencies` :== Map Package` (Bool, [Version`])
:: ConstraintSet` :== Map Package` (Map Version` Dependencies`)

:: ConstantMapping :== Map String Z3FuncDecl
:: VersionMapping :== Map String (Map (?Version) (Z3FuncDecl, Z3FuncDecl))

instance == Version` where (==) x y = x.v == y.v
instance < Version` where (<) x y = x.v < y.v

instance == Package` where (==) x y = x.pkg == y.pkg
instance < Package` where (<) x y = x.pkg < y.pkg

combineDependencies :: ![Dependencies] -> Dependencies
combineDependencies [] = Dependencies 'Data.Map'.newMap
combineDependencies [deps] = deps
combineDependencies [Dependencies xs,Dependencies ys:rest] = combineDependencies
	[ Dependencies (foldr (\(pkg,cstr) -> 'Data.Map'.alter (put cstr) pkg) xs ('Data.Map'.toList ys))
	: rest
	]
where
	put new=:(newopt,newcstr) mbOld =
		?Just $ maybe
			new
			(\(oldopt,oldcstr) -> (newopt && oldopt, combineConstraints newcstr oldcstr))
			mbOld

satisfyConstraints :: !Dependencies !ConstraintSet -> MaybeError String (Map String Version)
satisfyConstraints deps constraints=:(ConstraintSet constraintsMap) = evalZ3 $
	mapM genPackageVersionEnum ('Data.Map'.toList constraintsMap) >>= \constantsAndMappings ->
	let
		cMapping = 'Data.Map'.fromList [(pkg, const) \\ (pkg,const,_) <- constantsAndMappings]
		vMapping = 'Data.Map'.fromList [(pkg, mapping) \\ (pkg,_,mapping) <- constantsAndMappings]
		mbDeps` = convDependencies deps cMapping vMapping
	in
	if (isError mbDeps`) (fail (fromError mbDeps`)) (pure (fromOk mbDeps`)) >>= \deps` ->
	withOptimizer (
		convConstraintSet deps constraints cMapping vMapping >>= \constraints` ->
		assertConstraintSet constraints` >>|
		let
			assumptions = [toDisjunction dep \\ dep <- 'Data.Map'.toList deps`]
			packages = [pkg \\ {pkg} <- 'Data.Map'.keys deps`]
		in
		checkWithAssumptions assumptions >>= \(sat, unsatisfiable) ->
		case sat of
			Z3LTrue ->
				// NB: checkWithAssumptions won't maximize well, so we assert the dependencies here
				// (instead of using `checkWithAssumptions`), set the maximization goals, and re-check.
				mapM_ assert assumptions >>|
				maximizeLatestVersions deps constraints` >>|
				check >>|
				getModel >>=
				interpretModel vMapping
			Z3LUndef ->
				getReasonForUndef >>= \r ->
				fail ("failed to determine if constraints were satisfiable:\n" +++ r)
			Z3LFalse ->
				let unsatisfiable_packages = 'Text'.join ", " [p \\ p <- packages & i <- [0..] | isMember i unsatisfiable] in
				fail ("constraints are unsatisfiable; I had trouble with: " +++ unsatisfiable_packages)
	)
where
	genPackageVersionEnum :: !(String, Map Version Dependencies) -> Z3 (String, Z3FuncDecl, Map (?Version) (Z3FuncDecl, Z3FuncDecl))
	genPackageVersionEnum (pkg, versions) =
		makeEnum (pkg +++ "-version")
			[ pkg +++ "-unused"
			: [concat3 pkg "-" (toString v) \\ v <- versionList]
			] >>= \(type, [noVersion:constructors]) ->
		makeConst pkg type >>= \const ->
		pure
			( pkg
			, const
			, 'Data.Map'.fromList
				[ (?None, noVersion)
				: [(?Just v, cons) \\ v <- versionList & cons <- constructors]
				]
			)
	where
		versionList = 'Data.Map'.keys versions

	convDependencies :: !Dependencies !ConstantMapping !VersionMapping -> MaybeError String Dependencies`
	convDependencies (Dependencies deps) cMapping vMapping = 'Data.Map'.fromList <$> sequence
		[ liftM2 (\const vs -> ({pkg=pkg, const=const}, (opt, vs)))
			(mb2error (concat3 "mentioned dependency " pkg " not found") ('Data.Map'.get pkg cMapping))
				// this happens when a package specifies a non-existing dependency
			(case conv opt pkg cstr of
				[] | not opt -> Error (concat4 "no matching versions for " pkg ": " (toString cstr))
				vs -> Ok vs)
		\\ (pkg,(opt,cstr)) <- 'Data.Map'.toList deps
		]
	where
		// If the package is optional, we return precisely those versions that should be excluded.
		// By not picking any of those versions, we select either a good version or no version at all.
		conv opt pkg cstr =
			[ {v=v, cons=f, checkCons=isF}
			\\ (?Just v, (f,isF)) <- 'Data.Map'.toList (fromJust ('Data.Map'.get pkg vMapping))
			| if opt not id (v satisfies cstr)
			]

	convConstraintSet :: !Dependencies !ConstraintSet !ConstantMapping !VersionMapping -> Z3 ConstraintSet`
	convConstraintSet (Dependencies deps) (ConstraintSet constraints) cMapping vMapping = 'Data.Map'.fromList <$> sequence
		[ tuple {pkg=pkg, const=cMappingEntry} <$> conv pkg versions cMappingEntry vMappingEntry
		\\ (pkg,versions) <- 'Data.Map'.toList constraints
		, let // These should be safe:
			cMappingEntry = fromMaybe (abort "internal error in convConstraintSet\n") ('Data.Map'.get pkg cMapping)
			vMappingEntry = fromMaybe (abort "internal error in convConstraintSet\n") ('Data.Map'.get pkg vMapping)
		]
	where
		conv pkg versions cMappingEntry vMappingEntry = 'Data.Map'.fromList <$> sequence
			[ case 'Data.Map'.get (?Just v) vMappingEntry of
				?None ->
					// This should not happen
					abort (concat5 "mentioned version " pkg "@" (toString v) " not found\n")
				?Just (f,isF) ->
					(case convDependencies deps cMapping vMapping of
						Ok deps ->
							pure deps
						Error _ ->
							// This happens when the constraints for this package version are
							// broken, e.g. when they specify non-existing dependencies. This is
							// not critical, since it may not matter for the current constraints.
							// Instead, we ensure that the broken version is not used.
							assert (Z3Ite (Z3App isF [Z3App cMappingEntry []]) Z3False Z3True) >>|
							pure 'Data.Map'.newMap) >>= \deps ->
					pure ({v=v, cons=f, checkCons=isF}, deps)
			\\ (v, deps) <- 'Data.Map'.toList versions
			]

	toDisjunction :: !(!Package`, !(!Bool, ![Version`])) -> Z3Expr
	toDisjunction (pkg, (opt, versions)) = if opt (not o Z3Or) Z3Or
		[ Z3App v.checkCons [Z3App pkg.const []]
		\\ v <- versions
		]
	where
		not t = Z3Ite t Z3False Z3True

	assertConstraintSet :: !ConstraintSet` -> Z3 ()
	assertConstraintSet constraints = mapM_
		(\(pkg, versions) -> mapM_
			(\(v, deps) -> assert (Z3Ite
				(Z3App v.checkCons [Z3App pkg.const []])
				(Z3And (map toDisjunction ('Data.Map'.toList deps)))
				Z3True))
			('Data.Map'.toList versions))
		('Data.Map'.toList constraints)

	maximizeLatestVersions :: !Dependencies !ConstraintSet` -> Z3 ()
	maximizeLatestVersions (Dependencies deps) constraints = mapM_ maximizeLatestVersion (first ++ second)
	where
		top_deps = 'Data.Map'.keys deps // maximizing the top-level deps is most important
		(first,second) = partition (\(pkg,_) -> isMember pkg.pkg top_deps) ('Data.Map'.toList constraints)

		maximizeLatestVersion (pkg, versions) = maximize $ foldr
			(\v -> Z3Ite (Z3App v.checkCons [Z3App pkg.const []]) (Z3Int (score v.v)))
			(Z3Int 0x7fffffff) // ideally we don't pick the package at all - this is for the case no version is picked
			('Data.Map'.keys versions)
		where
			// the higher the version, the better
			score v = (v.major << 16) bitor (v.minor << 8) bitor v.patch

	interpretModel :: !VersionMapping !Z3Model -> Z3 (Map String Version)
	interpretModel vMapping model =
		'Data.Map'.fromList <$>
		catMaybes <$>
		mapM getVersion ('Data.Map'.toList model)
	where
		getVersion (pkg, Z3App version []) = find ('Data.Map'.toList (fromJust ('Data.Map'.get pkg vMapping)))
		where
			find [] = fail ("failed to find version for " +++ pkg)
			find [(mbV, (cons,_)):vs] =
				eqFuncDecl cons version >>= \is_eq
					| is_eq -> pure (tuple pkg <$> mbV)
					| otherwise -> find vs
