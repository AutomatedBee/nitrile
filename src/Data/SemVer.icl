implementation module Data.SemVer

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Control.Monad
import Data.Error
import StdEnv
from Text import class Text(split), instance Text String, concat5
import Text.GenJSON

instance == Version
where
	(==) x y = x.major == y.major && x.minor == y.minor && x.patch == y.patch

instance < Version
where
	(<) x y
		| x.major < y.major = True
		| x.major > y.major = False
		| x.minor < y.minor = True
		| x.minor > y.minor = False
		| otherwise = x.patch < y.patch

instance toString Version
where
	toString v = concat5 (toString v.major) "." (toString v.minor) "." (toString v.patch)

JSONEncode{|Version|} _ v = [JSONString (toString v)]
JSONDecode{|Version|} _ [JSONString v:rest] = (error2mb (parseVersion v), rest)
JSONDecode{|Version|} _ json = (?None, json)

parseVersion :: !String -> MaybeError String Version
parseVersion s =
	mapM parseInt (split "." s) >>= \parts ->
	case parts of
		parts | all ((==) 0) parts ->
			Error "at least one part must be non-zero"
		[maj,min,ptc] ->
			Ok {major=maj, minor=min, patch=ptc}
		[maj,min] ->
			Ok {major=maj, minor=min, patch=0}
		[maj] ->
			Ok {major=maj, minor=0, patch=0}
		[] ->
			Error "no input"
		_ ->
			Error "versions can have up to three levels (major.minor.patch)"
where
	parseInt s = case toInt s of
		0
			| s == "0" -> Ok 0
			| otherwise -> Error ("illegal version part '" +++ s +++ "'")
		i
			| i < 0 -> Error "version parts cannot be negative"
			| otherwise -> Ok i
