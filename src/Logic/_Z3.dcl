definition module Logic._Z3

/**
 * This is the low-level interface to Z3. Use `Logic.Z3` instead.
 *
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from StdOverloaded import class fromInt
from System._Pointer import :: Pointer

:: Z3LBool
	= Z3LFalse
	| Z3LUndef
	| Z3LTrue

:: Z3AstKind
	= Z3NumeralAst
	| Z3AppAst
	| Z3VarAst
	| Z3QuantifierAst
	| Z3SortAst
	| Z3FuncDeclAst

:: Z3Config
:: Z3Context
:: Z3Solver
:: Z3Optimize
:: Z3Model`

:: Z3Params

:: Z3Symbol

:: Z3Constructor
:: Z3Sort

:: Z3Ast
:: Z3App
:: Z3FuncDecl

:: Z3AstVector

instance fromInt Z3LBool, Z3AstKind

noContext :: Z3Context
noSolver :: Z3Solver
isNoSolver :: Z3Solver -> Bool
noOptimize :: Z3Optimize
isNoOptimize :: Z3Optimize -> Bool

Z3_global_param_reset_all :: Bool // always True
Z3_global_param_set :: !String !String -> Bool // always True

Z3_mk_config :: Z3Config
Z3_set_param_value :: !String !String !Z3Config -> Z3Config
Z3_del_config :: !Z3Config !Z3Context -> Z3Context

Z3_mk_context :: !Z3Config -> Z3Context
Z3_del_context :: !Z3Context -> Bool

Z3_mk_params :: !Z3Context -> Z3Params
Z3_params_set_bool :: !Z3Symbol !Bool !Z3Params !Z3Context -> Z3Context
Z3_params_dec_ref :: !Z3Params !Z3Context -> Z3Context

Z3_mk_solver :: !Z3Context -> Z3Solver
Z3_solver_set_params :: !Z3Params !Z3Solver !Z3Context -> Z3Context
Z3_solver_assert :: !Z3Ast !Z3Solver !Z3Context -> Z3Context
Z3_solver_check :: !Z3Solver !Z3Context -> Z3LBool
Z3_solver_check_assumptions :: ![Z3Ast] !Z3Solver !Z3Context -> Z3LBool
Z3_solver_get_reason_unknown :: !Z3Solver !Z3Context -> String
Z3_solver_get_model :: !Z3Solver !Z3Context -> Z3Model`
Z3_solver_get_unsat_core :: !Z3Solver !Z3Context -> Z3AstVector
Z3_solver_to_string :: !Z3Solver !Z3Context -> String
Z3_solver_dec_ref :: !Z3Solver !Z3Context -> Z3Context

Z3_mk_optimize :: !Z3Context -> Z3Optimize
Z3_optimize_set_params :: !Z3Params !Z3Optimize !Z3Context -> Z3Context
Z3_optimize_assert :: !Z3Ast !Z3Optimize !Z3Context -> Z3Context
Z3_optimize_check :: ![Z3Ast] !Z3Optimize !Z3Context -> Z3LBool
Z3_optimize_get_reason_unknown :: !Z3Optimize !Z3Context -> String
Z3_optimize_get_model :: !Z3Optimize !Z3Context -> Z3Model`
Z3_optimize_get_unsat_core :: !Z3Optimize !Z3Context -> Z3AstVector
Z3_optimize_maximize :: !Z3Ast !Z3Optimize !Z3Context -> Z3Context
Z3_optimize_minimize :: !Z3Ast !Z3Optimize !Z3Context -> Z3Context
Z3_optimize_to_string :: !Z3Optimize !Z3Context -> String
Z3_optimize_dec_ref :: !Z3Optimize !Z3Context -> Z3Context

Z3_model_eval :: !Bool !Z3Ast !Z3Model` !Z3Context -> ?Z3Ast
Z3_model_get_const_decl :: !Int !Z3Model` !Z3Context -> Z3FuncDecl
Z3_model_get_const_interp :: !Z3FuncDecl !Z3Model` !Z3Context -> ?Z3Ast
Z3_model_get_num_consts :: !Z3Model` !Z3Context -> Int
Z3_model_to_string :: !Z3Model` !Z3Context -> String

Z3_mk_int_symbol :: !Int !Z3Context -> Z3Symbol
Z3_mk_string_symbol :: !String !Z3Context -> Z3Symbol
Z3_get_symbol_string :: !Z3Symbol !Z3Context -> String

Z3_get_ast_kind :: !Z3Ast !Z3Context -> ?Z3AstKind
Z3_ast_to_string :: !Z3Ast !Z3Context -> String

Z3_to_app :: !Z3Ast !Z3Context -> Z3App // just type casting
Z3_get_app_arg :: !Int !Z3App !Z3Context -> Z3Ast
Z3_get_app_decl :: !Z3App !Z3Context -> Z3FuncDecl
Z3_get_app_num_args :: !Z3App !Z3Context -> Int

Z3_mk_func_decl :: !Z3Symbol ![Z3Sort] !Z3Sort !Z3Context -> Z3FuncDecl
Z3_func_decl_to_ast :: !Z3FuncDecl !Z3Context -> Z3Ast // just type casting
Z3_to_func_decl :: !Z3Ast !Z3Context -> Z3FuncDecl // just type casting
Z3_get_decl_name :: !Z3FuncDecl !Z3Context -> Z3Symbol
Z3_get_func_decl_id :: !Z3FuncDecl !Z3Context -> Int

// Simple versions of Z3_mk_constructor etc. for constructors without arguments
Z3_mk_constant_constructor :: !Z3Symbol !Z3Symbol !Z3Context -> Z3Constructor
Z3_query_constant_constructor :: !Z3Constructor !Z3Context -> (!Z3FuncDecl, !Z3FuncDecl)

Z3_del_constructor :: !Z3Constructor !Z3Context -> Z3Context

Z3_mk_datatype :: !Z3Symbol ![Z3Constructor] !Z3Context -> Z3Sort

Z3_mk_int_sort :: !Z3Context -> Z3Sort

Z3_is_eq_ast :: !Z3Ast !Z3Ast !Z3Context -> Bool

Z3_mk_and :: ![Z3Ast] !Z3Context -> Z3Ast
Z3_mk_app :: !Z3FuncDecl ![Z3Ast] !Z3Context -> Z3Ast
Z3_mk_eq :: !Z3Ast !Z3Ast !Z3Context -> Z3Ast
Z3_mk_false :: !Z3Context -> Z3Ast
Z3_mk_int :: !Int !Z3Sort !Z3Context -> Z3Ast
Z3_mk_ite :: !Z3Ast !Z3Ast !Z3Ast !Z3Context -> Z3Ast
Z3_mk_or :: ![Z3Ast] !Z3Context -> Z3Ast
Z3_mk_true :: !Z3Context -> Z3Ast

Z3_ast_vector_get :: !Z3AstVector !Int !Z3Context -> Z3Ast
Z3_ast_vector_size :: !Z3AstVector !Z3Context -> Int
Z3_ast_vector_dec_ref :: !Z3AstVector !Z3Context -> Z3Context
