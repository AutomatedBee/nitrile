implementation module System.FilePath.Match

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.List
import StdEnv
import System.FilePath

matchesAny :: ![String] !FilePath -> Bool
matchesAny patterns path = any (flip matches path) patterns

matches :: !String !FilePath -> Bool
matches pattern path
	// Convert to lists
	# pattern = [c \\ c <-: pattern]
	  path = [if (c == pathSeparator) '/' c \\ c <-: path]
	// Add initial path separator if there is none
	# path = case path of ['/':_] -> path; _ -> ['/':path]
	// Make patterns without slashes at the beginning/in the middle match anywhere
	# pattern = if (any ((==) '/') (init pattern)) pattern ['**/':pattern]
	// Do the actual matching
	= case pattern of
		['**/':pattern] -> // match any in directories
			any (match pattern) [p \\ ['/':p] <- tails path]
		['/':_] ->
			match pattern path
		_ -> // prefix a slash to the pattern, as we did with the path
			match ['/':pattern] path

// /** at the end matches everything in a directory (not the directory itself)
match ['/**'] ['/':path] = not (isEmpty path)
// /**/ in the middle matches an arbitrary number of directories
match ['/**/':pattern] path=:['/':_] = any (match pattern) [p \\ ['/':p] <- tails path]
match ['/**/':pattern] _ = False

// * matches any number of characters except /
match ['*':pattern] path = any (match pattern) (takeWhile (\p -> if (isEmpty p) True (hd p <> '/')) (tails path))
// ? matches any one character except /
match ['?':_] ['/':_] = False
match ['?':pattern] [_:path] = match pattern path

// / at the end matches a directory (which also includes everything in the directory)
match ['/'] ['/':_] = True

// other characters match by equality
match [c1:pattern] [c2:path] = c1 == c2 && match pattern path
match [] ['/':_] = True
match [] [] = True
match _ _ = False
